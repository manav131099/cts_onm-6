rm(list=ls(all =TRUE))

pathRead <- "C:/Users/16030503/Desktop/Cleantech Solar/Wall Graph/Raw Data/[SG-004S]"
pathWrite <- "C:/Users/16030503/Desktop/Cleantech Solar/Wall Graph/Data Extracted/SG-004S_PR_summary.txt"
pathWrite2 <- "C:/Users/16030503/Desktop/Cleantech Solar/Wall Graph/Data Extracted/SG-004S_PR_summary.csv"
setwd(pathRead)                                     #set working directory
print("Extracting data..")
filelist <- dir(pattern = ".txt", recursive= TRUE)  #contains only files of '.txt' format

nameofStation <- "SG-004S"

col0 <- c()
col1 <- c()
col2 <- c()
col3 <- c()
col4 <- c()


col0[10^6] = col1[10^6] = col2[10^6] = col3[10^6] = 0


index <- 1

for (i in filelist){
  data <- NULL
  temp <- read.table(i,header = T, sep = "\t")  #reading text file at location filelist[i]
  
  if(identical(nchar(i), as.integer(31))){
    
    for (j in 1:nrow(temp)){
      
      col0[index] <- nameofStation
      
      col1[index] <- paste0(temp[j,1])
      
      col2[index] <- as.numeric(temp[j,11])
      
      col3[index] <- as.numeric(temp[j,12])
      
      index <- index + 1
      
    }
    
    print(paste(i, "done"))
    
  }

}

col0 <- col0[1:(index-1)]   #removes last row for all columns
col1 <- col1[1:(index-1)]
col2 <- col2[1:(index-1)]
col3 <- col3[1:(index-1)]
col1[is.na(col1)] <- NA              #states T/F
col2[is.na(col2)] <- NA
col3[is.na(col3)] <- NA
col4[is.na(col4)] <- NA
col2[which(col2==0)] = NA
col3[which(col3==0)] = NA

col2[c(4,157,5)] = NA 
col3[c(158,4,5,437)] = NA           



print("Starting to save..")

result <- cbind(col0,col1,col2,col3)
colnames(result) <- c("Meter Reference","Date","PR MA","PR MB")         #columns names


for(x in c(3,4)){
  result[,x] <- round(as.numeric(result[,x]),1)

}

rownames(result) <- NULL
result <- data.frame(result)
result <- result[-c(1:3),] # remove row 1 to 3
for(i in 1:nrow(result)){
  result[i,5] <- i
}
colnames(result) <- c("Meter Reference","Date","PR MA","PR MB","No. of Days")
write.table(result,na = "",pathWrite,row.names = FALSE,sep ="\t")   #saving into txt and csv file
write.csv(result,pathWrite2, na= "", row.names = FALSE)