from ftplib import FTP
import pandas as pd
import time
import datetime
import pytz
import io
import urllib.request
import re 
import os
import numpy as np

pd.options.mode.chained_assignment = None

def chkdir(path):
    a=os.path.isdir(path)
    if(a):
        return
    else:
        os.makedirs(path)
        
path='/home/admin/Dropbox/SERIS_Live_Data/[SG-005S]/'
chkdir(path)
tz = pytz.timezone('Asia/Singapore')
curr=datetime.datetime.now(tz)
currnextmin=curr + datetime.timedelta(minutes=1)
currnextmin=currnextmin.replace(second=8,microsecond=0)
while(curr!=currnextmin):
    curr=datetime.datetime.now(tz)
    curr=curr.replace(microsecond=0)
    time.sleep(1)
currtime=curr.replace(tzinfo=None)
print("Start time is",curr)
cols=[]
for i in range(600):
    cols.append(i+1)
print("Starting Live Bot!")
ftp = FTP('ftpnew.cleantechsolar.com')
ftp.login(user='cleantechsolar', passwd = 'bscxataB2')
ftp.cwd('1min')
flag=1
starttime=time.time()
cols2=['Time Stamp','AvgSMP10','AvgGsi00','AvgGmod','AvgTamb','AvgHamb','AvgTmod','AvgWindS','AvgWindD','Act_Pwr-Tot_F3-5','Act_Pwr-Tot_F2','Act_Pwr-Tot_F7A','Act_Pwr-Tot_F7B','Act_Pwr-Tot_F7G','Act_E-Del_F3-5','Act_E-Del_F2','Act_E-Del_F7A','Act_E-Del_F7B','Act_E-Del_F7G']
while(1):
    if(flag==1):
        cur=currtime.strftime("%Y-%m-%d_%H-%M")
    else:
        curr=datetime.datetime.now(tz)
        currtime=curr.replace(tzinfo=None)
        cur=currtime.strftime("%Y-%m-%d_%H-%M")
    print('Now adding for',currtime)
    flag2=1
    j=0
    while(j<5 and flag2!=0):
        j=j+1
        files=ftp.nlst()        
        for i in files:
            if(re.search(cur,i)):
                req = urllib.request.Request('ftp://cleantechsolar:bscxataB2@ftpnew.cleantechsolar.com/1min/Cleantech-'+cur+'.xls')
                try:
                    with urllib.request.urlopen(req) as response:
                        s = response.read()
                    df=pd.read_csv(io.StringIO(s.decode('utf-8')),sep='\t',names=cols)
                    a=df.loc[df[1] == 724]
                    a2=df.loc[df[1] == 725]
                    a3=df.loc[df[1] == 726]
                    a4=df.loc[df[1] == 727]
                    if(flag==1):
                        b=a[[2,3,4,5,6,7,8,9,10,24,39]]
                        b2=a2[[16,31]]
                        b3=a3[[16,31,63,78]]
                        b4=a4[[16,31]]
                        res=pd.concat([b, b2])
                        res=pd.concat([res, b3])
                        res=pd.concat([res, b4])
                        res.insert(11,'17',str(res.iloc[2][16]))
                        res["17"] = pd.to_numeric(res["17"])
                        res.insert(12,'18',str(res.iloc[2][63]))
                        res["18"] = pd.to_numeric(res["18"])
                        del res[63]
                        res.insert(13,'19',str(res.iloc[3][16]))
                        res["19"] = pd.to_numeric(res["19"])
                        res.loc[:, 16] =res.iloc[1][16] 
                        res.insert(16,'40',str(res.iloc[2][31]))
                        res["40"] = pd.to_numeric(res["40"])
                        res["80"] = res.iloc[3][31] 
                        res.loc[:, 31] =res.iloc[1][31] 
                        res.loc[:, 78] =res.iloc[2][78] 
                        res=res.iloc[0]
                        res=res.to_frame()
                        res=res.transpose()
                        print(res.loc[:, '80'])
                        flag=0
                    else:
                        b=a[[2,3,4,5,6,7,8,9,10,24,39]]
                        b2=a2[[16,31]]
                        b3=a3[[16,31,63,78]]
                        b4=a4[[16,31]]
                        res=pd.concat([b, b2])
                        res=pd.concat([res, b3])
                        res=pd.concat([res, b4])
                        res.insert(11,'17',str(res.iloc[2][16]))
                        res["17"] = pd.to_numeric(res["17"])
                        res.insert(12,'18',str(res.iloc[2][63]))
                        res["18"] = pd.to_numeric(res["18"])
                        del res[63]
                        res.insert(13,'19',str(res.iloc[3][16]))
                        res["19"] = pd.to_numeric(res["19"])
                        res.loc[:, 16] =res.iloc[1][16] 
                        res.insert(16,'40',str(res.iloc[2][31]))
                        res["40"] = pd.to_numeric(res["40"])
                        res["80"] = res.iloc[3][31] 
                        res.loc[:, 31] =res.iloc[1][31] 
                        res.loc[:, 78] =res.iloc[2][78] 
                        res=res.iloc[0]
                        res=res.to_frame()
                        res=res.transpose()
                        print(res.columns)
                        if(os.path.exists(path+cur[0:4]+'/'+cur[0:7]+'/'+'[SG-005S] '+cur[0:10]+'.txt')):
                            df= pd.read_csv(path+cur[0:4]+'/'+cur[0:7]+'/'+'[SG-005S] '+cur[0:10]+'.txt',sep='\t')
                            df2= pd.DataFrame( np.concatenate( (df.values, res.values), axis=0 ) )
                            df2.columns = cols2
                        std1=df2['Act_E-Del_F2'].std()
                        std2=df2['Act_E-Del_F3-5'].std()
                        std3=df2['Act_E-Del_F7A'].std()
                        std4=df2['Act_E-Del_F7B'].std()
                        std5=df2['Act_E-Del_F7G'].std()
                        if(std1>13000):
                            res.loc[:, 39] = 'NaN' 
                        if(std2>13000):
                            res.loc[:, 31] = 'NaN' 
                        if(std3>13000):
                            res.loc[:, '40'] = 'NaN'
                        if(std4>13000):
                            res.loc[:, 78] = 'NaN'
                        if(std5>13000):
                            res.loc[:, '80'] = 'NaN'
                        res.loc[res[39] == 0, 39] = 'NaN'
                        res.loc[res[31] == 0, 31] = 'NaN'
                        res.loc[res['40'] == 0, '40'] = 'NaN'
                        res.loc[res[78] == 0, 78] = 'NaN'
                        res.loc[res['80'] == 0, '80'] = 'NaN'
                        res.loc[res[39] < 0, 39] = 'NaN'
                        res.loc[res[31] < 0, 31] = 'NaN'
                        res.loc[res['40'] < 0, '40'] = 'NaN'
                        res.loc[res[78] < 0, 78] = 'NaN'
                        res.loc[res['80'] < 0, '80'] = 'NaN'
                    #Taking care of Power
                    res.loc[(res[3] < 0), 3] = 0
                    if(currtime.hour>19):
                        res.loc[(res[16] > 50) | (res[16] < -2), 16] = 'NaN'
                        res.loc[(res[24] > 50) | (res[24] < -2), 24] = 'NaN'
                        res.loc[(res['17'] > 50) | (res['17'] < -2), '17'] = 'NaN'
                        res.loc[(res['18'] > 50) | (res['18'] < -2), '18'] = 'NaN'
                        res.loc[(res['19'] > 50) | (res['19'] < -2), '19'] = 'NaN'
                    res.loc[(res[16] > 2000) | (res[16] < -2), 16] = 'NaN'
                    res.loc[(res[24] > 2000) | (res[24] < -2), 24] = 'NaN'
                    res.loc[(res['17'] > 2000) | (res['17'] < -2), '17'] = 'NaN'
                    res.loc[(res['18'] > 2000) | (res['18'] < -2), '18'] = 'NaN'
                    res.loc[(res['19'] > 2000) | (res['19'] < -2), '19'] = 'NaN'
                    res.columns=cols2
                    chkdir(path+cur[0:4]+'/'+cur[0:7])
                    if(os.path.exists(path+cur[0:4]+'/'+cur[0:7]+'/'+'[SG-005S] '+cur[0:10]+'.txt')):
                        res.to_csv(path+cur[0:4]+'/'+cur[0:7]+'/'+'[SG-005S] '+cur[0:10]+'.txt',header=False,sep='\t',index=False,mode='a')
                    else:
                        res.to_csv(path+cur[0:4]+'/'+cur[0:7]+'/'+'[SG-005S] '+cur[0:10]+'.txt',header=True,sep='\t',index=False)
                    flag2=0
                except Exception as e:
                    flag2=0
                    print(e)
                    print("Failed for",currtime)                  
        if(flag2!=0 and j<5):
            print('sleeping for 10 seconds!')            
            time.sleep(10)
    if(flag2==1):
        print('File not there!',cur)
    print('sleeping for a minute!')
    time.sleep(60.0 - ((time.time() - starttime) % 60.0))
    
        