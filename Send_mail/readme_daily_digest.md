# Daily Digest Mail Alert System
* ## Addition : How to add new recipients
1. To add new recipients go over to this file residing at *[Dropbox](https://www.dropbox.com)*, **'mail_recipients.csv'**, and open the file using any application capable of opening **Excel** or **Spreadsheet**  such as **Microsoft Excel**.
2. Go to the last row which is not already filled and add the desired *email-id*, on which the digest are expected, to the 1st column and then make the relevant site-code's value positive,i.e., 1,anything other than 1 or 0 in the site-code columns will result in the mail system failure. For example if the receiver wants digest for site *IN-006*,then change the corresponding cell for the user in the column titles _**IN-006**_.

For Example, here 
Recipients|IN-006T|IN-008X
----|----|----
*example@domain.com*|1|0 
In the above example to add yourself to the digest for _**IN-008X**_ create another entry below the already existing one and change the value for the corresponding cell value to 1. Such as...
Recipients|IN-006T|IN-008X
----|----|----
*example@domain.com*|1|0 
*yourself@domain.com*|0|1
This way the digest for *IN-008X* is only sent to *yourself@domain.com* not of any other sites.

* ## Remove: How to remove already present recipients

1. To add new recipients go over to this file residing at *[Dropbox](https://www.dropbox.com)*, **'mail_recipients.csv'**, and open the file using any application capable of opening **Excel** or **Spreadsheet**  such as **Microsoft Excel**. 
2. Remove the desired email-id from the digest along with the full row,i.e.,**Delete** the whole row containing the email-id which is to be removed.

* ## Turn off Digest Alerts for a specific time for specific sites

1. Change the value at the last 2 column titled *From* , *To* from _**TR**_ to the dates desired. The format of the date is as follows : *YYYY-MM-DD* with month in number such as for January -> *01*.
2. The value at *From* and *To* are default at _**TR**_ or *TRUE*. The range of *From* to *To* are the dates for which digests are excluded for desired person.

For example, here
Recipients|IN-006T|IN-008X|From|To
----|----|----|----|----
*example@domain.com*|1|0|TR|TR 

Right now the person with email-id *example@domain.com* will recieve all the emails for site *IN-006T*.

To change to certain range
Recipients|IN-006T|IN-008X|From|To
----|----|----|----|----
*example@domain.com*|1|0|2018-04-25|2018-04-27 
Right now the person with email-id *example@domain.com* will not recieve any emails for site *IN-006T* for the time period of *April 25,2018* to *April 27,2018*.





