errHandle = file('/home/admin/Logs/LogsMY005XMail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)
source('/home/admin/CODE/MY005XDigest/HistoricalAnalysis2G3GMY005X.R')
source('/home/admin/CODE/Misc/memManage.R')
source('/home/admin/CODE/MasterMail/timestamp.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
source('/home/admin/CODE/MY005XDigest/aggregateInfo.R')

initDigest = function(df)
{
  #{
	#if(as.numeric(no) == 1)
	#{
	#  no2 = 'Solar'
	#}
	#else if(as.numeric(no)==2)
	#{
	#  no2 = 'Coke'
	#}
	#}
	no2=no=""
	ratspec = round(as.numeric(df[,2])/190.8,2)
  body = "\n\n_________________________________________________\n\n"
  body = paste(body,substr(as.character(df[,1]),1,10),no2)
  body = paste(body,"\n\n_________________________________________________\n\n")
  acpts = round(as.numeric(df[,5]) * 14.4)
  body = paste(body,"Points recorded: ",acpts," (",as.character(df[,5]),"%)",sep="")
  body = paste(body,"\n\nDowntime (%):",as.character(df[,6]))
  body = paste(body,"\n\nEac",no,"-1 [kWh]: ",as.character(df[,2]),sep="")
  body = paste(body,"\n\nEac",no,"-2 [kWh]: ",as.character(df[,3]),sep="")
  body = paste(body,"\n\nRatio [%]:",as.character(df[,4]))
  body = paste(body,"\n\nYield-1 [kWh/kWp]:",as.character(df[,12]))
  body = paste(body,"\n\nYield-2 [kWh/kWp]:",as.character(df[,13]))
  body = paste(body,"\n\nPR-1:",as.character(df[,14]))
  body = paste(body,"\n\nPR-2:",as.character(df[,15]))
  body = paste(body,"\n\nDaily irradiation [kWh/m2] (from MY-004S):",as.character(df[,16]))
  body = paste(body,"\n\nPR [%] (GHI of MY-004S as baseline)",as.character(df[,17]))
  body = paste(body,"\n\nLast recorded timestamp:",as.character(df[,7]))
  body = paste(body,"\n\nLast recorded energy meter reading [kWh]:",as.character(df[,8]))
  body = paste(body,"\n\nTotal daily irradiation [kWh/m2]:",as.character(df[,9]))
  body = paste(body,"\n\nMean module temperature [C]:",as.character(df[,10]))
  body = paste(body,"\n\nMean module temperature - solar hours [C]:",as.character(df[,11]))
	
	#if(no==1)
	#{
  #body = paste(body,"\n\nCable losses [%]:",CABLOSSTOPRINT)
	#}
  return(body)
}
printtsfaults = function(TS,body)
{
  #{
	#if(as.numeric(num) == 1)
	#{
	 # num = 'Solar'
	#}
	#else if(as.numeric(num)==2)
	#{
	 # num =- 'Coke'
	#}
	#}
	num = " "
	if(length(TS) > 1)
	{
		body = paste(body,"\n\n_________________________________________________\n\n")
		body = paste(body,paste("Timestamps for",num,"where Pac < 1 between 8am - 6pm"))
		body = paste(body,"\n\n_________________________________________________\n")
		for(lm in  1 : length(TS))
		{
			body = paste(body,TS[lm],sep="\n")
		}
	}
	return(body)
}
sendMail = function(df1,pth1,pth3)
{
  filetosendpath = c(pth1)
  if(file.exists(pth3))
	{
	filetosendpath = c(pth1,pth3)
  }
	filenams = c()
  for(l in 1 : length(filetosendpath))
  {
    temp = unlist(strsplit(filetosendpath[l],"/"))
    filenams[l] = temp[length(temp)]
		if(l == 1)
			currday = temp[length(temp)]
  }
	#data3G = read.table(pth3,header =T,sep = "\t")
	#dateineed = unlist(strsplit(filenams[1]," "))
	#dateineed = unlist(strsplit(dateineed[2],"\\."))
	#dateineed = dateineed[1] 
	#idxineed = match(dateineed,as.character(data3G[,1]))
	print('Filenames Processed')
	body = ""
	body = paste(body,"Site Name: Globetronics Industries",sep="")
	body = paste(body,"\n\nLocation: Bayan Lepas, Penang, Malaysia")
	body = paste(body,"\n\nO&M Code: MY-005")
	body = paste(body,"\n\nSystem Size: 558.72 kWp")
	body = paste(body,"\n\nNumber of Energy Meters: 1")
	body = paste(body,"\n\nModule Brand / Model / Nos: Jinko / JKM320PP-320Wp")
	body = paste(body,"\n\nInverter Brand / Model / Nos: SMA / SOLID Q-50")
	body = paste(body,"\n\nSite COD: 2019-03-15")
	body = paste(body,"\n\nSystem age [days]:",as.character((as.numeric(DAYSALIVE))))
	body = paste(body,"\n\nSystem age [years]:",as.character(round((as.numeric(DAYSALIVE))/365,2)))
	bodyac = body
	body=""
  body = initDigest(df1)  #its correct, dont change
	body = paste(bodyac,body)
	#body = paste(body,initDigest(df1))  #its correct, dont change
  body = printtsfaults(TIMESTAMPSALARM,body)
	print('2G data processed')
	body = paste(body,"\n\n_________________________________________________\n\n")
  body = paste(body,"Station Data")
  body = paste(body,"\n\n_________________________________________________\n\n")
  body = paste(body,"Station DOB:",as.character(DOB))
  body = paste(body,"\n\n# Days alive:",as.character(DAYSALIVE))
  yrsalive = format(round((DAYSALIVE/365),2),nsmall=2)
  body = paste(body,"\n\n# Years alive:",yrsalive)
	print('3G data processed')
	body = gsub("\n ","\n",body)
  send.mail(from = sender,
            to = recipients,
            subject = paste("Station [MY-005X] Digest",substr(currday,11,20)),
            body = body,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            attach.files = filetosendpath,
            file.names = filenams, # optional parameter
            debug = F)
recordTimeMaster("MY-005X","Mail",substr(currday,11,20))
}
sender = c('operations@cleantechsolar.com')
uname = 'shravan.karthik@cleantechsolar.com'

recipients = getRecipients("MY-005X","m")
#recipients = c('shravan1994@gmail.com')

pwd = 'CTS&*(789'
todisp = 1
while(1)
{
	recipients = getRecipients("MY-005X","m")
  recordTimeMaster("MY-005X","Bot")
	sendmail = 0
  prevx = x
  prevy = y
  prevt = t
  years = dir(path)
  for(x in prevx : length(years))
  {
    pathyear = paste(path,years[x],sep="/")
    writepath2Gyr = paste(writepath2G,years[x],sep="/")
    writepath3Gyr = paste(writepath3G,years[x],sep="/")
    checkdir(writepath2Gyr)
    checkdir(writepath3Gyr)
    months = dir(pathyear)
    startmnth = 1
    if(prevx == x)
    {
      startmnth = prevy
    }
    for(y in startmnth : length(months))
    {
      pathmonths = paste(pathyear,months[y],sep="/")
      writepath2Gmon = paste(writepath2Gyr,months[y],sep="/")
      writepath3Gfinal = paste(writepath3Gyr,"/[MY-005X] ",months[y],".txt",sep="")
      checkdir(writepath2Gmon)
      #stations = dir(pathmonths)
      #pathdays = paste(pathmonths,stations[1],sep="/")
      #pathdays2 = paste(pathmonths,stations[2],sep="/")
      #writepath2Gdays = paste(writepath2Gmon,stations[1],sep="/")
      #writepath2Gdays2 = paste(writepath2Gmon,stations[2],sep="/")
      #checkdir(writepath2Gdays)
      #checkdir(writepath2Gdays2)
      days = dir(pathmonths)
      #days2 = dir(pathdays2)
      startdays = 1
      if(y == prevy)
      {
        startdays = prevt
      }
      if(startdays == 1)
      {
        temp = unlist(strsplit(days[1]," "))
				print(substr(temp[2],1,10))
        temp = as.Date(substr(temp[2],1,10),"%Y-%m-%d")
      }
      for(t in startdays : length(days))
      {
         condn1  = (t != length(days))
         condn2 = ((t == length(days)) && ((y != length(months)) || (x != length(years))))
         if(!(condn1 || condn2))
         {
				 	 if(todisp)
					 {
					 	print('No new files')
						todisp = 0
					 }
           Sys.sleep(3600)
           next
         }
				 if(grepl("Copy",c(days[t])))
				 {
				 	daycop = c(days[t])
					daycop = daycop[grepl("Copy",daycop)]
					for(inner in 1 : length(daycop))
					{
				 	print('Copy file found so removing it')
					command = paste('rm "',pathmonths,'/',daycop[t],'"',sep="")
					print(command)
					system(command)
					print('Command complete.. sleeping')
					}
				 	Sys.sleep(3600)
					next
				 }

				 todisp = 1
         sendmail = 1
         DAYSALIVE = DAYSALIVE + 1
				 print(paste('Processing',days[t]))
				 #print(paste('Processing',days2[t]))
         writepath2Gfinal = paste(writepath2Gmon,"/",days[t],sep="")
         #writepath2Gfinal2 = paste(writepath2Gdays2,"/",days2[t],sep="")
         readpath = paste(pathmonths,days[t],sep="/")
         #readpath2 = paste(pathdays2,days2[t],sep="/")
				 #dataprev = read.table(readpath2,header = T,sep = "\t")
				 #METERCALLED <<- 2  #its correct dont change
				 METERCALLED <<- 1 # its correct dont change
         df1 = secondGenData(readpath,writepath2Gfinal)
         #df2 = secondGenData(readpath2,writepath2Gfinal2)
				 datemtch = unlist(strsplit(days[t]," "))
				 thirdGenData(writepath2Gfinal,writepath3Gfinal)
  if(sendmail ==0)
  {
    next
  }
	print('Sending Mail')
  sendMail(df1,writepath2Gfinal,writepath3Gfinal)
	print('Mail Sent')
      }
    }
  }
	gc()
}
sink()
