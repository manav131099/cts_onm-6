
import os
import glob
path_old = '/home/admin/Dropbox/Cleantechsolar/1min/[716]' 
count = 0
flag = 0
for filename in os.listdir(path_old):
  path = path_old + '/' + filename
  writepath = '/home/admin/Dropbox/ErrataFix/Big Data Analytics/Data Availability/Monthly/SG006X' + '_' + filename + '.txt'
  target = open(writepath, 'w')
  target.write("MONTH")
  target.write("--")
  target.write("DATA AVAILABILITY")
  target.write("\r\n=====================\r\n")
  target.write("\r\n")
  month_list=[]
  for filename in os.listdir(path):
      year = int(filename[0:4])
      #print(year)
      if(year%4 == 0):
        flag = 1 #set flag to one in case of leap year
      newfilepath = path + '/' + filename
      for newfilename in os.listdir(newfilepath):
        path2 = os.path.join(newfilepath + '/' + newfilename)
        with open(path2) as f: 
          lines = [line.strip() for line in f.readlines()]
          for i in lines: 
            
            count = count + 1
      mon = path2[56:-21]
      if(mon == '01' or mon == '03' or mon == '05' or mon == '07' or mon == '08' or mon == '10' or mon == '12'):
        DA = round(count/44671*100,2) # 1441*31 denominator, taking first row into consideration in both N and D
        target.write(path2[51:-21] + '\n')
        target.write("--")
        target.write(str(DA) + ' %')
        target.write("\r\n______________________\r\n")
      if(mon == '02' and flag == 1):
        DA = round(count/41789*100,2) #1441*29, leap year
        target.write(path2[51:-21] + '\n')
        target.write("--")
        target.write(str(DA) + ' %')
        target.write("\r\n______________________\r\n")
      if(mon == '02' and flag == 0):
        DA = round(count/40348*100,2) # 1441*28, not a leap year
        target.write(path2[51:-21] + '\n')
        target.write("--")
        target.write(str(DA) + ' %')
        target.write("\r\n______________________\r\n")
      if(mon == '04' or mon == '06' or mon == '09' or mon == '11'):
        DA = round(count/43230*100,2) #1441*30
        target.write(path2[51:-21] + '\n')
        target.write("--")
        target.write(str(DA) + ' %')
        target.write("\r\n______________________\r\n")
      count = 0
  target.close()



    
    