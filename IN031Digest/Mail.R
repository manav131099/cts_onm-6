rm(list=ls())
system('rm -R "/home/admin/Dropbox/Third Gen/[IN-031T]"')
source('/home/admin/CODE/IN031Digest/2G3GFunctions.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
require('mailR')
errHandle = file('/home/admin/Logs/LogsIN031Mail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)
source('/home/admin/CODE/MasterMail/timestamp.R')
source('/home/admin/CODE/common/math.R')
eac_meth2=0
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}
daysAlive = 0
DOB = "02-11-2017"
sender = c('operations@cleantechsolar.com')
uname = 'shravan.karthik@cleantechsolar.com'
recipients = getRecipients("IN-031T","m")
pwd = 'CTS&*(789'
stnnickNam2 = c("Texmo Moulding","Texmo TPPL")
sendMail= function(path)
{
  stnnickNam2 = c("Texmo Moulding","Texmo TPPL")
  body = "Site Name: Texmo Pipes, Burhanpur\n"
  body = paste(body,"\nLocation: Burhanpur, India\n")
  body = paste(body,"\nO&M Code: IN-031\n")
  body = paste(body,"\nSystem Size: 760.20 kWp\n")
  body = paste(body,"\nNumber of Energy Meters: 2\n")
  body = paste(body,"\nModule Brand / Model / Nos: JA Solar / 265W / 2880\n")
  body = paste(body,"\nInverter Brand / Model / Nos: Delta / RPI M50A / 12\n")
  body = paste(body,"\nSite COD: 2017-08-18\n")
  body = paste(body,"\nSystem age [days]:",(75+daysAlive))
  body = paste(body,"\n\nSystem age [years]:",round((75+daysAlive)/365,2))
  
  bodyac = body
  body=""
  filenams = c()
	texyl = textpyl = NA
  for(outer in 1 : length(path))
  {
    outer2 = stnnickNam2[outer]
    dataread = read.table(path[outer],header = T,sep="\t")
    currday = as.character(dataread[1,1])
    filenams[outer] = paste(currday,".txt",sep="")
    body = paste(body,"\n\n------------------------------------------------\n\n")
    body = paste(body," ",currday," ",outer2,sep="")
    body = paste(body,"\n\n------------------------------------------------\n\n")
    body = paste(body,"DA [%]:",as.character(dataread[1,2]),"\n\n")
    body = paste(body,"EAC method-1 (Pac) [kWh]:",as.character(dataread[1,3]),"\n\n")
    body = paste(body,"EAC method-2 (Eac) [kWh]:",as.character(dataread[1,4]),"\n\n")
    eac_meth2=eac_meth2+as.numeric(dataread[1,4])
    body = paste(body,"Yield-1 [kWh/kWp]:",as.character(dataread[1,5]),"\n\n")
    body = paste(body,"Yield-2 [kWh/kWp]:",as.character(dataread[1,6]),"\n\n")
    if(outer==1)
      texyl=as.numeric(dataread[1,6])
    else
      textpyl=as.numeric(dataread[1,6])
    body = paste(body,"Last recorded value [kWh]:",as.character(dataread[1,7]),"\n\n")
    body = paste(body,"Last recorded timestamp:",as.character(dataread[1,8]),"\n\n")
    body = paste(body,"Total Irradiation [kWh/m^2]:",as.character(dataread[1,9]),"\n\n")
    body = paste(body,"Mean module temperature [C]:",as.character(dataread[1,10]),"\n\n")
    body = paste(body,"PR-1 [%]:",as.character(dataread[1,11]),"\n\n")
    body = paste(body,"PR-2 [%]:",as.character(dataread[1,12]),"\n\n")
    if(outer==1)
    {
      body = paste(body,"Station DOB:",as.character(DOB),"\n\n")
      body = paste(body,"Days alive:",as.character(daysAlive))
    }
  }
	y=(eac_meth2/760.20)
	std=round(sdp(c(texyl,textpyl)),2)
	cv=round((std/mean(c(texyl,textpyl)))*100,1)
	bodyac = paste(bodyac,"\n\nSystem Full Generation (kWh):",format(round(eac_meth2,2),nsmall = 2),"\n\n")
	bodyac = paste(bodyac,"System Full Yield (kWh/kWp):",format(round(y,2),nsmall = 2),"\n\n")
	bodyac = paste(bodyac,"Texmo Moulding Yield (kWh/kWp):",format(round(texyl,2),nsmall = 2),"\n\n")
	bodyac = paste(bodyac,"Texmo TPPL Yield (kWh/kWp):",format(round(textpyl,2),nsmall = 2),"\n\n")
	bodyac = paste(bodyac,"Stdev/COV Yields:",std,"/",cv,"[%]")
	
	body = paste(bodyac,body)
	send.mail(from = sender,
            to = recipients,
            subject = paste("Station [IN-031T] Digest",currday),
            body = body,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            attach.files = path,
            file.names = filenams, # optional paramete
            debug = F)
  recordTimeMaster("IN-031T","Mail",currday)
}


path = "/home/admin/Dropbox/Gen 1 Data/[IN-031T]"
path2G = '/home/admin/Dropbox/Second Gen/[IN-031T]'
path3G = '/home/admin/Dropbox/Third Gen/[IN-031T]'

checkdir(path2G)
checkdir(path3G)
stnnickName2 = "IN-031T"
lastdatemail = lastMailDate(paste('/home/admin/Start/MasterMail/',stnnickName2,'_Mail.txt',sep=""))
stopDate = paste("[",stnnickName2,"-M1] ",lastdatemail,".txt",sep="")
ENDCALL=0

years = dir(path)
for(x in 1 : length(years))
{
  path2Gyr = paste(path2G,years[x],sep = "/")
  path3Gyr = paste(path3G,years[x],sep = "/")
  pathyr = paste(path,years[x],sep="/")
  checkdir(path2Gyr)
  checkdir(path3Gyr)
  months = dir(pathyr)
  for(y in 1 : length(months))
  {
    path2Gmon = paste(path2Gyr,months[y],sep = "/")
    path2Gmon1 = paste(path2Gmon,"Moulding",sep="/")
    path2Gmon2 = paste(path2Gmon,"TPPL",sep="/")
    dirpath3Gfinal1 = paste(path3Gyr,"Moulding",sep = "/")
    dirpath3Gfinal2 = paste(path3Gyr,"TPPL",sep = "/")
    path3Gfinal1 = paste(path3Gyr,"/Moulding/",months[y],".txt",sep = "")
    path3Gfinal2 = paste(path3Gyr,"/TPPL/",months[y],".txt",sep = "")
    pathmon1 = paste(pathyr,months[y],"Moulding",sep="/")
    pathmon2 = paste(pathyr,months[y],"TPPL",sep="/")
    checkdir(path2Gmon)
    checkdir(path2Gmon1)
    checkdir(path2Gmon2)
    checkdir(dirpath3Gfinal1)
    checkdir(dirpath3Gfinal2)
    days1 = dir(pathmon1)
    days2 = dir(pathmon2)
    if(length(days1) > 1)
    {
      for(z in 1 : length(days1))
      {
        if(ENDCALL==1)
          break
        
        if((z==length(days1)) && (y == length(months)) && (x ==length(years)))
          next
        print(days1[z])
        pathfinal1 = paste(pathmon1,days1[z],sep = "/")
        pathfinal2 = paste(pathmon2,days2[z],sep = "/")
        path2Gfinal1 = paste(path2Gmon1,days1[z],sep="/")
        path2Gfinal2 = paste(path2Gmon2,days2[z],sep="/")
        secondGenData(pathfinal1,path2Gfinal1,1)
        secondGenData(pathfinal2,path2Gfinal2,2)
        thirdGenData(path2Gfinal1,path3Gfinal1)
        thirdGenData(path2Gfinal2,path3Gfinal2)
        daysAlive = daysAlive+1
        if(days1[z] == stopDate)
        {
          ENDCALL = 1
        }
      }
    }
    if(ENDCALL == 1)
      break
  }
  if(ENDCALL == 1)
    break
}

print('Backlog done')

prevx = x
prevy = y
prevz = z
repeats = 0
while(1)
{
	recipients = getRecipients("IN-031T","m")
  recordTimeMaster("IN-031T","Bot")
  years = dir(path)
  noyrs = length(years)
  for(x in prevx : noyrs)
  {
    pathyr = paste(path,years[x],sep="/")
    path2Gyr = paste(path2G,years[x],sep="/")
    path3Gyr = paste(path3G,years[x],sep="/")
    checkdir(path2Gyr)
    checkdir(path3Gyr)
    mons = dir(pathyr)
    nomons = length(mons)
    startmn = prevy
    endmn = nomons
    if(startmn>endmn)
    {
      startmn = 1
      prevx = x-1
      prevz = 1
    }
    for(y in startmn:endmn)
    {
      pathmon = paste(pathyr,mons[y],sep="/")
      path2Gmon = paste(path2Gyr,mons[y],sep="/")
      checkdir(path2Gmon)
      path2Gmon1 = paste(path2Gmon,"Moulding",sep="/")
      path2Gmon2 = paste(path2Gmon,"TPPL",sep="/")
      dirpath3Gfinal1 = paste(path3Gyr,"Moulding",sep = "/")
      dirpath3Gfinal2 = paste(path3Gyr,"TPPL",sep = "/")
      path3Gfinal1 = paste(path3Gyr,"/Moulding/",months[y],".txt",sep = "")
      path3Gfinal2 = paste(path3Gyr,"/TPPL/",months[y],".txt",sep = "")
      pathmon1 = paste(pathyr,months[y],"Moulding",sep="/")
      pathmon2 = paste(pathyr,months[y],"TPPL",sep="/")
      checkdir(path2Gmon)
      checkdir(path2Gmon1)
      checkdir(path2Gmon2)
      checkdir(dirpath3Gfinal1)
      checkdir(dirpath3Gfinal2)
      days1 = dir(pathmon1)
      days2 = dir(pathmon2)
      chkcopydays = days1[grepl('Copy',days1)]
      if(length(chkcopydays) > 0)
      {
        print('Copy file found they are')
        print(chkcopydays)
        idxflse = match(chkcopydays,days1)
        print(paste('idx matches are'),idxflse)
        for(innerin in 1 : length(idxflse))
        {
          command = paste("rm '",pathmon1,"/",days1[idxflse[innerin]],"'",sep="")
          print(paste('Calling command',command))
          system(command)
        }
        days1 = days1[-idxflse]
      }
      chkcopydays = days2[grepl('Copy',days2)]
      if(length(chkcopydays) > 0)
      {
        print('Copy file found they are')
        print(chkcopydays)
        idxflse = match(chkcopydays,days2)
        print(paste('idx matches are'),idxflse)
        for(innerin in 1 : length(idxflse))
        {
          command = paste("rm '",pathmon,"/",days2[idxflse[innerin]],"'",sep="")
          print(paste('Calling command',command))
          system(command)
        }
        days2 = days2[-idxflse]
      }
      if(y > startmn)
      {
        z = prevz = 1
      }
      nodays = length(days1) 
      if(prevz <= nodays)
      {
        for(z in prevz : nodays)
        {
          if((z == nodays) && (y == endmn) && (x == noyrs))
          {
            if(!repeats)
            {
              print('No new data')
              repeats = 1
            }
            next
          }
          repeats = 0
          print(paste('New data, calculating digests',days1[z]))
          pathdays1 = paste(pathmon1,days1[z],sep = "/")
          path2Gfinal1 = paste(path2Gmon1,days1[z],sep="/")
          pathdays2 = paste(pathmon2,days2[z],sep = "/")
          path2Gfinal2 = paste(path2Gmon2,days2[z],sep="/")
          secondGenData(pathdays1,path2Gfinal1,1)
          secondGenData(pathdays2,path2Gfinal2,2)
          thirdGenData(path2Gfinal1,path3Gfinal1)
          thirdGenData(path2Gfinal2,path3Gfinal2)
          print('Sending mail')
          daysAlive = daysAlive+1
          sendMail(c(path2Gfinal1,path2Gfinal2))
        }
      }
    }
  }
  prevx = x
  prevy = y
  prevz = z
  Sys.sleep(3600)
}
print(paste('Exited for some reason x y z values are'),x,y,z)
