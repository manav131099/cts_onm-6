source('/home/admin/CODE/common/math.R')
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}
rf = function(x)
{
  return(format(round(x,2),nsmall=2))
}
rf1 = function(x)
{
  return(format(round(x,1),nsmall=1))
}
rf3 = function(x)
{
  return(format(round(x,3),nsmall=3))
}
prepareSumm = function(dataread)
{
	APPENDWARNING <<- 0
	da = nrow(dataread)
	daPerc = round(da/14.4,1)
  thresh = 5/1000
	gsi1=gsi2=tambsh=tambmxsh=tambmnsh=tamb=tambmx=tambmn=hamb=hambmx=hambmn=NA
  hambsh=hambmxsh=hambmnsh=tsi=tsish=tsimx=tsimxsh=tsimn=tsimnsh=NA

	dataread2 = dataread[complete.cases(dataread[,3]),3]
	if(length(dataread2))
		gsi1 = sum(dataread[complete.cases(dataread[,3]),3])/60000
	dataread2 = dataread[complete.cases(dataread[,5]),5]
	if(length(dataread2))
		gsi2 = sum(dataread[complete.cases(dataread[,5]),5])/60000
#  gsi2 = sum(dataread[complete.cases(dataread[,4]),4])/60000
#  gsismp = sum(dataread[complete.cases(dataread[,5]),5])/60000
  subdata = dataread[complete.cases(dataread[,3]),]
  subdata = subdata[as.numeric(subdata[,3]) > thresh,]
	
	dataread2 = subdata[complete.cases(subdata[,6]),6]
	if(length(dataread2))
	{
  	tambsh = mean(dataread2)
		tambmxsh = max(dataread2)
		tambmnsh = min(dataread2) 
	}
	dataread2 = subdata[complete.cases(subdata[,4]),4]
	if(length(dataread2))
	{
  	hambsh = mean(dataread2)
		hambmxsh = max(dataread2)
		hambmnsh = min(dataread2)
	}
	dataread2 = subdata[complete.cases(subdata[,7]),7]
	if(length(dataread2))
	{
  	tsish = mean(dataread2)
		tsimxsh = max(dataread2)
		tsimnsh = min(dataread2)
	}
	
	dataread2 = dataread[complete.cases(dataread[,6]),6]
	if(length(dataread2))
	{
  	tamb = mean(dataread2)
		tambmx = max(dataread2)
		tambmn = min(dataread2) 
	}
	dataread2 = dataread[complete.cases(dataread[,4]),4]
	if(length(dataread2))
	{
  	hamb = mean(dataread2)
		hambmx = max(dataread2)
		hambmn = min(dataread2)
	}
	dataread2 = dataread[complete.cases(dataread[,7]),7]
	if(length(dataread2))
	{
  	tsi = mean(dataread2)
		tsimx = max(dataread2)
		tsimn = min(dataread2)
	}

  
 	 gsirat = gsi1 / gsi2
#  smprat = gsismp / gsi2

	Eac11 = Eac21 =LastR1=LastT1=Eac12 = Eac22 = Eac13 = Eac23 = LastR2 = LastT2 = LastR3 = LastT3 = NA
	
	dataread2 = dataread[complete.cases(dataread[,21]),21]
	if(length(dataread2))
	{
		Eac11 = sum(dataread[complete.cases(dataread[,21]),21])/-60
	}
	dataread2 = dataread[complete.cases(dataread[,36]),36]
	if(length(dataread2))
	{
  	Eac21 = as.numeric(dataread2[length(dataread2)])  - as.numeric(dataread2[1])
		LastR1 = dataread2[length(dataread2)]
		LastT1 = as.character(dataread[complete.cases(dataread[,36]),1])
		print("######################")
		print(LastT1)
		print("######################")
		LastT1 = LastT1[length(LastT1)]
	}
	
	dataread2 = dataread[complete.cases(dataread[,68]),68]
	if(length(dataread2))
	{
		Eac12 = sum(dataread[complete.cases(dataread[,68]),68])/-60
	}
	dataread2 = dataread[complete.cases(dataread[,83]),83]
	if(length(dataread2))
	{
  	Eac22 = as.numeric(dataread2[length(dataread2)])  - as.numeric(dataread2[1])
		LastR2 = dataread2[length(dataread2)]
		LastT2 = as.character(dataread[complete.cases(dataread[,83]),1])
		print("######################")
		print(LastT2)
		print("######################")
		LastT2 = LastT2[length(LastT2)]
	}
	
	dataread2 = dataread[complete.cases(dataread[,115]),115]
	if(length(dataread2))
	{
		Eac13 = sum(dataread[complete.cases(dataread[,115]),115])/-60
	}
	dataread2 = dataread[complete.cases(dataread[,130]),130]
	if(length(dataread2))
	{
  	Eac23 = as.numeric(dataread2[length(dataread2)])  - as.numeric(dataread2[1])
		LastR3 = dataread2[length(dataread2)]
		LastT3 = as.character(dataread[complete.cases(dataread[,130]),1])
		print("######################")
		print(LastT3)
		print("######################")
		LastT3 = LastT3[length(LastT3)]
	}

	FullSiteProd = Eac11 + Eac12 + Eac13
	FullSiteProd2 = Eac21 + Eac22 + Eac23
	
	FullSiteYld=(FullSiteProd / 4053.3)
	FullSitePR=(FullSiteYld * 100 /gsi1)
	FullSitePRsi=(FullSiteYld * 100/ gsi2)
	
	FullSiteYld2=(FullSiteProd2 / 4053.3)
	FullSitePR2=(FullSiteYld2 * 100 / gsi1)
	FullSitePRsi2=(FullSiteYld2 * 100 / gsi2)
	
	dateAc = NA

	if(nrow(dataread))
		dateAc = substr(dataread[1,1],1,10)

  datawrite = data.frame(Date = dateAc,
	                       PtsRec = rf(da),
												 GPy = rf(gsi1), 
												 GSi = rf(gsi2),
												 GRat = rf3(gsirat),
												 Tamb = rf1(tamb),
												 TambMx = rf1(tambmx),
												 TambMn = rf1(tambmn),
												 TambSH = rf1(tambsh),
												 TambMxSH = rf1(tambmxsh),
												 TambMnSH = rf1(tambmnsh),
												 Hamb = rf1(hamb),
												 HambMx = rf1(hambmx),
												 HambMn = rf1(hambmn),
												 HambSH = rf1(hambsh),
												 HambMxSH = rf1(hambmxsh),
												 HambMnSH = rf1(hambmnsh),
												 TSi = rf1(tsi),
												 TSiMx = rf1(tsimx),
												 TSiMn = rf1(tsimn),
												 TSiSH = rf1(tsish),
												 TSiMxSH = rf1(tsimxsh),
												 TSiMnSH = rf1(tsimnsh),
												 FullSite = rf(FullSiteProd),
												 FullSiteYld = rf(FullSiteYld),
												 FullSitePR = rf1(FullSitePR),
												 FullSitePRSi = rf1(FullSitePRsi),
												 Eac11 = rf(Eac11),
												 Eac21 = rf(Eac21),
												 LastR1 = LastR1,
												 LastT1 = LastT1,
												 Eac12 = rf(Eac12),
												 Eac22 = rf(Eac22),
												 LastR2 = LastR2,
												 LastT2 = LastT2,
												 Eac13 = rf(Eac13),
												 Eac23 = rf(Eac23),
												 LastR3 = LastR3,
												 LastT3 = LastT3,
												 FillSite2 = rf(FullSiteProd2),
												 FullSiteYld2 = rf(FullSiteYld2),
												 FullSitePR2 = rf1(FullSitePR2),
												 FullSitePR2Si=rf1(FullSitePRsi2),
												 DA = rf1(daPerc),
												 stringsAsFactors=F
												)
  datawrite
}

rewriteSumm = function(datawrite)
{
  df = datawrite
  df
}

path = "/home/admin/Dropbox/Cleantechsolar/1min/[729]"
pathwrite = "/home/admin/Dropbox/Second Gen/[MY-006S]"
checkdir(pathwrite)
years = dir(path)
x=y=z=1
for(x in 1 : length(years))
{
  pathyear = paste(path,years[x],sep="/")
  writeyear = paste(pathwrite,years[x],sep="/")
  checkdir(writeyear)
  months = dir(pathyear)
  for(y in  1: length(months))
  {
    pathmonth = paste(pathyear,months[y],sep="/")
    writemonth = paste(writeyear,months[y],sep="/")
    checkdir(writemonth)
    days = dir(pathmonth)
    sumfilename = paste("[MY-006S] ",substr(months[y],3,4),substr(months[y],6,7),".txt",sep="")
    for(z in 1 : length(days))
    {
      dataread = read.table(paste(pathmonth,days[z],sep="/"),sep="\t",header = T)
      datawrite = prepareSumm(dataread)
      datasum = rewriteSumm(datawrite)
			currdayw = gsub("729","MY-006S",days[z])
      write.table(datawrite,file = paste(writemonth,currdayw,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
      {
        if(!file.exists(paste(writemonth,sumfilename,sep="/")) || (x == 1 && y == 1 && z==1))
        {
          write.table(datasum,file = paste(writemonth,sumfilename,sep="/"),append = F,sep="\t",row.names = F,col.names = T)
        }
        else 
        {
          write.table(datasum,file = paste(writemonth,sumfilename,sep="/"),append = T,sep="\t",row.names = F,col.names = F)
        }
      }
    }
  }
}
