import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys

tz = pytz.timezone('Asia/Bangkok')
path='/home/admin/Dropbox/Gen 1 Data/'
mailpath='/home/admin/CODE/Send_mail/mail_recipients.csv'
stations=['[TH-010L]', '[TH-011L]', '[TH-012L]', '[TH-013L]', '[TH-014L]', '[TH-015L]', '[TH-016L]', '[TH-017L]', '[TH-018L]', '[TH-019L]', '[TH-020L]', '[TH-021L]', '[TH-022L]', '[TH-023L]', '[TH-024L]', '[TH-025L]', '[TH-026L]', '[TH-027L]', '[TH-028L]']
substations=[['WMS_3_Pyranometer', 'MFM_1_PV Meter'], ['WMS_1_Pyranometer', 'MFM_1_PV meter'], ['WMS_5_Pyranometer', 'MFM_2_PV Meter'], ['WMS_5_Pyranometer', 'MFM_1_PV Meter'], ['WMS_5_Pyranometer', 'MFM_1_PV Meter'], [ 'WMS_4_Pyranometer','MFM_2_PV Meter',], ['WMS_5_Pyranometer', 'MFM_1_PV Meter'], ['WMS_5_Pyranometer', 'MFM_1_PV Meter'], ['WMS_5_Pyranometer', 'MFM_1_PV Meter'], ['WMS_5_Pyranometer', 'MFM_1_PV Meter'], ['WMS_3_Pyranometer', 'MFM_1_PV Meter'], ['WMS_5_Pyranometer', 'MFM_1_PV Meter'], ['WMS_5_Pyranometer', 'MFM_1_PV Meter'], ['WMS_5_Pyranometer', 'MFM_1_PV Meter'], ['WMS_1_Pyranometer','MFM_2_PV MFM-2', 'MFM_1_PV MFM-1'], ['WMS_5_Pyranometer', 'MFM_1_PV Meter'], ['WMS_3_Pyranometer', 'MFM_1_PV Meter'], ['WMS_5_Pyranometer', 'MFM_1_PV Meter'], ['WMS_5_Pyranometer', 'MFM_2_PV Meter']]
alarm=[datetime.datetime.now(tz)]*19
alarm2=[datetime.datetime.now(tz)]*19
flag=[0]*19
flag2=[0]*19
def sendmail(station,type,data,rec):
    SERVER = "smtp.office365.com"
    FROM = 'operations@cleantechsolar.com'
    recipients = rec# must be a list
    TO=", ".join(recipients)
    if(type=='irr'):
        SUBJECT = station+' Pyranometer Irradiance Alarm'
    else:
        SUBJECT = station+' Power Trip Alarm'
    text2='Last Timestamp Read: '+str(data) +'\n\n'
    TEXT = text2
    # Prepare actual message
    message = "From:"+FROM+"\nTo:"+TO+"\nSubject:"+ SUBJECT +"\n\n"+TEXT
    # Send the mail
    import smtplib
    server = smtplib.SMTP(SERVER)
    server.starttls()
    server.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
    server.sendmail(FROM, recipients, message)
    server.quit()
    from twilio.rest import TwilioRestClient
    account = "ACcac80891fc3a1edb14f942975a0a8939"
    token = "d39acf2278b2194b59aa15aa9e3a2072"
    client = TwilioRestClient(account, token)
    smsrec=['+918310792948','+66870447993','+66928953265']
    for t in smsrec:
        message = client.messages.create(to="+"+str(int(t)), from_="+12402930809",body=SUBJECT +"\n\n"+TEXT)

def rectolist(station,path):
    df=pd.read_csv(path)
    df2=df[['Recipients',station]]
    a=df2.loc[df2[station] == 1]['Recipients'].tolist()
    return a

starttime=time.time()
while(1):
    date_now=datetime.datetime.now(tz).strftime('%Y-%m-%d')
    print(datetime.datetime.now(tz).hour)
    if(datetime.datetime.now(tz).hour>7 and datetime.datetime.now(tz).hour<18):
        for j,i in enumerate(stations):
            pow2=[30]
            print(i)
            temp=substations[j]
            for index,k in enumerate(temp):
                df=pd.read_csv(path+i+'/'+date_now[0:4]+'/'+date_now[0:7]+'/'+k+'/'+i+'-'+k[0:3]+k[4]+'-'+date_now[0:10]+'.txt',sep='\t')
                if(index==0):
                    df_irr=df[['OTI_avg']]
                    irr=df_irr.tail(6).sum()
                    print('irr is',irr[0])
                elif(index==1):
                    df_power=df[['W_avg']]
                    pow1=df_power.tail(6).sum()
                    print('Pow 1 is',pow1[0])
                elif(index==2):
                    df_power=df[['W_avg']]
                    pow2=df_power.tail(6).sum()
                    print('Pow 2 is',pow2[0])
                date=df.tail(1)['ts'].values

            #Irradiance Alarm
            if(i!='[TH-012L]' and i!='[TH-027L]'):
                if(irr[0]<1 and flag[j]==0):
                    recipients=rectolist(i[1:-1]+'_Mail',mailpath)
                    sendmail(i,'irr',date[0],recipients)
                    alarm[j]=datetime.datetime.now(tz)
                    flag[j]=1
                elif(irr[0]<1 and flag[j]==1):
                    diff=datetime.datetime.now(tz)-alarm[j]
                    if((diff.seconds)//60>120):
                        alarm[j]=datetime.datetime.now(tz)
                        recipients=rectolist(i[1:-1]+'_Mail',mailpath)
                        sendmail(i,'irr',date[0],recipients)
                else:
                    flag[j]==0
            #Power Alarm  
            if(((pow1[0]<1 and irr[0]>1) or (pow2[0]<1 and irr[0]>1) or (flag[j]==1 and pow1[0]<1)) and flag2[j]==0):
                recipients=rectolist(i[1:-1]+'_Mail',mailpath)
                sendmail(i,'pow',date[0],recipients)
                alarm2[j]=datetime.datetime.now(tz)
                print(flag2[j])
                flag2[j]=1
                print(flag2[j])
            elif(((pow1[0]<1 and irr[0]>1) or (pow2[0]<1 and irr[0]>1) or (flag[j]==1 and pow1[0]<1)) and flag2[j]==1):
                diff=datetime.datetime.now(tz)-alarm2[j]
                if((diff.seconds)//60>120):
                    alarm2[j]=datetime.datetime.now(tz)
                    recipients=rectolist(i[1:-1]+'_Mail',mailpath)
                    sendmail(i,'pow',date[0],recipients)
            else:
                flag2[j]==0
    time.sleep(1800.0 - ((time.time() - starttime) % 1800.0))