import requests, json
import requests.auth
import pandas as pd
import numpy as np
import datetime
import os
import re 
import time
import shutil
import pytz
import sys
import ast
import math
import logging
import pyodbc


tz = pytz.timezone('Asia/Calcutta')
tz2 = pytz.timezone('Asia/Singapore')
readpath='/home/admin/Dropbox/Fourth_Gen/'
pathflexi='/home/admin/Dropbox/FlexiMC_Data/Fourth_Gen/'
writepath='/home/admin/Dropbox/Lifetime/Gen-1/'
writepath2='/home/admin/Dropbox/Lifetime/Gen-1/'
lifetimepath='/home/pranav/Lifetime3.csv'

#Add 62 later

server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 13 for SQL Server}'

tz = pytz.timezone('Asia/Calcutta')
tz2 = pytz.timezone('Asia/Singapore')


def convert_names(arr,n):
    arr=arr[2:-2]
    temp=[]
    for index,i in enumerate(arr):
        if(index<n):
            x=i.split('.')
            if(len(x)>2):
                temp2=[]
                for i in x:
                    temp3='LastR-'
                    if('LastRead' not in i):
                        temp2.append(i)
                temp2='.'.join(temp2)
                temp.append(temp3+temp2)
            else:
                temp.append('LastR-'+x[0])
        else:
            x=i.split('.')
            if(len(x)>2):
                temp2=[]
                for i in x:
                    temp3='Eac-'
                    if('Eac2' not in i):
                        temp2.append(i)
                temp2='.'.join(temp2)
                temp.append(temp3+temp2)
            else:
                temp.append('Eac-'+x[0])
    return temp

def convert_names2(df,n):
    df.insert(2, "GHI-Flag", 0)
    cols=df.columns.tolist()
    for m in range(n):
        df.insert(2+(2*(m+1)), cols[3+m]+"-Flag", 0)
    for m in range(n):
        df.insert(2+2*n+(2*(m+1)),cols[3+n+m]+'-Flag',0)
    df2=df.copy()
    df2['Master-Flag']=0
    return df2



#Initialising Arrays
connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
SQL_Query = pd.read_sql_query(
'''SELECT TOP (1000) [Station_Id]
      ,[Station_Name]
      ,[Station_Columns]
      ,[Station_Irradiation_Center]
      ,[Station_No_Meters]
      ,[Provider]
  FROM [dbo].[stations] ''', connStr)
dflifetime = pd.DataFrame(SQL_Query, columns=['Station_Id','Station_Name','Station_Columns','Station_Irradiation_Center','Station_No_Meters','Provider'])
connStr.close()
dflifetime=dflifetime.loc[dflifetime['Provider']=='Locus',]
stations=dflifetime['Station_Name'].dropna().tolist()
for index,i in enumerate(stations):
    stations[index]=str('['+i.strip()+'L]')
irr=dflifetime['Station_Irradiation_Center'].fillna('NA').tolist()
for index,i in enumerate(irr):
    if(i=='NA'):
        continue
    irr[index]=str('['+i.strip()+'L]')
nometers=dflifetime['Station_No_Meters'].dropna().tolist()
nometers=list(map(int, nometers))
metercols=[]
b=dflifetime['Station_Columns'].dropna().tolist()
for i in b:
    temp=ast.literal_eval(i)
    metercols.append(temp)


#First Time Creation
for index,n in enumerate(stations):
    print(n)
    if(os.path.exists(writepath+n[1:7]+"-LT.txt")):
        print('ggg')
        pass
    else:
        df=pd.read_csv(readpath+n+'/'+n+'-lifetime.txt',sep='\t',engine='python')
        if(irr[index]=='NA'):
            df_write=df[metercols[index]]
            col_names=['Date','GHI']+convert_names(metercols[index],nometers[index])+['LastT','DA']
            df_write.columns=col_names
            df_write=convert_names2(df_write,nometers[index])
            df_write=df_write.reset_index(drop=True)
            df_write.to_csv(writepath+n[1:7]+"-LT.txt",sep='\t',mode='a',index=False,header=True)
        else:
            dfdate=df[['Date']]
            if(n=='[MY-008L]'):
                df_irr=pd.read_csv(writepath2+irr[index]+'.txt',sep='\t',engine='python')
            else:
                df_irr=pd.read_csv(readpath+irr[index]+'/'+irr[index]+'-lifetime.txt',sep='\t',engine='python')
            df_irr=df_irr[metercols[index][0:2]]
            finaldf=pd.merge(dfdate, df_irr, on=['Date'],sort=False,how='left')
            df_write=df[[metercols[index][0]]+metercols[index][2:]]
            col_names=['Date','GHI']+convert_names(metercols[index],nometers[index])+['LastT','DA']
            df_merge=pd.merge(finaldf, df_write, on=['Date'],sort=False,how='left')
            df_merge.columns=col_names
            df_merge=convert_names2(df_merge,nometers[index])
            df_merge=df_merge.reset_index(drop=True)
            df_merge.to_csv(writepath+n[1:7]+"-LT.txt",sep='\t',mode='a',index=False,header=True)


#Live Process
while(1):
    for index,n in enumerate(stations):
        try:
            print(n)
            if(n=='[MY-008L]'):
                timenow=datetime.datetime.now(tz2)+datetime.timedelta(hours=-1)
            else:
                timenow=datetime.datetime.now(tz)+datetime.timedelta(hours=-1)
            timenowstr=str(timenow)
            timenowdate=str(timenow.date())
            print(timenowdate)
            df=pd.read_csv(readpath+n+'/'+n+'-lifetime.txt',sep='\t',engine='python')
            try:
                df2=pd.read_csv(writepath+n[1:7]+"-LT.txt",sep='\t',engine='python')
            except:
                logging.exception('msg')
                pass
            if(irr[index]=='NA'):
                df_write=df[metercols[index]]
                col_names=['Date','GHI']+convert_names(metercols[index],nometers[index])+['LastT','DA']
                df_write.columns=col_names
                df_write=convert_names2(df_write,nometers[index])
                for i in range(1,15):
                    df2=pd.read_csv(writepath+n[1:7]+"-LT.txt",sep='\t',engine='python')
                    pastdate=timenow+datetime.timedelta(days=-i)
                    pastdatestr=str(pastdate)
                    pastdatestr2=pastdate.strftime("%d-%m-%Y")
                    if(((df2['Date']==pastdatestr[0:10]).any() or df2['Date']==pastdatestr2[0:10]).any()):
                        pass
                    else:
                        df3=df_write.loc[(df_write['Date']==pastdatestr[0:10]) | (df_write['Date']==pastdatestr2[0:10])]
                        if(df3.empty):
                            pass
                        else:
                            df3.to_csv(writepath+n[1:7]+"-LT.txt",sep='\t',mode='a',index=False,header=False)
                if((df2['Date']==timenowdate).any()):
                    df3=df_write.loc[df_write['Date']==timenowdate]
                    vals=df3.values.tolist()
                    print(vals)
                    print(df2.columns.tolist())
                    df2.loc[df2['Date']==timenowdate,df2.columns.tolist()]=vals[0]
                    df2.to_csv(writepath+n[1:7]+"-LT.txt",sep='\t',mode='w',index=False,header=True)
                else:
                    df3=df_write.loc[df_write['Date']==timenowdate]
                    if(df3.empty):
                        print("Waiting")
                    else:
                        df3.to_csv(writepath+n[1:7]+"-LT.txt",sep='\t',mode='a',index=False,header=False)
            else:
                dfdate=df[['Date']]
                if(n=='[MY-008L]'):
                    df_irr=pd.read_csv(writepath2+irr[index]+'.txt',sep='\t',engine='python')
                else:
                    df_irr=pd.read_csv(readpath+irr[index]+'/'+irr[index]+'-lifetime.txt',sep='\t',engine='python')
                df_irr=df_irr[metercols[index][0:2]]
                finaldf=pd.merge(dfdate, df_irr, on=['Date'],sort=False,how='left')
                df_write=df[[metercols[index][0]]+metercols[index][2:]]
                col_names=['Date','GHI']+convert_names(metercols[index],nometers[index])+['LastT','DA']
                df_merge=pd.merge(finaldf, df_write, on=['Date'],sort=False,how='left')
                df_merge.columns=col_names
                df_merge=convert_names2(df_merge,nometers[index])
                for i in range(1,15):
                    df2=pd.read_csv(writepath+n[1:7]+"-LT.txt",sep='\t',engine='python')
                    pastdate=timenow+datetime.timedelta(days=-i)
                    pastdatestr=str(pastdate)
                    pastdatestr2=pastdate.strftime("%d-%m-%Y")
                    if(((df2['Date']==pastdatestr[0:10]).any() or df2['Date']==pastdatestr2[0:10]).any()):
                        pass
                    else:
                        df3=df_merge.loc[(df_merge['Date']==pastdatestr[0:10]) | (df_merge['Date']==pastdatestr2[0:10])]
                        if(df3.empty):
                            pass
                        else:
                            df3.to_csv(writepath+n[1:7]+"-LT.txt",sep='\t',mode='a',index=False,header=False)
                if((df2['Date']==timenowdate).any()):
                    df3=df_merge.loc[df_merge['Date']==timenowdate]
                    vals=df3.values.tolist()
                    df2.loc[df2['Date']==timenowdate,:]=vals[0]
                    df2.to_csv(writepath+n[1:7]+"-LT.txt",sep='\t',mode='w',index=False,header=True)
                else:
                    df3=df_merge.loc[df_merge['Date']==timenowdate]
                    if(df3.empty):
                        print("Waiting")
                    else:
                        df3.to_csv(writepath+n[1:7]+"-LT.txt",sep='\t',mode='a',index=False,header=False)
        except:
            print("Issue")
            logging.exception("message")
    time.sleep(300)







