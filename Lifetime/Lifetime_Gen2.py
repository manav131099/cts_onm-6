import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys
import ast
import pyodbc
#Notes
#Removed Delete Function to handle SG-003 Live.
#SQL Server Stuff

server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 13 for SQL Server}'

def delete():
    cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = cnxn.cursor()
    with cursor.execute("IF OBJECT_ID('[Cleantech Meter Readings].dbo.Stations_Data', 'U') IS NOT NULL TRUNCATE TABLE [Cleantech Meter Readings].dbo.Stations_Data"): 
        print('Successfuly Deleted cause already present!') 
#Cols needed are Date,O&MR Refrence,GHI,Last-R-MFM and Eac-MFM

tz = pytz.timezone('Asia/Calcutta')
path='/home/admin/Dropbox/Lifetime/Gen-1/'
path2='/home/pranav/Dropbox/Stations_Data555.txt'
#Initialising
columns=['Date','GHI','GHI-Flag','LastR-MFM','LastR-Flag','Eac-MFM','Eac-Flag','Reference','Station_Id']
cols=['Seris_Station_Name','Flexi_Station_Name','Locus_Station_Name','Seris_Meter_Reference','Flexi_Meter_Reference','Locus_Meter_Reference','Seris_No_Meters','Flexi_No_Meters','Locus_No_Meters','Seris_Capacities','Flexi_Capacities','Locus_Capacities']

connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
SQL_Query = pd.read_sql_query(
'''SELECT TOP (1000) [Meter_id]
      ,[Station_Id],[Country_Id]
      ,[Reference]
      ,[Salesorce_Reference]
      ,[Capacity]
  FROM [dbo].[Meters]''', connStr)
dflifetime = pd.DataFrame(SQL_Query, columns=['Meter_id','Station_Id','Country_Id','Reference','Salesorce_Reference','Capacity'])
connStr.close()


stns=[]
meterref=[]
meterno=[]
capacities=[]
mtr_id=[]
stn_id=[]
cntry_id=[]
g=dflifetime.groupby('Station_Id')
stns_id=g.groups.keys()
print(stns_id)
for i in stns_id:
    if(i==53 or i==64 or i==66 or i==43):                  #Issues
        continue
    temp=[]
    temp2=[]
    temp3=[]
    temp4=[]
    temp5=[]
    mtr_grps=g.get_group(i)
    mtr_data=mtr_grps.values.tolist()
    meterno.append(len(mtr_data))
    for j in mtr_data:
        temp3.append(j[0])
        temp4.append(j[1])
        temp5.append(j[2])
        temp.append(str(j[3]))
        temp2.append(j[5])
    stn_id.append(temp4)
    mtr_id.append(temp3)
    cntry_id.append(temp5)
    meterref.append(temp)
    capacities.append(temp2)
    stns.append(str(temp[0][:-1])+'-LT.txt')

print(stns)
print(mtr_id)
#Hardcoded 
#stns.append('SG-006-LT.txt')
#meterref.append(['SG-006A','SG-006B','SG-006C','SG-006D','SG-006E'])
#meterno.append(5)
#capacities.append([163.8,100.8,176.4,50.4,504])


connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
for m,n in enumerate(stns):
    SQL_Query = pd.read_sql_query('SELECT TOP (1) [Station_Id] FROM [dbo].[Stations_Data] Where Station_Id='+str(stn_id[m][0]), connStr)
    dflifetime = pd.DataFrame(SQL_Query, columns=['Station_Id'])
    if(dflifetime.empty):
        print(n)
        df=pd.read_csv(path+n,sep='\t')
        cols=df.columns.tolist()
        print(cols)
        for i in range(meterno[m]):
            #Unpivoting
            temp=[]
            temp2=[]
            temp=cols[0:3]
            temp.append(cols[3+2*i])
            temp.append(cols[4+2*i])
            temp.append(cols[3+meterno[m]*2+2*i])
            temp.append(cols[2+2*meterno[m]+(2*(i+1))])
            for temp3 in temp:
                temp2.append(temp3)
            df2=df[temp].copy()
            df2['MeterReference']=meterref[m][i]
            df2['Station_Id']=meterref[m][i][:-1]
            df2.columns=columns
            df2=df2.fillna(0)
            print(df2['Eac-MFM'])
            print(df2['Eac-MFM'] > 10*capacities[m][i])
            df2.loc[df2['Eac-MFM'] > 10*capacities[m][i], 'Eac-MFM'] = 0
            df2.loc[df2['Eac-MFM'] < 0, 'Eac-MFM'] = 0
            print(df2)
            cursor = connStr.cursor()
            for index,row in df2.iterrows():
                if(row['GHI'])!=0:
                    try:
                        print(row)
                        with cursor.execute("INSERT INTO dbo.Stations_Data([Date],[GHI],[GHI-Flag],[LastR-MFM],[LastR-Flag],[Eac-MFM],[Eac-Flag],[Yield],[PR],[Station_Id],[Meter_Id],[Country_Id]) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", row['Date'], row['GHI'], row['GHI-Flag'], row['LastR-MFM'], row['LastR-Flag'],row['Eac-MFM'], row['Eac-Flag'],float(row['Eac-MFM'])/float(capacities[m][i]),((float(row['Eac-MFM'])/float(capacities[m][i]))/row['GHI'])*100,stn_id[m][i],mtr_id[m][i],cntry_id[m][i]):
                            pass
                        connStr.commit()
                    except Exception as e:
                        print(e)
                        pass
                else:
                    try:
                        with cursor.execute("INSERT INTO dbo.Stations_Data([Date],[GHI],[GHI-Flag],[LastR-MFM],[LastR-Flag],[Eac-MFM],[Eac-Flag],[Yield],[PR],[Station_Id],[Meter_Id],[Country_Id]) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", row['Date'], row['GHI'], row['GHI-Flag'], row['LastR-MFM'], row['LastR-Flag'],row['Eac-MFM'], row['Eac-Flag'],float(row['Eac-MFM'])/float(capacities[m][i]),0,stn_id[m][i],mtr_id[m][i],cntry_id[m][i]):
                            pass
                        connStr.commit()
                    except Exception as e:
                        print(e)
                        pass
            cursor.close()
connStr.close()



while(1): #Live
    #Time setting
    print('LLIVVVVVEEE!!!!!!')
    connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
    timenow=datetime.datetime.now(tz)+datetime.timedelta(hours=-.1)
    timenowstr=str(timenow)
    timenowdate=str(timenow.date())
    print(timenowdate)
    #Reading
    for m,n in enumerate(stns):
        print(n)
        df=pd.read_csv(path+n,sep='\t')
        cols=df.columns.tolist()
        #Historical
        for d in range(1,15):
            pastdate=timenow+datetime.timedelta(days=-d)
            pastdatestr=str(pastdate)
            SQL_Query=pd.read_sql_query("SELECT TOP (1) [Meter_Id] FROM [dbo].[Stations_Data] Where [Date]='"+pastdatestr[0:10]+"' AND [Meter_Id]="+str(mtr_id[m][0]), connStr)
            df_prev=pd.DataFrame(SQL_Query, columns=['Meter_Id'])
            if(df_prev.empty):
                print('ABSENT')
                df3=df.loc[(df['Date']==pastdatestr[0:10])]
                if(df3.empty):
                    print('Historical Data with Date not present!')
                else:
                    print('Historical Data with Date present!')
                    for i in range(meterno[m]):
                        temp=[]
                        temp=cols[0:3]
                        temp.append(cols[3+2*i])
                        temp.append(cols[4+2*i])
                        temp.append(cols[3+meterno[m]*2+2*i])
                        temp.append(cols[2+2*meterno[m]+(2*(i+1))])
                        df4=df3[temp].copy()
                        df4['MeterReference']=meterref[m][i]
                        df4['Station_Id']=meterref[m][i][:-1]
                        df4.columns=columns
                        df4=df4.fillna(0)
                        df4.loc[df4['Eac-MFM'] > 10*capacities[m][i], 'Eac-MFM'] = 0
                        df4.loc[df4['Eac-MFM'] < 0, 'Eac-MFM'] = 0
                        cursor = connStr.cursor()
                        for index,row in df4.iterrows():
                            if(row['GHI'])!=0:
                                print("INSERT INTO dbo.Stations_Data([Date],[GHI],[GHI-Flag],[LastR-MFM],[LastR-Flag],[Eac-MFM],[Eac-Flag],[Reference],[Station_Id],[Yield],[PR]) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", row['Date'], row['GHI'], row['GHI-Flag'], row['LastR-MFM'], row['LastR-Flag'],row['Eac-MFM'], row['Eac-Flag'],row['Reference'],row['Station_Id'],float(row['Eac-MFM'])/float(capacities[m][i]),((float(row['Eac-MFM'])/float(capacities[m][i]))/row['GHI'])*100)
                                try:
                                    with cursor.execute("INSERT INTO dbo.Stations_Data([Date],[GHI],[GHI-Flag],[LastR-MFM],[LastR-Flag],[Eac-MFM],[Eac-Flag],[Yield],[PR],[Station_Id],[Meter_Id],[Country_Id]) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", row['Date'], row['GHI'], row['GHI-Flag'], row['LastR-MFM'], row['LastR-Flag'],row['Eac-MFM'], row['Eac-Flag'],float(row['Eac-MFM'])/float(capacities[m][i]),((float(row['Eac-MFM'])/float(capacities[m][i]))/row['GHI'])*100,stn_id[m][i],mtr_id[m][i],cntry_id[m][i]):
                                        pass
                                    connStr.commit()
                                except:
                                    pass
                            else:
                                try:
                                    with cursor.execute("INSERT INTO dbo.Stations_Data([Date],[GHI],[GHI-Flag],[LastR-MFM],[LastR-Flag],[Eac-MFM],[Eac-Flag],[Yield],[PR],[Station_Id],[Meter_Id],[Country_Id]) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", row['Date'], row['GHI'], row['GHI-Flag'], row['LastR-MFM'], row['LastR-Flag'],row['Eac-MFM'], row['Eac-Flag'],float(row['Eac-MFM'])/float(capacities[m][i]),0,stn_id[m][i],mtr_id[m][i],cntry_id[m][i]):
                                        pass
                                    connStr.commit()
                                except:
                                    pass
                        cursor.close()
            else:
                print('PRESENT!')
                df3=df.loc[(df['Date']==pastdatestr[0:10])]
                for i in range(meterno[m]):
                    temp=[]
                    temp=cols[0:3]
                    temp.append(cols[3+2*i])
                    temp.append(cols[4+2*i])
                    temp.append(cols[3+meterno[m]*2+2*i])
                    temp.append(cols[2+2*meterno[m]+(2*(i+1))])
                    df4=df3[temp].copy()
                    df4['MeterReference']=meterref[m][i]
                    df4['Station_Id']=meterref[m][i][:-1]
                    df4.columns=columns
                    df4=df4.fillna(0)
                    df4.loc[df4['Eac-MFM'] > 10*capacities[m][i], 'Eac-MFM'] = 0
                    df4.loc[df4['Eac-MFM'] < 0, 'Eac-MFM'] = 0
                    vals=df4.values.tolist()
                    cursor = connStr.cursor()
                    if(vals[0][1])!=0:
                        try:
                            with cursor.execute("UPDATE [dbo].[Stations_Data] SET [GHI]="+str(vals[0][1])+",[GHI-Flag]="+str(vals[0][2])+",[LastR-MFM]="+str(vals[0][3])+",[LastR-Flag]="+str(vals[0][4])+",[Eac-MFM]="+str(vals[0][5])+",[Eac-Flag]="+str(vals[0][6])+",[Yield]="+str(float(vals[0][5])/float(capacities[m][i]))+",[PR]="+str(((float(vals[0][5])/float(capacities[m][i]))/vals[0][1])*100)+" WHERE [Date]='"+str(vals[0][0])+"' and [Meter_Id]="+str(mtr_id[m][i])):
                                pass
                            connStr.commit()
                        except:
                            pass
                    else:
                        try:
                            with cursor.execute("UPDATE [dbo].[Stations_Data] SET [GHI]="+str(vals[0][1])+",[GHI-Flag]="+str(vals[0][2])+",[LastR-MFM]="+str(vals[0][3])+",[LastR-Flag]="+str(vals[0][4])+",[Eac-MFM]="+str(vals[0][5])+",[Eac-Flag]="+str(vals[0][6])+",[Yield]="+str(float(vals[0][5])/float(capacities[m][i]))+",[PR]=0 WHERE [Date]='"+str(vals[0][0])+"' and [Meter_Id]="+str(mtr_id[m][i])):
                                pass
                            connStr.commit()
                        except:
                            pass
                    cursor.close()
        #Live
        SQL_Query_Live=pd.read_sql_query("SELECT TOP (1) [Meter_Id] FROM [dbo].[Stations_Data] Where [Date]='"+timenowdate[0:10]+"' AND [Meter_Id]="+str(mtr_id[m][0]), connStr)
        df_live=pd.DataFrame(SQL_Query_Live, columns=['Meter_Id'])
        if(df_live.empty):
            print('ABSENT IN GEN 2')
            df3=df.loc[df['Date']==timenowdate]
            if(df3.empty):
                print('Live Data with Date not present!')
            else:
                print('Live Data with Date present!')
                for i in range(meterno[m]):
                    temp=[]
                    temp=cols[0:3]
                    temp.append(cols[3+2*i])
                    temp.append(cols[4+2*i])
                    temp.append(cols[3+meterno[m]*2+2*i])
                    temp.append(cols[2+2*meterno[m]+(2*(i+1))])
                    df4=df3[temp].copy()
                    df4['MeterReference']=meterref[m][i]
                    df4['Station_Id']=meterref[m][i][:-1]
                    df4.columns=columns
                    df4=df4.fillna(0)
                    df4.loc[df4['Eac-MFM'] > 10*capacities[m][i], 'Eac-MFM'] = 0
                    df4.loc[df4['Eac-MFM'] < 0, 'Eac-MFM'] = 0
                    cursor = connStr.cursor()
                    for index,row in df4.iterrows():
                        if(row['GHI'])!=0:
                            try:
                                with cursor.execute("INSERT INTO dbo.Stations_Data([Date],[GHI],[GHI-Flag],[LastR-MFM],[LastR-Flag],[Eac-MFM],[Eac-Flag],[Yield],[PR],[Station_Id],[Meter_Id],[Country_Id]) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", row['Date'], row['GHI'], row['GHI-Flag'], row['LastR-MFM'], row['LastR-Flag'],row['Eac-MFM'], row['Eac-Flag'],float(row['Eac-MFM'])/float(capacities[m][i]),((float(row['Eac-MFM'])/float(capacities[m][i]))/row['GHI'])*100,stn_id[m][i],mtr_id[m][i],cntry_id[m][i]):
                                    pass
                                connStr.commit()
                            except:
                                pass
                        else:
                            print('ZERO!')
                            try:
                                with cursor.execute("INSERT INTO dbo.Stations_Data([Date],[GHI],[GHI-Flag],[LastR-MFM],[LastR-Flag],[Eac-MFM],[Eac-Flag],[Yield],[PR],[Station_Id],[Meter_Id],[Country_Id]) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", row['Date'], row['GHI'], row['GHI-Flag'], row['LastR-MFM'], row['LastR-Flag'],row['Eac-MFM'], row['Eac-Flag'],float(row['Eac-MFM'])/float(capacities[m][i]),0,stn_id[m][i],mtr_id[m][i],cntry_id[m][i]):
                                    pass
                                connStr.commit()
                            except:
                                pass
                    cursor.close()
        else:
            print('PRESENT IN GEN 2!')
            df3=df.loc[df['Date']==timenowdate]
            for i in range(meterno[m]):
                temp=[]
                temp=cols[0:3]
                temp.append(cols[3+2*i])
                temp.append(cols[4+2*i])
                temp.append(cols[3+meterno[m]*2+2*i])
                temp.append(cols[2+2*meterno[m]+(2*(i+1))])
                df4=df3[temp].copy()
                df4['MeterReference']=meterref[m][i]
                df4['Station_Id']=meterref[m][i][:-1]
                df4.columns=columns
                df4=df4.fillna(0)
                df4.loc[df4['Eac-MFM'] > 10*capacities[m][i], 'Eac-MFM'] = 0
                df4.loc[df4['Eac-MFM'] < 0, 'Eac-MFM'] = 0
                vals=df4.values.tolist()
                cursor = connStr.cursor()
                if(vals[0][1])!=0:
                    try:
                        with cursor.execute("UPDATE [dbo].[Stations_Data] SET [GHI]="+str(vals[0][1])+",[GHI-Flag]="+str(vals[0][2])+",[LastR-MFM]="+str(vals[0][3])+",[LastR-Flag]="+str(vals[0][4])+",[Eac-MFM]="+str(vals[0][5])+",[Eac-Flag]="+str(vals[0][6])+",[Yield]="+str(float(vals[0][5])/float(capacities[m][i]))+",[PR]="+str(((float(vals[0][5])/float(capacities[m][i]))/vals[0][1])*100)+" WHERE [Date]='"+str(vals[0][0])+"' and [Meter_Id]="+str(mtr_id[m][i])):
                            pass
                        connStr.commit()
                    except:
                        pass
                else:
                    try:
                        with cursor.execute("UPDATE [dbo].[Stations_Data] SET [GHI]="+str(vals[0][1])+",[GHI-Flag]="+str(vals[0][2])+",[LastR-MFM]="+str(vals[0][3])+",[LastR-Flag]="+str(vals[0][4])+",[Eac-MFM]="+str(vals[0][5])+",[Eac-Flag]="+str(vals[0][6])+",[Yield]="+str(float(vals[0][5])/float(capacities[m][i]))+",[PR]=0 WHERE [Date]='"+str(vals[0][0])+"' and [Meter_Id]="+str(mtr_id[m][i])):
                            pass
                        connStr.commit()
                    except:
                        pass
                cursor.close()
    print("Sleeping")
    connStr.close()
    time.sleep(300)






