rm(list = ls())

# This script assumes path present in list2.txt file starts with /home/admin as base
#So files in /tmp/list2.txt have contents like
# "Dropbox/GIS/" etc.. so that is why we append /home/admin and remove the files
# to populate list2.txt run find Dropbox | grep <some condition> > /tmp/list2.txt
# You need to run the above command from the /home/admin folder
path = "/tmp/list2.txt"

dataread = readLines(path)

for(x in 2 :length(dataread))
{
	pathfull = paste("/home/admin",dataread[x],sep="/")
	cmd = paste('rm -r "',pathfull,'"',sep="")
	print(cmd)
	system(cmd)
}
