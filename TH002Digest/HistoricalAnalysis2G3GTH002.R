system('rm -R "/home/admin/Dropbox/Second Gen/[TH-002X]"')
system('rm -R "/home/admin/Dropbox/Third Gen/[TH-002X]"')

require('mailR')
require('Hmisc')

source('/home/admin/CODE/TH002Digest/TH002MailDigestFunctions.R')
source('/home/admin/CODE/Misc/calcBackLog.R')
path = '/home/admin/Dropbox/Gen 1 Data/[TH-002X]'
writepath2G = '/home/admin/Dropbox/Second Gen/[TH-002X]'
checkdir(writepath2G)

writepath3G = '/home/admin/Dropbox/Third Gen/[TH-002X]'
checkdir(writepath3G)
DAYSALIVE = 0
years = dir(path)
stnnickName2 = "TH-002X"
lastdatemail = lastMailDate(paste('/home/admin/Start/MasterMail/',stnnickName2,'_Mail.txt',sep=""))
stopDate = paste("[TH-002X-M1] ",lastdatemail,".txt",sep="")
ENDCALL=0

for(x in 2 : length(years))
{
  pathyrs = paste(path,years[x],sep="/")
  writepath2Gyr = paste(writepath2G,years[x],sep="/")
  checkdir(writepath2Gyr)
  writepath3Gyr = paste(writepath3G,years[x],sep="/")
  checkdir(writepath3Gyr)
  months = dir(pathyrs)
  startmn = 2
  if(x > 2)
    startmn = 1
  for(y in startmn : length(months))
  {
    pathmonths = paste(pathyrs,months[y],sep="/")
    writepath2Gmon = paste(writepath2Gyr,months[y],sep="/")
    checkdir(writepath2Gmon)
    writepath3Gfinal = paste(writepath3Gyr,"/[TH-002X] ",months[y],".txt",sep="")
    stations = dir(pathmonths)
		#if(length(stations) > 2)
		#{
		#  stations = c(stations[2],stations[3],stations[1])
	#		print('rewordring')
#			print(stations)
	#	}
    pathdays = paste(pathmonths,stations[1],sep="/")
    pathdays2 = paste(pathmonths,stations[2],sep="/")
    pathdays3 = paste(pathmonths,stations[3],sep="/")
    writepath2Gdays = paste(writepath2Gmon,stations[1],sep="/")
    writepath2Gdays2 = paste(writepath2Gmon,stations[2],sep="/")
    writepath2Gdays3 = paste(writepath2Gmon,stations[3],sep="/")
    checkdir(writepath2Gdays)
    checkdir(writepath2Gdays2)
    checkdir(writepath2Gdays3)
    days = dir(pathdays)
    temp = unlist(strsplit(days[1]," "))
    temp = as.Date(substr(temp[2],1,10),"%Y-%m-%d")
    days2 = dir(pathdays2)
		days3 = dir(pathdays3)
		if(length(days) != length(days2) || length(days) != length(days3))
		{
			minlen = min(length(days),length(days2),length(days3))
			days = tail(days,n=minlen)
			days2 = tail(days2,n=minlen)
			days3 = tail(days3,n=minlen)
		}
      for(t in 1 : length(days))
      {
        if(ENDCALL==1)
					break

				{
          if(x == 2 && y == 2 && t == 1)
          {
            DOB = unlist(strsplit(days[t]," "))
            DOB = substr(DOB[2],1,10)
            DOB = as.Date(DOB,"%Y-%m-%d")
          }
          else if(x == length(years) && y == length(months) && t == length(days))
          {
            next
          }
        }
        DAYSALIVE = DAYSALIVE + 1
        writepath2Gfinal = paste(writepath2Gdays,"/",days[t],sep="")
        writepath2Gfinal2 = paste(writepath2Gdays2,"/",days2[t],sep="")
        writepath2Gfinal3 = paste(writepath2Gdays3,"/",days3[t],sep="")
        readpath = paste(pathdays,days[t],sep="/")
        readpath2 = paste(pathdays2,days2[t],sep="/")
        readpath3 = paste(pathdays3,days3[t],sep="/")
				TOTSOLAR <<- 0
				METERCALLED <<- 2 # correct dont change
        df2 = secondGenData(readpath2,writepath2Gfinal2)
				METERCALLED <<- 1 #correct dont change
        df1 = secondGenData(readpath,writepath2Gfinal)
				
				patchLoadData(readpath,readpath2,readpath3)
				
				METERCALLED <<- 3 # correct dont change
        df3 = secondGenData(readpath3,writepath2Gfinal3)
				
				invoiceData = computeInvoice(readpath2)
				print(days[t])
				print(days2[t])
				print(days3[t])
        thirdGenData(writepath2Gfinal,writepath2Gfinal2,
				writepath2Gfinal3,writepath3Gfinal,invoiceData)
				print(writepath3Gfinal)
				if(days[t] == stopDate)
				{
					ENDCALL = 1
				}
      }
			if(ENDCALL == 1)
				break
    }
			if(ENDCALL == 1)
				break
}
print('Historical Analysis Done')
