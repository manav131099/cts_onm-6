import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys
import pyodbc
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as mpatch
import matplotlib.dates as mdates
from scipy import stats

def chkdir(path):
    a=os.path.isdir(path)
    if(a):
        return
    else:
        os.makedirs(path)

stn=sys.argv[1]
end_date=sys.argv[2]


path='/home/admin/Dropbox/Fourth_Gen/['+stn+'L]/['+stn+'L]-lifetime.txt'
path_write='/home/admin/Graphs/'

df=pd.read_csv(path,sep='\t')
temp=[]
cols=df.columns
for i in cols:
    if(i[-4:]=='Yld2' and i[0:3]!='MFM' and i[0:10]!='INVERTER_1'):
        temp.append(i)

df_Yld=df[['Date']+temp].fillna(0).copy()
df_Yld['COV']=((df_Yld.std(axis=1)*100)/df_Yld.mean(axis=1))
df=df_Yld
df=df[df['Date']>'2020-03-09'].dropna()
no_points=len(df)
df=df[df['Date']<=end_date].dropna()
df=df.replace(0, np.nan)
last_7=df.tail(7)
last_30=df.tail(30)
last_60=df.tail(60).copy()
avg_std_7=last_7['COV'].mean()
avg_std_30=last_30['COV'].mean()
avg_std_60=last_60['COV'].mean()
avg_std_lifetime=df['COV'].mean()
plt.rcParams.update({'font.size': 28})
plt.rcParams['axes.facecolor'] = 'gainsboro'
fig = plt.figure(num=None, figsize=(55  , 30))
ax = fig.add_subplot(111)
last_date=last_60['Date'].tail(1).values[0]
first_date=last_60['Date'].head(1).values[0]
dates=last_60['Date'].values.tolist()
df_outlier=last_60[(np.abs(stats.zscore(last_60['COV'])) > 3)]
outlier_dates=df_outlier['Date'].values.tolist()
outlier_values=df_outlier['COV'].values.tolist()
last_60['Date']=pd.to_datetime(last_60['Date'])
last_60.index=last_60['Date']
upsampled=last_60.resample('2H')
interpolated = upsampled.interpolate(method='cubic')
plt.plot(interpolated['COV'],color='black',linewidth=3)
cov_values=last_60['COV'].values.tolist()
#plt.plot(last_60['Date'].tolist(),last_60['COV'].tolist(),color='black',linewidth=3)
plt.xticks(last_60['Date'].tolist(),rotation=90)
plt.xlim([str(datetime.datetime.strptime(first_date,"%Y-%m-%d").date()+datetime.timedelta(days=-1)),last_date])
plt.ylim([0,30])
myFmt = mdates.DateFormatter('%Y-%m-%d')
ax.xaxis.set_major_formatter(myFmt)
ttl = ax.set_title('From '+str(first_date)+' to '+str(last_date), fontdict={'fontsize': 45, 'fontweight': 'medium'})
ttl.set_position([.485, 1.02])
ttl_main=fig.suptitle(stn+' Daily Inverter CoV',fontsize=60)
plt.text(.13, 0.83,'No. of Points: '+str(no_points)+' days\nLast 7-day CoV average [%]: '+str(round(avg_std_7,1))+'\nLast 30-day CoV average [%]: '+str(round(avg_std_30,1))+'\nLast 60-day CoV average [%]: '+str(round(avg_std_60,1))+'\nLifetime CoV average [%]: '+str(round(avg_std_lifetime,1)), fontsize=25, transform=plt.gcf().transFigure,weight='bold',color='white',bbox=dict(facecolor='black', alpha=0.7,pad=8.0))
plt.axhline(y=5, color='green',linewidth=8)
plt.axhline(y=8, color='orange',linewidth=8)
ax.set_ylabel('CoV [%]', fontsize=32)
ln1=None
ln2=None
ln3=None
for index,i in enumerate(cov_values):
    if(i<=5):
        ln1=plt.scatter(dates[index], i, marker='D',color='green',s=300)
    elif(i>5 and i<8):
        ln2=plt.scatter(dates[index], i, marker='s',color='orange',s=300)
    elif(i>=8):
        ln3=plt.scatter(dates[index], i, marker='^',color='red',s=300)
if(ln1!=None and ln2!=None and ln3!=None):
    plt.legend((ln1, ln2, ln3),
            ('Cov [%] <= 5', 'Cov [%] > 5 and CoV [%] < 8', 'Cov [%] >= 8'),
            scatterpoints=1,
            loc='upper right',
            ncol=3,
            fontsize=30)
elif(ln1!=None and ln2!=None):
    plt.legend((ln1, ln2),
            ('Cov [%] <= 5', 'Cov [%] > 5 and CoV [%] < 8'),
            scatterpoints=1,
            loc='upper right',
            ncol=3,
            fontsize=30)
else:
    plt.legend((ln1),
            ('Cov [%] <= 5'),
            scatterpoints=1,
            loc='upper right',
            ncol=3,
            fontsize=30)
for i, label in enumerate(outlier_dates):
    ax.annotate(label, (label, outlier_values[i]+.25),size=25,color='red')
chkdir(path_write+"Graph_Output/"+stn)
chkdir(path_write+"Graph_Extract/"+stn)
fig.savefig(path_write+"Graph_Output/"+stn+'/['+stn+"] Graph "+last_date+" - Inverter CoV.pdf",bbox_inches='tight')
last_60.to_csv(path_write+"Graph_Extract/"+stn+'/['+stn+"] Graph "+last_date+" - Inverter CoV.txt",sep='\t',index=False)