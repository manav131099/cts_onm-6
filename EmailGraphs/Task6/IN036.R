library(MASS)

tempList = list()
myList = list()

yearFiles = list.files(path = "/home/admin/Dropbox/Cleantechsolar/1min/[718]/", all.files = F, full.names = T)
numOfYears = length(yearFiles) - 1

readData = function() {
  numOfFiles = length(files)
  for (x in 1:numOfFiles) {
    #extract data from .txt file
    data = read.table(files[x], header = T, sep = "\t")
    
    #extract data from table
    dataDate = substr(files[x], (nchar(files[x]) - 13), (nchar(files[x]) - 4))
    numOfRows = length(data$Rec_ID)
    DAPercentage = round(100 * (numOfRows / 1440), digits = 1)
    SMPList = data$AvgSMP10
    GsiList = data$AvgGsi00
    SMPVal = Reduce("+",lapply(SMPList, as.numeric))/60000
    GsiVal = Reduce("+",lapply(GsiList, as.numeric))/60000
    ratioVal = SMPVal/GsiVal
    
    strEnergy = as.numeric(data$Act_E.Del_TF_STR[length(data$Act_E.Del_TF_STR)]) - as.numeric(data$Act_E.Del_TF_STR[1])
    ctrEnergy = as.numeric(data$Act_E.Del_TF_CTR[length(data$Act_E.Del_TF_CTR)]) - as.numeric(data$Act_E.Del_TF_CTR[1])
    
    yieldSTR = strEnergy/2255.5
    yieldCTR = ctrEnergy/2255.5
    
    GModList = data$AvgGTI_CTC
    GModVal = Reduce("+",lapply(GModList, as.numeric))/60000
    
    PRStrGMod = 100 * yieldSTR/GModVal
    PRCtrGMod = 100 * yieldCTR/GModVal
    PRStrSMP10 = 100 * yieldSTR/SMPVal
    PRCtrSMP10 = 100 * yieldCTR/SMPVal
    
    ratioYield = yieldSTR/yieldCTR
    
    tempList <<- append(tempList,c(dataDate, DAPercentage, SMPVal, GsiVal, ratioVal, strEnergy, ctrEnergy, yieldSTR, yieldCTR, GModVal, PRStrGMod, PRCtrGMod, PRStrSMP10, PRCtrSMP10, ratioYield))
  }
}

extractData = function() {
  for (m in 1:12) {
    if (m < 10) {
      pathprobe = paste0("/home/admin/Dropbox/Cleantechsolar/1min/[718]/",toString(y),"/",toString(y),"-","0",toString(m))
      if (file.exists(pathprobe)) {
        files <<- list.files(path = pathprobe, all.files = F, full.names = T)
        readData()
        
        #adding info to list
        myList <<- append(myList,tempList)
        tempList <<- list()
      }
      else {
        print(paste0("File ", pathprobe, " does not exist!"))
      }
    }
    else {
      pathprobe = paste0("/home/admin/Dropbox/Cleantechsolar/1min/[718]/",toString(y),"/",toString(y),"-",toString(m))
      if (file.exists(pathprobe)) {
        files <<- list.files(path = pathprobe, all.files = F, full.names = T)
        readData()
        
        #adding info to list
        myList <<- append(myList,tempList)
        tempList <<- list()
      }
      else {
        print(paste0("File ", pathprobe, " does not exist!"))
      }
    }
  }
}

#loop to begin extracting data
for (y in 2018:(2018+numOfYears)) {
  extractData()
}

#combining all data into a matrix
numDataPoints = length(myList)/15
usefulDataMatrix = matrix(nrow = (numDataPoints + 1), ncol = 15)
usefulDataMatrix[1,] = c("Date","Data Availability","Pyranometer","Silicon","Ratio (SMP/Gsi)","EAC String","EAC Central","Yield (String)","Yield (Central)","GMod","String PR (GMod)","Central PR (GMod)","String PR (SMP10)","Central PR (SMP10)","Ratio (Yield)")

for(z in 1:numDataPoints) {
  usefulDataMatrix[(z+1),] = c(unlist(myList[15*(z-1) + 1]), unlist(myList[15*(z-1) + 2]), unlist(myList[15*(z-1) + 3]), unlist(myList[15*(z-1) + 4]), unlist(myList[15*(z-1) + 5]), unlist(myList[15*(z-1) + 6]), unlist(myList[15*(z-1) + 7]), unlist(myList[15*(z-1) + 8]), unlist(myList[15*(z-1) + 9]), unlist(myList[15*(z-1) + 10]), unlist(myList[15*(z-1) + 11]), unlist(myList[15*(z-1) + 12]), unlist(myList[15*(z-1) + 13]), unlist(myList[15*(z-1) + 14]), unlist(myList[15*z]))
}

#save .txt file to the right directory
setwd("/home/admin/Jason/cec intern/results/[IN-036S]")
write.matrix(usefulDataMatrix, "allData.txt", sep = "\t")
pdf("Rplots.pdf",width=11,height=8)
#plot Data Availability
plotDataAvailability = function() {
  #plot time series graphs
  plot(lapply(usefulDataMatrix[2:(length(usefulDataMatrix[,1]) - 1),1],as.Date),usefulDataMatrix[2:(length(usefulDataMatrix[,1]) - 1),2], type = "l", xaxt = "n", xlab = "", ylab = "Data Availability [%]", col = "black", lty = 1, las = 1, cex.lab = 1.3)
  
  #add titles
  title(main = paste("[IN-036S] Data Availability"), line = 2.2,cex.main = 1.5)
  title(main = paste0("From ",usefulDataMatrix[2,1], " to ", usefulDataMatrix[(nrow(usefulDataMatrix) - 1), 1]), line = 0.75, cex.main = 1.2)
  
  #re-lable time-axis
  axis.Date(1, at = seq(from = as.Date(usefulDataMatrix[2,1]), to = as.Date(usefulDataMatrix[(nrow(usefulDataMatrix) - 1), 1]), by = "month"), format = "%b/%y")
  
  #add names
  title(sub=paste0("Cleantech Solar","\n","O&M Team"), adj=0, line=3, font=2, cex.sub = 0.7)
  title(sub=paste0("Dhruv Baid","\n",toString(Sys.Date())), adj=1, line=3, font=2, cex.sub = 0.7)
  
  #get information about data
  averageDA = round(((Reduce("+",lapply(usefulDataMatrix[2:(length(usefulDataMatrix[,1]) - 1),2],as.numeric)))/(length(usefulDataMatrix[,1]) - 1)), digits = 1)
  lifespanSoFar = difftime(as.Date(usefulDataMatrix[(nrow(usefulDataMatrix) - 1), 1]), as.Date(usefulDataMatrix[2,1]), units = c("days"))
  
  #add information about average DA to plot
  text(x = (par("usr")[1] + par("usr")[2])/2 - 30, y = 65, paste0("Average data availability = ",averageDA,"%","\n","Lifetime = ",lifespanSoFar," days (",round(lifespanSoFar/365,digits = 1)," years)"), cex = 0.8, adj = 0)
}
plotDataAvailability()

#filtering out odd values
newList = list()
newUsefulMatrix = matrix()
dataCleaner = function() {
  for (i in 2:length(usefulDataMatrix[,1])) {
    if (toString(usefulDataMatrix[i,6]) != "NaN" && toString(usefulDataMatrix[i,7]) != "NaN" && as.numeric(usefulDataMatrix[i,6]) < 100000 && as.numeric(usefulDataMatrix[i,7]) < 100000 && as.numeric(usefulDataMatrix[i,6]) > -100000 && as.numeric(usefulDataMatrix[i,7]) > -100000 && as.numeric(usefulDataMatrix[i,8]) < 7 && as.numeric(usefulDataMatrix[i,9] < 7) && as.numeric(usefulDataMatrix[i,11]) < 100 && as.numeric(usefulDataMatrix[i,12]) < 100 && as.numeric(usefulDataMatrix[i,13]) < 100 && as.numeric(usefulDataMatrix[i,14]) < 100 && as.numeric(usefulDataMatrix[i,11]) > 0 && as.numeric(usefulDataMatrix[i,12]) > 0 && as.numeric(usefulDataMatrix[i,13]) > 0 && as.numeric(usefulDataMatrix[i,14]) > 0 && as.numeric(usefulDataMatrix[i,15]) > 0.5 && as.numeric(usefulDataMatrix[i,15]) < 1.5) {
      newList <<- append(newList,unlist(usefulDataMatrix[i,]))
    }
    else {
      print("The data is out of range!")
    }
  }
  
  #combining all filtered data into a matrix
  numNewDataPoints = length(newList)/15
  newUsefulDataMatrix <<- matrix(nrow = (numNewDataPoints + 1), ncol = 15)
  newUsefulDataMatrix[1,] <<- c("Date","Data Availability","Pyranometer","Silicon","Ratio (SMP/Gsi)","EAC String","EAC Central","Yield (String)","Yield (Central)","GMod","String PR (GMod)","Central PR (GMod)","String PR (SMP10)","Central PR (SMP10)","Ratio (Yield)")
  
  for(z in 1:numNewDataPoints) {
    newUsefulDataMatrix[(z+1),] <<- c(unlist(newList[15*(z-1) + 1]), unlist(newList[15*(z-1) + 2]), unlist(newList[15*(z-1) + 3]), unlist(newList[15*(z-1) + 4]), unlist(newList[15*(z-1) + 5]), unlist(newList[15*(z-1) + 6]), unlist(newList[15*(z-1) + 7]), unlist(newList[15*(z-1) + 8]), unlist(newList[15*(z-1) + 9]), unlist(newList[15*(z-1) + 10]), unlist(newList[15*(z-1) + 11]), unlist(newList[15*(z-1) + 12]), unlist(newList[15*(z-1) + 13]), unlist(newList[15*(z-1) + 14]), unlist(newList[15*z]))
  }
}
dataCleaner()

#save .txt file to the right directory
setwd("/home/admin/Jason/cec intern/results/[IN-036S]")
write.matrix(newUsefulDataMatrix, "allDataFiltered.txt", sep = "\t")

#plot side-by-side bar plot for both counters
plotEAC = function() {
  myPlot = barplot(rbind(unlist(lapply(newUsefulDataMatrix[2:length(newUsefulDataMatrix[,6]),6],as.numeric)),unlist(lapply(newUsefulDataMatrix[2:length(newUsefulDataMatrix[,7]),7],as.numeric))), beside=T, col = c("blue","red"), names.arg = newUsefulDataMatrix[2:length(newUsefulDataMatrix[,1]),1], cex.names = 0.4, cex.axis = 0.5, las = 2, ylab = "EAC []", mgp = c(3,0.6,-0.1))
  
  #legend
  legend(x = c((par("usr")[2] - 275),par("usr")[2]), y = c(par("usr")[4],(par("usr")[4] - 4000)), c("String Counter","Central Counter"), col = c("blue","red"), pch = c(15,15), y.intersp = 0.5, bty = "n")
  
  #plot titles
  title(main = paste("[IN-036S] EAC for 2 Counters"), line = 2.2,cex.main = 1.5)
  title(main = paste0("From ", newUsefulDataMatrix[2,1], " to ", newUsefulDataMatrix[length(newUsefulDataMatrix[,1]),1]), line = 0.75, cex.main = 1.2)
  
  #add names
  title(sub=paste0("Cleantech Solar","\n","O&M Team"), adj=0, line=3, font=2, cex.sub = 0.7)
  title(sub=paste0("Dhruv Baid","\n",toString(Sys.Date())), adj=1, line=3, font=2, cex.sub = 0.7)
}
#plotEAC()

plotYields = function() {
  #plot scatter diagram for both yields
  plot(lapply(newUsefulDataMatrix[2:(length(newUsefulDataMatrix[,1])),1],as.Date),newUsefulDataMatrix[2:(length(newUsefulDataMatrix[,1])),8], type = "p", xaxt = "n", xlab = "", ylab = "Yield", ylim = c(0,7), col = "blue", pch = 15, las = 1, cex.lab = 1.3)
  points(lapply(newUsefulDataMatrix[2:(length(newUsefulDataMatrix[,1])),1],as.Date),newUsefulDataMatrix[2:(length(newUsefulDataMatrix[,1])),9], xaxt = "n", xlab = "", ylab = "", col = "red", pch = 16)
  
  #add text to state that only a certain range of PRs was plotted
  text(x = par("usr")[2] - 5, y = par("usr")[4] - 0.17, "Only Yields between 0-7 are plotted", adj = 1, cex = 0.8)
  
  #re-lable time-axis
  axis.Date(1, at = seq(from = as.Date(unlist(newList[1])), to = as.Date(unlist(newList[length(newList) - 14])), by = "month"), format = "%b/%y")
  
  #calculating averages
  avgSTR = (Reduce("+",unlist(lapply(newUsefulDataMatrix[2:(length(newUsefulDataMatrix[,1])),8],as.numeric))))/(length(newUsefulDataMatrix[,1]) - 1)
  avgCTR = (Reduce("+",unlist(lapply(newUsefulDataMatrix[2:(length(newUsefulDataMatrix[,1])),9],as.numeric))))/(length(newUsefulDataMatrix[,1]) - 1)
  
  #plotting lines for averages
  abline(h = avgSTR, col = "blue", lty = 2)
  abline(h = avgCTR, col = "red", lty = 2)
  
  #legend
  legend(x = c(par("usr")[2] - 200,par("usr")[2]), y = c(par("usr")[3] + 1,par("usr")[3]), c(paste0("String Counter [Average = ", round(avgSTR, digits = 2), "]"), paste0("Central Counter [Average = ", round(avgCTR, digits = 2), "]")), col = c("blue","red"), pch = c(15,16), y.intersp = 0.5)
  
  #add titles
  title(main = paste("[IN-036S] Yields for 2 Counters"), line = 2.2,cex.main = 1.5)
  title(main = paste0("From ", newUsefulDataMatrix[2,1], " to ", newUsefulDataMatrix[length(newUsefulDataMatrix[,1]),1]), line = 0.75, cex.main = 1.2)
  
  #add names
  title(sub=paste0("Cleantech Solar","\n","O&M Team"), adj=0, line=3, font=2, cex.sub = 0.7)
  title(sub=paste0("Dhruv Baid","\n",toString(Sys.Date())), adj=1, line=3, font=2, cex.sub = 0.7)
}
plotYields()

plotPR = function() {
  plot(lapply(newUsefulDataMatrix[2:(length(newUsefulDataMatrix[,1])),1],as.Date), newUsefulDataMatrix[2:(length(newUsefulDataMatrix[,1])),11], type = "p", xaxt = "n", xlab = "", ylab = "Performance Ratio [%]", ylim = c(0,100), col = "red", pch = 15, las = 1, cex.lab = 1.3)
  points(lapply(newUsefulDataMatrix[2:(length(newUsefulDataMatrix[,1])),1],as.Date), newUsefulDataMatrix[2:(length(newUsefulDataMatrix[,1])),12], xaxt = "n", xlab = "", ylab = "", col = "yellow", pch = 16, las = 1)
  points(lapply(newUsefulDataMatrix[2:(length(newUsefulDataMatrix[,1])),1],as.Date), newUsefulDataMatrix[2:(length(newUsefulDataMatrix[,1])),13], xaxt = "n", xlab = "", ylab = "", col = "blue", pch = 17, las = 1)
  points(lapply(newUsefulDataMatrix[2:(length(newUsefulDataMatrix[,1])),1],as.Date), newUsefulDataMatrix[2:(length(newUsefulDataMatrix[,1])),14], xaxt = "n", xlab = "", ylab = "", col = "black", pch = 18, las = 1)
  
  #add text to state that only a certain range of PRs was plotted
  text(x = (0.008*par("usr")[1] + 0.992*par("usr")[2]), y = 100, "Only PRs between 0-100% are plotted", adj = 1, cex = 0.8)
  
  #add business plan line
  abline(h = 76.9, lty = 2, lwd = 2, col = rgb(0, 128, 0, maxColorValue = 255))
  
  #add text to explain that line
  text(x = (0.99*par("usr")[1] + 0.01*par("usr")[2]), y = 100, "Performance Ratio = 76.9% (business plan)", col = rgb(0, 128, 0, maxColorValue = 255), adj = 0, cex = 0.8)
  
  #re-lable time-axis
  axis.Date(1, at = seq(from = as.Date(unlist(newList[1])), to = as.Date(unlist(newList[length(newList) - 14])), by = "month"), format = "%b/%y")
  
  #legend
  legend(x = c(par("usr")[2] - 140,par("usr")[2]), y = c(par("usr")[3] + 23,par("usr")[3]), c("String PR (GMod)","Central PR (GMod)","String PR (SMP10)","Central PR (SMP10)"), col = c("red","yellow","blue","black"), pch = c(15,16,17,18), y.intersp = 0.5)
  
  #add titles
  title(main = paste("[IN-036S] Performance Ratios"), line = 2.2,cex.main = 1.5)
  title(main = paste0("From ", newUsefulDataMatrix[2,1], " to ", newUsefulDataMatrix[length(newUsefulDataMatrix[,1]),1]), line = 0.75, cex.main = 1.2)
  
  #add names
  title(sub=paste0("Cleantech Solar","\n","O&M Team"), adj=0, line=3, font=2, cex.sub = 0.7)
  title(sub=paste0("Dhruv Baid","\n",toString(Sys.Date())), adj=1, line=3, font=2, cex.sub = 0.7)
}
plotPR()

plotPRTrimmed = function() {
  plot(lapply(newUsefulDataMatrix[2:(length(newUsefulDataMatrix[,1])),1],as.Date), newUsefulDataMatrix[2:(length(newUsefulDataMatrix[,1])),11], type = "p", xaxt = "n", xlab = "", ylab = "Performance Ratio [%]", ylim = c(0,100), col = "red", pch = 15, las = 1, cex.lab = 1.3)
  points(lapply(newUsefulDataMatrix[2:(length(newUsefulDataMatrix[,1])),1],as.Date), newUsefulDataMatrix[2:(length(newUsefulDataMatrix[,1])),12], xaxt = "n", xlab = "", ylab = "", col = "yellow", pch = 16, las = 1)

  #add text to state that only a certain range of PRs was plotted
  text(x = par("usr")[2] - 5, y = par("usr")[4] - 3, "Only PRs between 0-100% are plotted", adj = 1, cex = 0.8)
  
  #re-lable time-axis
  axis.Date(1, at = seq(from = as.Date(unlist(newList[1])), to = as.Date(unlist(newList[length(newList) - 14])), by = "month"), format = "%b/%y")
  
  #legend
  legend(x = c(par("usr")[2] - 140,par("usr")[2]), y = c(par("usr")[3] + 15,par("usr")[3]), c("String PR (GMod)","Central PR (GMod)"), col = c("red","yellow"), pch = c(15,16), y.intersp = 0.5)
  
  #add titles
  title(main = paste("[IN-036S] Performance Ratios"), line = 2.2,cex.main = 1.5)
  title(main = paste0("From ", newUsefulDataMatrix[2,1], " to ", newUsefulDataMatrix[length(newUsefulDataMatrix[,1]),1]), line = 0.75, cex.main = 1.2)
  
  #add names
  title(sub=paste0("Cleantech Solar","\n","O&M Team"), adj=0, line=3, font=2, cex.sub = 0.7)
  title(sub=paste0("Dhruv Baid","\n",toString(Sys.Date())), adj=1, line=3, font=2, cex.sub = 0.7)
}
plotPRTrimmed()

plotRatioYieldTime = function() {
  plot(lapply(newUsefulDataMatrix[2:(length(newUsefulDataMatrix[,1])),1],as.Date), newUsefulDataMatrix[2:(length(newUsefulDataMatrix[,1])),15], type = "p", xaxt = "n", xlab = "", ylab = "Ratio", pch = 4, cex = 0.5, las = 1, cex.lab = 1.3)
  
  #re-lable time-axis
  axis.Date(1, at = seq(from = as.Date(unlist(newList[1])), to = as.Date(unlist(newList[length(newList) - 14])), by = "month"), format = "%b/%y")
  
  #add titles
  title(main = paste("[IN-036S] Ratio (Yield)"), line = 2.2,cex.main = 1.5)
  title(main = paste0("From ", newUsefulDataMatrix[2,1], " to ", newUsefulDataMatrix[length(newUsefulDataMatrix[,1]),1]), line = 0.75, cex.main = 1.2)
  
  #add names
  title(sub=paste0("Cleantech Solar","\n","O&M Team"), adj=0, line=3, font=2, cex.sub = 0.7)
  title(sub=paste0("Dhruv Baid","\n",toString(Sys.Date())), adj=1, line=3, font=2, cex.sub = 0.7)
  
}
plotRatioYieldTime()

#defining constants which will be used in subsequent plots
ratioYieldMean1 = 0
ratioYieldSD1 = 0
ratioYieldMean2 = 0
ratioYieldSD2 = 0
ratioYieldMean3 = 0

plotRatioYieldGMod = function() {
  #perform some statistical calculations
  ratioYieldMean1 <<- mean(unlist(lapply(newUsefulDataMatrix[2:(length(newUsefulDataMatrix[,1])),15], as.numeric)))
  ratioYieldSD1 <<- sd(unlist(lapply(newUsefulDataMatrix[2:(length(newUsefulDataMatrix[,1])),15], as.numeric)))
  
  #plot graph
  plot(newUsefulDataMatrix[2:(length(newUsefulDataMatrix[,1])),10], newUsefulDataMatrix[2:(length(newUsefulDataMatrix[,1])),15], type = "p", xlab = expression('Daily Global Horizontal Irradiation [kWh/m'^2*']'), ylab = "Ratio", ylim = c(0.85,1.4), pch = 4, cex = 0.7, las = 1, cex.lab = 1.3)
  
  #Add text to show these statistics
  text(x = par("usr")[2] - 0.05, y = par("usr")[4] - 0.015, paste0("Average = ", round(ratioYieldMean1, digits = 2), "\n StDev = ", round(ratioYieldSD1, digits = 2)), cex = 0.8, adj = 1)
  
  #add lines for average and 2-sigma range
  abline(h = ratioYieldMean1, lwd = 2)
  abline(h = ratioYieldMean1 + 2*ratioYieldSD1, lty = 2)
  abline(h = ratioYieldMean1 - 2*ratioYieldSD1, lty = 2)
  
  #add labels for each of these lines
  text(x = par("usr")[1] + 0.06, y = (ratioYieldMean1 + 2*ratioYieldSD1 + 0.01), expression(paste("+2",sigma)), cex = 0.8, adj = 0)
  text(x = par("usr")[1] + 0.06, y = (ratioYieldMean1 - 2*ratioYieldSD1 - 0.01), expression(paste("-2",sigma)), cex = 0.8, adj = 0)
  
  #add titles
  title(main = paste("[IN-036S] Ratio (Yield) Against GMod"), line = 2.2,cex.main = 1.5)
  title(main = paste0("From ", newUsefulDataMatrix[2,1], " to ", newUsefulDataMatrix[length(newUsefulDataMatrix[,1]),1]), line = 0.75, cex.main = 1.2)
  
  #add names
  title(sub=paste0("Cleantech Solar","\n","O&M Team"), adj=0, line=3, font=2, cex.sub = 0.7)
  title(sub=paste0("Dhruv Baid","\n",toString(Sys.Date())), adj=1, line=3, font=2, cex.sub = 0.7)
  
}
plotRatioYieldGMod()

purpleList = list()
newerList = list()
newerUsefulDataMatrix = matrix()
plotTwoSigmaFilteredRatioYieldGMod = function() {
  #calculate some statistics
  ratioYieldMean1 <<- mean(unlist(lapply(newUsefulDataMatrix[2:(length(newUsefulDataMatrix[,1])),15], as.numeric)))
  ratioYieldSD1 <<- sd(unlist(lapply(newUsefulDataMatrix[2:(length(newUsefulDataMatrix[,1])),15], as.numeric)))
  
  #filter out points outside the 2 Sigma range
  for (i in 2:length(newUsefulDataMatrix[,1])) {
    if (as.numeric(newUsefulDataMatrix[i,15]) < (ratioYieldMean1 + 2*ratioYieldSD1) && as.numeric(newUsefulDataMatrix[i,15]) > (ratioYieldMean1 - 2*ratioYieldSD1)) {
      newerList <<- append(newerList,unlist(newUsefulDataMatrix[i,]))
    }
    else {
      purpleList <<- append(purpleList,unlist(newUsefulDataMatrix[i,]))
    }
  }
  
  #combining all filtered data into a matrix
  numPoints = length(newerList)/15
  newerUsefulDataMatrix <<- matrix(nrow = (numPoints + 1), ncol = 15)
  newerUsefulDataMatrix[1, ] <<- c("Date","Data Availability","Pyranometer","Silicon","Ratio (SMP/Gsi)","EAC String","EAC Central","Yield (String)","Yield (Central)","GMod","String PR (GMod)","Central PR (GMod)","String PR (SMP10)","Central PR (SMP10)","2 Sigma Filtered Ratio (Yield)")
  
  for(z in 1:numPoints) {
    newerUsefulDataMatrix[(z+1),] <<- c(unlist(newerList[15*(z-1) + 1]), unlist(newerList[15*(z-1) + 2]), unlist(newerList[15*(z-1) + 3]), unlist(newerList[15*(z-1) + 4]), unlist(newerList[15*(z-1) + 5]), unlist(newerList[15*(z-1) + 6]), unlist(newerList[15*(z-1) + 7]), unlist(newerList[15*(z-1) + 8]), unlist(newerList[15*(z-1) + 9]), unlist(newerList[15*(z-1) + 10]), unlist(newerList[15*(z-1) + 11]), unlist(newerList[15*(z-1) + 12]), unlist(newerList[15*(z-1) + 13]), unlist(newerList[15*(z-1) + 14]), unlist(newerList[15*z]))
  }
  
  #save .txt file to the right directory
  setwd("/home/admin/Jason/cec intern/results/[IN-036S]")
  write.matrix(newerUsefulDataMatrix, "allDataFiltered (2 Sigma).txt", sep = "\t")
  
  #plot the now-filtered points
  plot(newerUsefulDataMatrix[2:(length(newerUsefulDataMatrix[,1])),10], newerUsefulDataMatrix[2:(length(newerUsefulDataMatrix[,1])),15], type = "p", xlab = expression('Daily Global Horizontal Irradiation [kWh/m'^2*']'), ylab = "Ratio", pch = 4, cex = 0.7, las = 1, cex.lab = 1.3)

  #Add text to show the statistics of what was filtered out
  text(x = par("usr")[2] - 0.05, y = par("usr")[4] - 0.0075, paste0("Points eliminated (First 2 Sigma Filter) = ", (length(newUsefulDataMatrix[,1]) - length(newerUsefulDataMatrix[,1])), "\n % of points eliminated = ", round((100 * ((length(newUsefulDataMatrix[,1]) - length(newerUsefulDataMatrix[,1]))/(length(newUsefulDataMatrix[,1])))),digits = 1),"%"), cex = 0.8, adj = 1)
  
  #calculate new statistics
  ratioYieldMean2 = mean(unlist(lapply(newerUsefulDataMatrix[2:(length(newerUsefulDataMatrix[,1])),15], as.numeric)))
  ratioYieldSD2 = sd(unlist(lapply(newerUsefulDataMatrix[2:(length(newerUsefulDataMatrix[,1])),15], as.numeric)))
  
  #add lines for new average and 2-sigma range
  abline(h = ratioYieldMean2, lwd = 2)
  abline(h = ratioYieldMean2 + 2*ratioYieldSD2, lty = 2)
  abline(h = ratioYieldMean2 - 2*ratioYieldSD2, lty = 2)
  
  #add labels for each of these lines
  text(x = par("usr")[1] + 0.06, y = (ratioYieldMean2 + 2*ratioYieldSD2 + 0.005), expression(paste("+2",sigma)), cex = 0.8, adj = 0)
  text(x = par("usr")[1] + 0.06, y = (ratioYieldMean2 - 2*ratioYieldSD2 - 0.005), expression(paste("-2",sigma)), cex = 0.8, adj = 0)
  
  #add titles
  title(main = expression(bold(paste("[IN-036S] 2-", sigma, " Filtered Ratio (Yield) Against GMod"))), line = 2.2,cex.main = 1.5)
  title(main = paste0("From ", newerUsefulDataMatrix[2,1], " to ", newerUsefulDataMatrix[length(newerUsefulDataMatrix[,1]),1]), line = 0.75, cex.main = 1.2)
  
  #add names
  title(sub=paste0("Cleantech Solar","\n","O&M Team"), adj=0, line=3, font=2, cex.sub = 0.7)
  title(sub=paste0("Dhruv Baid","\n",toString(Sys.Date())), adj=1, line=3, font=2, cex.sub = 0.7)
}
plotTwoSigmaFilteredRatioYieldGMod()

redList = list()
newestList = list()
newestUsefulDataMatrix = matrix()
plotDoubleTwoSigmaFilteredRatioYieledGMod = function() {
  #calculate some statistics
  ratioYieldMean2 <<- mean(unlist(lapply(newerUsefulDataMatrix[2:(length(newerUsefulDataMatrix[,1])),15], as.numeric)))
  ratioYieldSD2 <<- sd(unlist(lapply(newerUsefulDataMatrix[2:(length(newerUsefulDataMatrix[,1])),15], as.numeric)))
  
  #filter out points outside the 2 Sigma range
  for (i in 2:length(newerUsefulDataMatrix[,1])) {
    if (as.numeric(newerUsefulDataMatrix[i,15]) < (ratioYieldMean2 + 2*ratioYieldSD2) && as.numeric(newerUsefulDataMatrix[i,15]) > (ratioYieldMean2 - 2*ratioYieldSD2)) {
      newestList <<- append(newestList,unlist(newerUsefulDataMatrix[i,]))
    }
    else {
      redList <<- append(redList,unlist(newerUsefulDataMatrix[i,]))
    }
  }
  
  #combining all filtered data into a matrix
  numPoints = length(newestList)/15
  newestUsefulDataMatrix <<- matrix(nrow = (numPoints + 1), ncol = 15)
  newestUsefulDataMatrix[1, ] <<- c("Date","Data Availability","Pyranometer","Silicon","Ratio (SMP/Gsi)","EAC String","EAC Central","Yield (String)","Yield (Central)","GMod","String PR (GMod)","Central PR (GMod)","String PR (SMP10)","Central PR (SMP10)","2 Sigma Filtered Ratio (Yield)")
  
  for(z in 1:numPoints) {
    newestUsefulDataMatrix[(z+1),] <<- c(unlist(newestList[15*(z-1) + 1]), unlist(newestList[15*(z-1) + 2]), unlist(newestList[15*(z-1) + 3]), unlist(newestList[15*(z-1) + 4]), unlist(newestList[15*(z-1) + 5]), unlist(newestList[15*(z-1) + 6]), unlist(newestList[15*(z-1) + 7]), unlist(newestList[15*(z-1) + 8]), unlist(newestList[15*(z-1) + 9]), unlist(newestList[15*(z-1) + 10]), unlist(newestList[15*(z-1) + 11]), unlist(newestList[15*(z-1) + 12]), unlist(newestList[15*(z-1) + 13]), unlist(newestList[15*(z-1) + 14]), unlist(newestList[15*z]))
  }
  
  #save .txt file to the right directory
  setwd("/home/admin/Jason/cec intern/results/[IN-036S]")
  write.matrix(newestUsefulDataMatrix, "allDataFiltered (2 Sigma - twice).txt", sep = "\t")
  
  #plot the now-filtered points
  plot(newestUsefulDataMatrix[2:(length(newestUsefulDataMatrix[,1])),10], newestUsefulDataMatrix[2:(length(newestUsefulDataMatrix[,1])),15], type = "p", xlab = expression('Daily Global Horizontal Irradiation [kWh/m'^2*']'), ylab = "Ratio", pch = 4, cex = 0.7, las = 1, cex.lab = 1.3)
  
  #Add text to show the statistics of what was filtered out
  text(x = par("usr")[2] - 0.05, y = par("usr")[4] - 0.0025, paste0("Points eliminated (Second 2 Sigma Filter) = ", (length(newerUsefulDataMatrix[,1]) - length(newestUsefulDataMatrix[,1])), "\n % of points eliminated = ", round((100 * ((length(newerUsefulDataMatrix[,1]) - length(newestUsefulDataMatrix[,1]))/(length(newerUsefulDataMatrix[,1])))),digits = 1),"%"), cex = 0.8, adj = 1)
  
  #add titles
  title(main = expression(bold(paste("[IN-036S] Double 2-", sigma, " Filtered Ratio (Yield) Against GMod"))), line = 2.2,cex.main = 1.5)
  title(main = paste0("From ", newestUsefulDataMatrix[2,1], " to ", newestUsefulDataMatrix[length(newestUsefulDataMatrix[,1]),1]), line = 0.75, cex.main = 1.2)
  
  #add names
  title(sub=paste0("Cleantech Solar","\n","O&M Team"), adj=0, line=3, font=2, cex.sub = 0.7)
  title(sub=paste0("Dhruv Baid","\n",toString(Sys.Date())), adj=1, line=3, font=2, cex.sub = 0.7)
  
}
plotDoubleTwoSigmaFilteredRatioYieledGMod()

plotOverall = function() {
  numRedPoints = length(redList)/15
  redDataMatrix = matrix(nrow = (numRedPoints + 1), ncol = 15)
  redDataMatrix[1, ] = c("Date","Data Availability","Pyranometer","Silicon","Ratio (SMP/Gsi)","EAC String","EAC Central","Yield (String)","Yield (Central)","GMod","String PR (GMod)","Central PR (GMod)","String PR (SMP10)","Central PR (SMP10)","2 Sigma Filtered Ratio (Yield)")
  for(z in 1:numRedPoints) {
    redDataMatrix[(z+1),] = c(unlist(redList[15*(z-1) + 1]), unlist(redList[15*(z-1) + 2]), unlist(redList[15*(z-1) + 3]), unlist(redList[15*(z-1) + 4]), unlist(redList[15*(z-1) + 5]), unlist(redList[15*(z-1) + 6]), unlist(redList[15*(z-1) + 7]), unlist(redList[15*(z-1) + 8]), unlist(redList[15*(z-1) + 9]), unlist(redList[15*(z-1) + 10]), unlist(redList[15*(z-1) + 11]), unlist(redList[15*(z-1) + 12]), unlist(redList[15*(z-1) + 13]), unlist(redList[15*(z-1) + 14]), unlist(redList[15*z]))
  }
  
  numPurplePoints = length(purpleList)/15
  purpleDataMatrix = matrix(nrow = (numPurplePoints + 1), ncol = 15)
  purpleDataMatrix[1, ] = c("Date","Data Availability","Pyranometer","Silicon","Ratio (SMP/Gsi)","EAC String","EAC Central","Yield (String)","Yield (Central)","GMod","String PR (GMod)","Central PR (GMod)","String PR (SMP10)","Central PR (SMP10)","2 Sigma Filtered Ratio (Yield)")
  for(z in 1:numPurplePoints) {
    purpleDataMatrix[(z+1),] = c(unlist(purpleList[15*(z-1) + 1]), unlist(purpleList[15*(z-1) + 2]), unlist(purpleList[15*(z-1) + 3]), unlist(purpleList[15*(z-1) + 4]), unlist(purpleList[15*(z-1) + 5]), unlist(purpleList[15*(z-1) + 6]), unlist(purpleList[15*(z-1) + 7]), unlist(purpleList[15*(z-1) + 8]), unlist(purpleList[15*(z-1) + 9]), unlist(purpleList[15*(z-1) + 10]), unlist(purpleList[15*(z-1) + 11]), unlist(purpleList[15*(z-1) + 12]), unlist(purpleList[15*(z-1) + 13]), unlist(purpleList[15*(z-1) + 14]), unlist(purpleList[15*z]))
  }
  
  #calculate statistics for newest filtered data
  ratioYieldMean3 <<- mean(unlist(lapply(newestUsefulDataMatrix[2:(length(newestUsefulDataMatrix[,1])),15], as.numeric)))
  
  #plot points in 3 different zones
  plot(newestUsefulDataMatrix[2:(length(newestUsefulDataMatrix[,1])),10], newestUsefulDataMatrix[2:(length(newestUsefulDataMatrix[,1])),15], type = "p", xlab = expression('Daily Global Horizontal Irradiation [kWh/m'^2*']'), ylab = "Ratio", ylim = c(0.8,1.5), pch = 1, cex = 0.7, las = 1, cex.lab = 1.3)
  points(redDataMatrix[2:(length(redDataMatrix[,1])),10], redDataMatrix[2:(length(redDataMatrix[,1])),15], type = "p", xlab = "", ylab = "", pch = 4, cex = 0.7, las = 1, col = "red")
  points(purpleDataMatrix[2:(length(purpleDataMatrix[,1])),10], purpleDataMatrix[2:(length(purpleDataMatrix[,1])),15], type = "p", xlab = "", ylab = "", pch = 6, cex = 0.7, las = 1, col = "purple")
  
  #add lines to show the ranges of data
  #average and 2-sigma range for data before any filtering
  abline(h = ratioYieldMean1, lwd = 1, col = "purple")
  abline(h = ratioYieldMean1 + 2*ratioYieldSD1, lty = 2, col = "purple")
  abline(h = ratioYieldMean1 - 2*ratioYieldSD1, lty = 2, col = "purple")
  #average and 2-sigma range for data after 1 2-sigma filter
  abline(h = ratioYieldMean2, lwd = 1, col = "red")
  abline(h = ratioYieldMean2 + 2*ratioYieldSD2, lty = 2, col = "red")
  abline(h = ratioYieldMean2 - 2*ratioYieldSD2, lty = 2, col = "red")
  #average for data after 2 2-sigma filters
  abline(h = ratioYieldMean3, lwd = 1, col = "green")
  
  #add labels for each of these lines
  #2-sigma range for data before any filtering
  text(x = par("usr")[1] + 0.06, y = (ratioYieldMean1 + 2*ratioYieldSD1 + 0.01), expression(paste("+2",sigma," (original data)")), cex = 0.8, adj = 0, col = "purple")
  text(x = par("usr")[1] + 0.06, y = (ratioYieldMean1 - 2*ratioYieldSD1 - 0.01), expression(paste("-2",sigma," (original data)")), cex = 0.8, adj = 0, col = "purple")
  #average and 2-sigma range for data after 1 2-sigma filter
  text(x = par("usr")[1] + 0.06, y = (ratioYieldMean2 + 2*ratioYieldSD2 + 0.01), expression(paste("+2",sigma," (data with the first 2-",sigma," filter applied)")), cex = 0.8, adj = 0, col = "red")
  text(x = par("usr")[1] + 0.06, y = (ratioYieldMean2 - 2*ratioYieldSD2 - 0.01), expression(paste("-2",sigma," (data with the first 2-",sigma," filter applied)")), cex = 0.8, adj = 0, col = "red")
  
  #add line of best fit for final unfiltered data (newestUsefulDataMatrix)
  unfilteredData = data.frame(y = unlist(lapply(newestUsefulDataMatrix[2:(length(newestUsefulDataMatrix[,1])),15], as.numeric)), x = unlist(lapply(newestUsefulDataMatrix[2:(length(newestUsefulDataMatrix[,1])),10], as.numeric)))
  abline(lm(unfilteredData), col = "blue", lwd = 2)
  
  #summary statistics
  legend('topleft', ncol = 4L, title = expression(underline('Summary Information')), legend = c('', 'Mean', 'SD', 'Num. of Points Filtered', '% of Points Filtered', 'Number of Points Remaining', 'Original Data', round(ratioYieldMean1, digits = 3), round(ratioYieldSD1, digits = 3), '-', '-', dim(newUsefulDataMatrix)[1] - 1, 'First 2-Sigma Filter', round(ratioYieldMean2, digits = 3), round(ratioYieldSD2, digits = 3), (length(newUsefulDataMatrix[,1]) - length(newerUsefulDataMatrix[,1])), paste(round((100 * ((length(newUsefulDataMatrix[,1]) - length(newerUsefulDataMatrix[,1]))/(length(newUsefulDataMatrix[,1])))),digits = 1),"%"), dim(newerUsefulDataMatrix)[1] - 1, 'Second 2-Sigma Filter', round(ratioYieldMean3,digits = 3), "-", (length(newerUsefulDataMatrix[,1]) - length(newestUsefulDataMatrix[,1])), paste(round((100 * ((length(newerUsefulDataMatrix[,1]) - length(newestUsefulDataMatrix[,1]))/(length(newerUsefulDataMatrix[,1])))),digits = 1),"%"), dim(newestUsefulDataMatrix)[1] - 1), cex = 0.3)
  
  #legend
  legend(x = c(par("usr")[2] - 1.25,par("usr")[2]), y = c(par("usr")[4],par("usr")[4] - 0.08), c("Points outside first 2-sigma filter","Points outside second 2-sigma filter","Unfiltered Points","Line of Best Fit through Unfiltered Points"), col = c("purple","red","black", "blue"), pch = c(6,4,1,NA), lty = c(NA,NA,NA,1), lwd = c(NA,NA,NA,2), y.intersp = 0.5, cex = 0.5)
  
  #add titles
  title(main = expression(bold(paste("[IN-036S] Ratio (Yield) Against GMod"))), line = 2.2,cex.main = 1.5)
  title(main = paste0("From ", newUsefulDataMatrix[2,1], " to ", newUsefulDataMatrix[length(newUsefulDataMatrix[,1]),1]), line = 0.75, cex.main = 1.2)
  
  #add names
  title(sub=paste0("Cleantech Solar","\n","O&M Team"), adj=0, line=3, font=2, cex.sub = 0.7)
  title(sub=paste0("Dhruv Baid","\n",toString(Sys.Date())), adj=1, line=3, font=2, cex.sub = 0.7)
  
}
plotOverall()

plotOverallZoomed = function() {
  numRedPoints = length(redList)/15
  redDataMatrix = matrix(nrow = (numRedPoints + 1), ncol = 15)
  redDataMatrix[1, ] = c("Date","Data Availability","Pyranometer","Silicon","Ratio (SMP/Gsi)","EAC String","EAC Central","Yield (String)","Yield (Central)","GMod","String PR (GMod)","Central PR (GMod)","String PR (SMP10)","Central PR (SMP10)","2 Sigma Filtered Ratio (Yield)")
  for(z in 1:numRedPoints) {
    redDataMatrix[(z+1),] = c(unlist(redList[15*(z-1) + 1]), unlist(redList[15*(z-1) + 2]), unlist(redList[15*(z-1) + 3]), unlist(redList[15*(z-1) + 4]), unlist(redList[15*(z-1) + 5]), unlist(redList[15*(z-1) + 6]), unlist(redList[15*(z-1) + 7]), unlist(redList[15*(z-1) + 8]), unlist(redList[15*(z-1) + 9]), unlist(redList[15*(z-1) + 10]), unlist(redList[15*(z-1) + 11]), unlist(redList[15*(z-1) + 12]), unlist(redList[15*(z-1) + 13]), unlist(redList[15*(z-1) + 14]), unlist(redList[15*z]))
  }
  
  numPurplePoints = length(purpleList)/15
  purpleDataMatrix = matrix(nrow = (numPurplePoints + 1), ncol = 15)
  purpleDataMatrix[1, ] = c("Date","Data Availability","Pyranometer","Silicon","Ratio (SMP/Gsi)","EAC String","EAC Central","Yield (String)","Yield (Central)","GMod","String PR (GMod)","Central PR (GMod)","String PR (SMP10)","Central PR (SMP10)","2 Sigma Filtered Ratio (Yield)")
  for(z in 1:numPurplePoints) {
    purpleDataMatrix[(z+1),] = c(unlist(purpleList[15*(z-1) + 1]), unlist(purpleList[15*(z-1) + 2]), unlist(purpleList[15*(z-1) + 3]), unlist(purpleList[15*(z-1) + 4]), unlist(purpleList[15*(z-1) + 5]), unlist(purpleList[15*(z-1) + 6]), unlist(purpleList[15*(z-1) + 7]), unlist(purpleList[15*(z-1) + 8]), unlist(purpleList[15*(z-1) + 9]), unlist(purpleList[15*(z-1) + 10]), unlist(purpleList[15*(z-1) + 11]), unlist(purpleList[15*(z-1) + 12]), unlist(purpleList[15*(z-1) + 13]), unlist(purpleList[15*(z-1) + 14]), unlist(purpleList[15*z]))
  }
  
  #calculate statistics for newest filtered data
  ratioYieldMean3 <<- mean(unlist(lapply(newestUsefulDataMatrix[2:(length(newestUsefulDataMatrix[,1])),15], as.numeric)))
  
  #plot points in 3 different zones
  plot(newestUsefulDataMatrix[2:(length(newestUsefulDataMatrix[,1])),10], newestUsefulDataMatrix[2:(length(newestUsefulDataMatrix[,1])),15], type = "p", xlab = expression('Daily Global Horizontal Irradiation [kWh/m'^2*']'), ylab = "Ratio", ylim = c(0.9,1.1), pch = 1, cex = 0.7, las = 1, cex.lab = 1.3)
  points(redDataMatrix[2:(length(redDataMatrix[,1])),10], redDataMatrix[2:(length(redDataMatrix[,1])),15], type = "p", xlab = "", ylab = "", pch = 4, cex = 0.7, las = 1, col = "red")
  points(purpleDataMatrix[2:(length(purpleDataMatrix[,1])),10], purpleDataMatrix[2:(length(purpleDataMatrix[,1])),15], type = "p", xlab = "", ylab = "", pch = 6, cex = 0.7, las = 1, col = "purple")
  
  #add lines to show the ranges of data
  #average and 2-sigma range for data before any filtering
  abline(h = ratioYieldMean1, lwd = 1, col = "purple")
  abline(h = ratioYieldMean1 + 2*ratioYieldSD1, lty = 2, col = "purple")
  abline(h = ratioYieldMean1 - 2*ratioYieldSD1, lty = 2, col = "purple")
  #average and 2-sigma range for data after 1 2-sigma filter
  abline(h = ratioYieldMean2, lwd = 1, col = "red")
  abline(h = ratioYieldMean2 + 2*ratioYieldSD2, lty = 2, col = "red")
  abline(h = ratioYieldMean2 - 2*ratioYieldSD2, lty = 2, col = "red")
  #average for data after 2 2-sigma filters
  abline(h = ratioYieldMean3, lwd = 1, col = "green")
  
  #add labels for each of these lines
  #2-sigma range for data before any filtering
  text(x = par("usr")[1] + 0.06, y = (ratioYieldMean1 + 2*ratioYieldSD1 + 0.005), expression(paste("+2",sigma," (original data)")), cex = 0.8, adj = 0, col = "purple")
  text(x = par("usr")[1] + 0.06, y = (ratioYieldMean1 - 2*ratioYieldSD1 - 0.005), expression(paste("-2",sigma," (original data)")), cex = 0.8, adj = 0, col = "purple")
  #average and 2-sigma range for data after 1 2-sigma filter
  text(x = par("usr")[1] + 0.06, y = (ratioYieldMean2 + 2*ratioYieldSD2 + 0.005), expression(paste("+2",sigma," (data with the first 2-",sigma," filter applied)")), cex = 0.8, adj = 0, col = "red")
  text(x = par("usr")[1] + 0.06, y = (ratioYieldMean2 - 2*ratioYieldSD2 - 0.005), expression(paste("-2",sigma," (data with the first 2-",sigma," filter applied)")), cex = 0.8, adj = 0, col = "red")
  
  #add line of best fit for final unfiltered data (newestUsefulDataMatrix)
  unfilteredData = data.frame(y = unlist(lapply(newestUsefulDataMatrix[2:(length(newestUsefulDataMatrix[,1])),15], as.numeric)), x = unlist(lapply(newestUsefulDataMatrix[2:(length(newestUsefulDataMatrix[,1])),10], as.numeric)))
  abline(lm(unfilteredData), col = "blue", lwd = 2)
  
  #legend
  legend(x = c((0.2*par("usr")[1] + 0.8*par("usr")[2]),par("usr")[2]), y = c(par("usr")[4], (0.1*par("usr")[3] + 0.9*par("usr")[4])), c("Points outside second 2-sigma filter","Unfiltered Points","Line of Best Fit through Unfiltered Points"), col = c("red","black", "blue"), pch = c(4,1,NA), lty = c(NA,NA,1), lwd = c(NA,NA,2), y.intersp = 0.5, cex = 0.5)
  
  #add titles
  title(main = expression(bold(paste("[IN-036S] Ratio (Yield) Against GMod"))), line = 2.2,cex.main = 1.5)
  title(main = paste0("From ", newUsefulDataMatrix[2,1], " to ", newUsefulDataMatrix[length(newUsefulDataMatrix[,1]),1]), line = 0.75, cex.main = 1.2)
  
  #add names
  title(sub=paste0("Cleantech Solar","\n","O&M Team"), adj=0, line=3, font=2, cex.sub = 0.7)
  title(sub=paste0("Dhruv Baid","\n",toString(Sys.Date())), adj=1, line=3, font=2, cex.sub = 0.7)
  
}
plotOverallZoomed()
dev.off()
