import urllib.request, json
import csv,time
import datetime
import collections
import os
import re
import pytz


print('Running History script')
##with open('F:/Flexi_final/[IN-036C]/Gen-1/IN-036C_gen1_history.py') as source_file: ###local
with open('/home/admin/CODE/DatabaseCreation/IN036C/IN-036C_gen1_history.py') as source_file: ###server
        exec(source_file.read())
print('RETURNED Running Live script')


#Global declarations
wordDictI = {
'i9':'AC_Voltage_R','i10':'AC_Voltage_Y','i11':'AC_Voltage_B','i12':'AC_Current_R','i13':'AC_Current_Y','i14':'AC_Current_B','i15':'AC_Power',
'i16':'AC_Power_Percentage','i17':'AC_Frequency','i18':'Power_Factor','i19':'Reactive_Power','i20':'DC_Current','i21':'DC_Power',
'i22':'Inverter_Temperature','i23':'Time_Of_Use_today','i24':'Time_Of_Use_life','i26':'KWh_Counter','i27':'MWh_Counter','i28':'GWh_Counter',
'i29':'DC_Voltage','i30':'Inverter_Status','i31':'Todays_Energy','i32':'Tstamp','i33':'Total_Energy','i34':'Inverter_Communication_Status',
'i35':'AC_Power_2','i36':'AC_Power_3','i37':'AC_Frequency_2','i38':'AC_Frequency_3','i39':'DC_Voltage_2','i40':'DC_Power_2','i41':'DC_Current_2',
'i42':'Plant_Id','i43':'Inverter_Status_Word','i44':'Grid_Conn_Status','i45':'AC_Current_Total','i46':'AC_Volatge_RY','i47':'AC_Volatge_YB',
'i48':'AC_Volatge_BR','i49':'Apparent_Power','i50':'Event_Flag_1','i51':'Event_Flag_2','i52':'Event_Flag_3','i53':'Event_Flag_4','i54':'Coolent_Temp',
'i55':'DC_Voltage_3','i56':'DC_Current_3','i57':'DC_Voltage_4','i58':'DC_Current_4','i59':'Inverter_Efficiency','i60':'SPD_Status','i61':'Switch_Status',
'i62':'String_1_Current','i63':'String_2_Current','i64':'String_3_Current','i65':'String_4_Current','i66':'String_5_Current','i67':'String_6_Current',
'i68':'String_7_Current','i69':'String_8_Current','i70':'String_9_Current','i71':'String_10_Current','i72':'String_11_Current','i73':'String_12_Current',
'i74':'String_13_Current','i75':'String_14_Current','i76':'String_15_Current','i77':'String_16_Current','i78':'String_17_Current','i79':'String_18_Current',
'i80':'String_19_Current','i81':'String_20_Current','i82':'String_21_Current','i83':'String_22_Current','i84':'String_23_Current','i85':'String_24_Current',
'i87':'String_Current_Avg'
}

wordDictM = {
'm6':'MFM_Identifier','m8':'AC_Voltage_R','m9':'AC_Voltage_Y','m10':'AC_Voltage_B','m11':'AC_Voltage_Avg','m12':'AC_Voltage_RY','m13':'AC_Voltage_YB',
'm14':'AC_Voltage_BR','m15':'AC_VLL_Avg','m16':'AC_Current_R','m17':'AC_Current_Y','m18':'AC_Current_B','m19':'AC_Current_N','m20':'AC_Current_Total',
'm28':'Power_Factor_Avg','m29':'AC_Power_R_Phase','m30':'AC_Power_Y_Phase','m31':'AC_Power_B_Phase','m32':'AC_Power_Total','m34':'Reactive_Power_R_Phase',
'm35':'Reactive_Power_Y_Phase','m36':'Reactive_Power_B_Phase','m37':'Reactive_Power_Total','m38':'Reactive_Power_Avg','m41':'Apparent_Power_R_Phase',
'm42':'Apparent_Power_Y_Phase','m43':'Apparent_Power_B_Phase','m64':'Tstamp','m65':'Active_Import','m66':'Active_Export','m67':'Plant_Id'
 }

wordDictW = {
'w9':'Humidity_Min','w10':'Module_Temp1','w11':'Wind_Direction','w12':'Wind_Speed','w13':'Ambient_Temp','w15':'Humidity_Max',
'w16':'Humidity_Actual','w17':'Ambient_Temp_Min','w18':'Ambient_Temp_Max','w19':'Ambient_Temp_Avg','w20':'Global_Irradiation_Min','w21':'Irradiation_Tilt1_Actual',
'w22':'Irradiation_Tilt2_Actual','w23':'Tstamp','w24':'Global_Irradiation_Max','w25':'Global_Irradiation_Avg','w26':'Wind_Speed_Min','w27':'Wind_Speed_Max',
'w28':'Humidity_Avg','w29':'Wind_Direction_Min','w30':'Wind_Direction_Max','w31':'Wind_Speed_Avg','w32':'Global_Irradiation_Actual','w34':'Rain','w35':'Room_Temperature'
    }



last_record1 = last_record2 = last_record3 = last_record4 = last_record5 = last_record6 = last_record7 = last_record8 = last_record9 = last_record10 = 0
last_record11 = last_record12 = last_record13 = last_record14 = last_record15 = last_record16 = last_record17 = last_record18 = last_record19 = last_record20 = 0
last_record21 = last_record22 = last_record23 = last_record24 = last_record25 = last_record26 = last_record27 = last_record28 = last_record29 = last_record30 = 0
last_record31 = last_record32 = last_record33 = last_record34 = last_record35 = last_record36 = last_record37 = last_record38 = last_record39 = last_record40 = 0
last_record41 = last_record42 = last_record43 = last_record44 = last_record45 = last_record46 = last_record47 = last_record48 = last_record49 = last_record50 = 0
last_record51 = last_record52 = last_record53 = last_record54 = 0

tz = pytz.timezone('Asia/Kolkata')
today_date_1 = datetime.datetime.now(tz).date()

start_time1 = start_time2 = start_time3 = start_time4 = start_time5 = start_time6 = start_time7 = start_time8 = start_time9 = start_time10 = datetime.datetime.strptime(str(today_date_1), "%Y-%m-%d")
start_time11 = start_time12 = start_time13 = start_time14 = start_time15 = start_time16 = start_time17 = start_time18 = start_time19 = start_time20 = datetime.datetime.strptime(str(today_date_1), "%Y-%m-%d")
start_time21 = start_time22 = start_time23 = start_time24 = start_time25 = start_time26 = start_time27 = start_time28 = start_time29 = start_time30 = datetime.datetime.strptime(str(today_date_1), "%Y-%m-%d")
start_time31 = start_time32 = start_time33 = start_time34 = start_time35 = start_time36 = start_time37 = start_time38 = start_time39 = start_time40 = datetime.datetime.strptime(str(today_date_1), "%Y-%m-%d")
start_time41 = start_time42 = start_time43 = start_time44 = start_time45 = start_time46 = start_time47 = start_time48 = start_time49 = start_time50 = datetime.datetime.strptime(str(today_date_1), "%Y-%m-%d")
start_time51 = start_time52 = start_time53 = start_time54 = datetime.datetime.strptime(str(today_date_1), "%Y-%m-%d")

last_date = datetime.datetime.now(tz).date()

dic = {}


#Function to determine the current day
def determine_date():
    global last_date    
    day_indicator = 0
    tz = pytz.timezone('Asia/Kolkata')
    today_date = datetime.datetime.now(tz).date()
    if today_date != last_date:
        last_date = today_date     
        day_indicator = 1        
    return day_indicator, today_date




#Function to determine path and create directory 
def determine_path(curr_date,device,fd):
    year = curr_date.strftime("%Y")
    month = curr_date.strftime("%Y-%m")
    day = curr_date.strftime("%Y-%m-%d")
##    master_path = 'F:/del/FlexiMC_Data/Gen1_Data/[IN-036C]/' ###local
    master_path = '/home/admin/Dropbox/FlexiMC_Data/Gen1_Data/[IN-036C]/' ###server
    year_path = master_path+year+'/'
    month_path = year_path+month+'/'
    device_path = month_path+device+'/'
    final_path = device_path+'[IN-036C]-'+fd+'-'+day+'.txt'
    
    if not os.path.exists(year_path):
        os.makedirs(year_path)
    if not os.path.exists(month_path):
        os.makedirs(month_path)
    if not os.path.exists(device_path):
        os.makedirs(device_path)
    if first_iteration == 0:
        if os.path.exists(final_path):
            os.remove(final_path)
            print('[IN-036C]-'+fd+'-'+day+'.txt'+' removed (first)')
    
    return final_path


#Function to replace header names 
def multiple_replace(text,name_head):

  if name_head == 'inverter':
      for key in wordDictI:
          text = re.sub(r"\b%s\b" % key, wordDictI[key],text)
      return text

  if name_head == 'MFM':
      for key in wordDictM:
          text = re.sub(r"\b%s\b" % key, wordDictM[key],text)
      return text

  if name_head == 'WMS':
      for key in wordDictW:
          text = re.sub(r"\b%s\b" % key, wordDictW[key],text)
      return text
    

#Function to reorder columns 
def reorder(line,name_head):
    
    if name_head == 'inverter':
        col = 31
    elif name_head == 'MFM':
        col = 63
    else:
        col = 22
        
    split = line.split('\t')
    temp = split[0]
    split[0] = split[col]
    split[col] = temp
    join = '\t'.join(split)
    return join

#Function to join average line
def get_avg(start_time,no_col,value,name_head):
    avg_line = str(start_time)
    for i in range(1,no_col):
        avg_line = avg_line + '\t'+str(value[i])
##    if name_head == 'inverter':
##        avg_line = avg_line + '\n'
    avg_line = avg_line + '\n'
    return avg_line


#Function to compute average values
def cmpt_avg(no_col,value,occ,counter):
    for i in range(no_col):
        try:
            value[i] = float(value[i])
            if value[i]!= 0:
                if i == 32:
                    value[i] = value[i]/(occ-counter)
                else:
                    value[i] = value[i]/occ
                    value[i] = round(value[i],7)
        
        except ValueError:
            pass
    return value

#Function to update start file 
def update_start_file(curr_date):
##    file = open('F:/Flexi_final/[IN-036C]/Gen-1/IN036CG.txt','w') ###local
    file = open('/home/admin/Start/IN036CG.txt','w') ###server
    file.write(str(curr_date))
    file.close()
      
#Function to write files for each device
def meter(name_head,folder_name,file_field,last_record,today_date,start_time):
    
    update_start_file(today_date)
    write_path = determine_path(today_date,folder_name,file_field)
    read_path = write_path.replace('Dropbox/FlexiMC_Data/Gen1_Data', 'Data/Flexi_Raw_Data')
    try:
        if not os.path.exists(read_path):
                raise Exception('Empty')        
    except Exception as e:
            print('No file found - '+str(today_date)+' '+ folder_name)
            return last_record, start_time

    length = 0
    with open(read_path, 'r') as read_file, open(write_path, 'a') as write_file :
                print('reading '+ read_path)
                
                data = read_file.readlines()
                length = len(data)
                lines = []
                occ = 0
                counter = 0
                if last_record < length:
                    while last_record < length:
                        line = data[last_record]
                        line = reorder(line,name_head)
                        if last_record == 0:
                            line = multiple_replace(line,name_head)
                            lines.append(line)
                            last_record +=1
                        else:
                            split = line.split('\t')
                            no_col = len(split)
                            timestamp = split[0]
                            timestamp = timestamp[:16] #To remove seconds                        
                            curr_time = datetime.datetime.strptime(timestamp, "%Y-%m-%d %H:%M")

                            while start_time < curr_time:
                                if file_field in dic.keys():
                                    value = dic[file_field][0]
                                    occ = dic[file_field][1]
                                    counter = dic[file_field][2]
                                    dic.pop(file_field)
                                    value = cmpt_avg(no_col,value,occ,counter)
                                    if name_head == 'inverter':                                    
                                        if value[32] != 0:
                                            line = get_avg(start_time,no_col,value,name_head)
                                            lines.append(line)
                                            print('Buffer WRITTEN  ',start_time,write_path[-27:])
                                            print('last_record = '+str(last_record) + ' ' + 'length = '+str(length) + ' '+ 'start_time = '+str(start_time))
                                        else:
                                            print('Skipping ',str(start_time),' as Total Energy is 0')
                                    else:
                                        line = get_avg(start_time,no_col,value,name_head)
                                        lines.append(line)
                                        print('Buffer WRITTEN  ',start_time,write_path[-27:])
                                        print('last_record = '+str(last_record) + ' ' + 'length = '+str(length) + ' '+ 'start_time = '+str(start_time))

                                    occ = 0
                                    counter = 0
                                if occ>0:
                                    value = cmpt_avg(no_col,value,occ,counter)
                                    if name_head == 'inverter':                                    
                                        if value[32] != 0:
                                            line = get_avg(start_time,no_col,value,name_head)
                                            lines.append(line)
                                            print('WRITTEN ',start_time,write_path[-27:])
                                            print('last_record = '+str(last_record) + ' ' + 'length = '+str(length) + ' '+ 'start_time = '+str(start_time))
                                        else:
                                            print('Skipping ',str(start_time),' as Total Energy is 0')
                                    else:
                                        line = get_avg(start_time,no_col,value,name_head)
                                        lines.append(line)
                                        print('WRITTEN ',start_time,write_path[-27:])
                                        print('last_record = '+str(last_record) + ' ' + 'length = '+str(length) + ' '+ 'start_time = '+str(start_time))
                                        
                                start_time+=datetime.timedelta(minutes=5)
                                occ = 0
                                counter = 0

                            if occ == 0:
                                value = [0]*no_col
                            
                            if file_field in dic.keys():
                                value = dic[file_field][0]
                                occ = dic[file_field][1]
                                counter = dic[file_field][2]
                                dic.pop(file_field)
                            
                            if(curr_time <= start_time):
                                occ +=1
##                                print(str(curr_time)+' averaged ')
                                last_record +=1
                                for i in range(no_col):
                                    try:
                                        if name_head == 'inverter' and i == 32:
                                            if split[i] == '0' or split[i] == 'NA' or split[i] == 'NULL':
                                                counter += 1
                                        value[i] += float(split[i])
                                    except ValueError: ##to check if string
                                        value[i] = split[i]
                                    except TypeError:
                                        if value[i] == 'NULL' or value[i] == 'NA': ## If NULL/NA is first, then can't do 'value[i] += float(split[i])' operation
                                            value[i] = 0
                                            value[i] += float(split[i]) 

                            
                    ## to add remaining rows (needed for last record as it won't enter 'while start_time < curr_time:' loop as no values present after that)       
                    if occ>0 :
                        print('Waiting to check if new record falls within bucket range', 'lr= ',last_record,'len=', length,'cur= ',curr_time,'st= ',start_time)
##                        global dic
                        dic.update({file_field:[value,occ,counter]})
                            
                    write_file.writelines(lines)
                    return last_record, start_time
                

                else:
                    print('Waiting for new record '+read_path[-27:]+'last_record = '+str(last_record) + ' ' + 'length = '+str(length) + ' '+ 'start_time = '+str(start_time))
                    return last_record, start_time

def reset_all(today_date):
    global last_record1, last_record2, last_record3, last_record4, last_record5, last_record6, last_record7, last_record8, last_record9, last_record10
    global last_record11, last_record12, last_record13, last_record14, last_record15, last_record16, last_record17, last_record18, last_record19, last_record20 
    global last_record21, last_record22, last_record23, last_record24, last_record25, last_record26, last_record27, last_record28, last_record29, last_record30
    global last_record31, last_record32, last_record33, last_record34, last_record35, last_record36, last_record37, last_record38, last_record39, last_record40 
    global last_record41, last_record42, last_record43, last_record44, last_record45, last_record46, last_record47, last_record48, last_record49, last_record50
    global last_record51, last_record52, last_record53, last_record54

    global start_time1, start_time2, start_time3, start_time4, start_time5, start_time6, start_time7, start_time8, start_time9, start_time10
    global start_time11, start_time12, start_time13, start_time14, start_time15, start_time16, start_time17, start_time18, start_time19, start_time20 
    global start_time21, start_time22, start_time23, start_time24, start_time25, start_time26, start_time27, start_time28, start_time29, start_time30
    global start_time31, start_time32, start_time33, start_time34, start_time35, start_time36, start_time37, start_time38, start_time39, start_time40 
    global start_time41, start_time42, start_time43, start_time44, start_time45, start_time46, start_time47, start_time48, start_time49, start_time50
    global start_time51, start_time52, start_time53, start_time54

    last_record1 = last_record2 = last_record3 = last_record4 = last_record5 = last_record6 = last_record7 = last_record8 = last_record9 = last_record10 = 0
    last_record11 = last_record12 = last_record13 = last_record14 = last_record15 = last_record16 = last_record17 = last_record18 = last_record19 = last_record20 = 0
    last_record21 = last_record22 = last_record23 = last_record24 = last_record25 = last_record26 = last_record27 = last_record28 = last_record29 = last_record30 = 0
    last_record31 = last_record32 = last_record33 = last_record34 = last_record35 = last_record36 = last_record37 = last_record38 = last_record39 = last_record40 = 0
    last_record41 = last_record42 = last_record43 = last_record44 = last_record45 = last_record46 = last_record47 = last_record48 = last_record49 = last_record50 = 0
    last_record51 = last_record52 = last_record53 = last_record54 = 0

    start_time1 = start_time2 = start_time3 = start_time4 = start_time5 = start_time6 = start_time7 = start_time8 = start_time9 = start_time10 = datetime.datetime.strptime(str(today_date), "%Y-%m-%d")
    start_time11 = start_time12 = start_time13 = start_time14 = start_time15 = start_time16 = start_time17 = start_time18 = start_time19 = start_time20 = datetime.datetime.strptime(str(today_date), "%Y-%m-%d")
    start_time21 = start_time22 = start_time23 = start_time24 = start_time25 = start_time26 = start_time27 = start_time28 = start_time29 = start_time30 = datetime.datetime.strptime(str(today_date), "%Y-%m-%d")
    start_time31 = start_time32 = start_time33 = start_time34 = start_time35 = start_time36 = start_time37 = start_time38 = start_time39 = start_time40 = datetime.datetime.strptime(str(today_date), "%Y-%m-%d")
    start_time41 = start_time42 = start_time43 = start_time44 = start_time45 = start_time46 = start_time47 = start_time48 = start_time49 = start_time50 = datetime.datetime.strptime(str(today_date), "%Y-%m-%d")
    start_time51 = start_time52 = start_time53 = start_time54 = datetime.datetime.strptime(str(today_date), "%Y-%m-%d")
    dic.clear()
    
    print('New day; Last record counters and start time reset')
    print('New Day sleeping for 27')
    time.sleep(1620)
        

first_iteration = 0
while True:
    day_indicator, today_date = determine_date()

    if day_indicator == 1:
        reset_all(today_date)

    
    last_record1, start_time1 = meter('inverter','C_Inverter_1','CI1',last_record1,today_date,start_time1) 
    last_record2, start_time2 = meter('inverter','C_Inverter_2','CI2',last_record2,today_date,start_time2)
    
    last_record3, start_time3 = meter('inverter','S_Inverter_1','SI1',last_record3,today_date,start_time3)    
    last_record4, start_time4 = meter('inverter','S_Inverter_2','SI2',last_record4,today_date,start_time4)    
    last_record5, start_time5 = meter('inverter','S_Inverter_3','SI3',last_record5,today_date,start_time5)    
    last_record6, start_time6 = meter('inverter','S_Inverter_4','SI4',last_record6,today_date,start_time6)    
    last_record7, start_time7 = meter('inverter','S_Inverter_5','SI5',last_record7,today_date,start_time7)    
    last_record8, start_time8 = meter('inverter','S_Inverter_6','SI6',last_record8,today_date,start_time8)    
    last_record9, start_time9 = meter('inverter','S_Inverter_7','SI7',last_record9,today_date,start_time9)    
    last_record10, start_time10 = meter('inverter','S_Inverter_8','SI8',last_record10,today_date,start_time10)    
    last_record11, start_time11 = meter('inverter','S_Inverter_9','SI9',last_record11,today_date,start_time11)    
    last_record12, start_time12 = meter('inverter','S_Inverter_10','SI10',last_record12,today_date,start_time12)    
    last_record13, start_time13 = meter('inverter','S_Inverter_11','SI11',last_record13,today_date,start_time13)    
    last_record14, start_time14 = meter('inverter','S_Inverter_12','SI12',last_record14,today_date,start_time14)    
    last_record15, start_time15 = meter('inverter','S_Inverter_13','SI13',last_record15,today_date,start_time15)    
    last_record16, start_time16 = meter('inverter','S_Inverter_14','SI14',last_record16,today_date,start_time16)    
    last_record17, start_time17 = meter('inverter','S_Inverter_15','SI15',last_record17,today_date,start_time17)
    last_record18, start_time18 = meter('inverter','S_Inverter_16','SI16',last_record18,today_date,start_time18)
    last_record19, start_time19 = meter('inverter','S_Inverter_17','SI17',last_record19,today_date,start_time19)
    last_record20, start_time20 = meter('inverter','S_Inverter_18','SI18',last_record20,today_date,start_time20)
    last_record21, start_time21 = meter('inverter','S_Inverter_19','SI19',last_record21,today_date,start_time21) 
    last_record22, start_time22 = meter('inverter','S_Inverter_20','SI20',last_record22,today_date,start_time22)
    last_record23, start_time23 = meter('inverter','S_Inverter_21','SI21',last_record23,today_date,start_time23)
    last_record24, start_time24 = meter('inverter','S_Inverter_22','SI22',last_record24,today_date,start_time24)
    last_record25, start_time25 = meter('inverter','S_Inverter_23','SI23',last_record25,today_date,start_time25)    
    last_record26, start_time26 = meter('inverter','S_Inverter_24','SI24',last_record26,today_date,start_time26)
    last_record27, start_time27 = meter('inverter','S_Inverter_25','SI25',last_record27,today_date,start_time27)
    last_record28, start_time28 = meter('inverter','S_Inverter_26','SI26',last_record28,today_date,start_time28)
    last_record29, start_time29 = meter('inverter','S_Inverter_27','SI27',last_record29,today_date,start_time29)    
    last_record30, start_time30 = meter('inverter','S_Inverter_28','SI28',last_record30,today_date,start_time30)
    last_record31, start_time31 = meter('inverter','S_Inverter_29','SI29',last_record31,today_date,start_time31) 
    last_record32, start_time32 = meter('inverter','S_Inverter_30','SI30',last_record32,today_date,start_time32)
    last_record33, start_time33 = meter('inverter','S_Inverter_31','SI31',last_record33,today_date,start_time33)
    last_record34, start_time34 = meter('inverter','S_Inverter_32','SI32',last_record34,today_date,start_time34)
    last_record35, start_time35 = meter('inverter','S_Inverter_33','SI33',last_record35,today_date,start_time35)    
    last_record36, start_time36 = meter('inverter','S_Inverter_34','SI34',last_record36,today_date,start_time36)
    last_record37, start_time37 = meter('inverter','S_Inverter_35','SI35',last_record37,today_date,start_time37)
    last_record38, start_time38 = meter('inverter','S_Inverter_36','SI36',last_record38,today_date,start_time38)
    last_record39, start_time39 = meter('inverter','S_Inverter_37','SI37',last_record39,today_date,start_time39)    
    last_record40, start_time40 = meter('inverter','S_Inverter_38','SI38',last_record40,today_date,start_time40)
    last_record41, start_time41 = meter('inverter','S_Inverter_39','SI39',last_record41,today_date,start_time41) 
    last_record42, start_time42 = meter('inverter','S_Inverter_40','SI40',last_record42,today_date,start_time42)
    last_record43, start_time43 = meter('inverter','S_Inverter_41','SI41',last_record43,today_date,start_time43)
    last_record44, start_time44 = meter('inverter','S_Inverter_42','SI42',last_record44,today_date,start_time44)
    last_record45, start_time45 = meter('inverter','S_Inverter_43','SI43',last_record45,today_date,start_time45)    
    last_record46, start_time46 = meter('inverter','S_Inverter_44','SI44',last_record46,today_date,start_time46)
    last_record47, start_time47 = meter('inverter','S_Inverter_45','SI45',last_record47,today_date,start_time47)
    last_record48, start_time48 = meter('inverter','S_Inverter_46','SI46',last_record48,today_date,start_time48)
    last_record49, start_time49 = meter('inverter','S_Inverter_47','SI47',last_record49,today_date,start_time49)
    
    last_record50, start_time50 = meter('MFM','LT','LT',last_record50,today_date,start_time50)    
    last_record51, start_time51 = meter('MFM','ACDB','ACDB',last_record51,today_date,start_time51)
    last_record52, start_time52 = meter('MFM','SI_MFM','SI_MFM',last_record52,today_date,start_time52) 
    last_record53, start_time53 = meter('MFM','CI_MFM','CI_MFM',last_record53,today_date,start_time53)
    
    last_record54, start_time54 = meter('WMS','WMS','WMS',last_record54,today_date,start_time54)
    
    first_iteration = 1
    time.sleep(180)



        

    
