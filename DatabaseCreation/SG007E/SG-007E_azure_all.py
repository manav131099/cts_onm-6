import os
import re
import pyodbc
import csv,time
import datetime
import collections
import os
import time
import pytz


server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 13 for SQL Server}'

read_path1 = '/home/admin/Dropbox/Fourth_Gen/[SG-007E]/Final-Settlements/[SG-007E-F]-lifetime.txt' ###server
read_path2 = '/home/admin/Dropbox/Fourth_Gen/[SG-007E]/Preliminary-Settlements/[SG-007E-P]-lifetime.txt' ###server

global start_date_update1
global start_date_update2

#Function to read start timestamp
def read_date():
    global start_date1
    global start_date2
##    file = open('R:/Flexi_final/dev/SG007EAA.txt','r') ###local
    file = open('/home/admin/Start/SG007EAA.txt','r') ###server
    date_read = file.read(21)
    date_read1 = date_read.split(" ")[0]
    date_read2 = date_read.split(" ")[1]
    start_date1 = datetime.datetime.strptime(date_read1, "%Y-%m-%d").date()
    start_date2 = datetime.datetime.strptime(date_read2, "%Y-%m-%d").date()

#Function to update start file 
def update_start_file(curr_date,filetype):
##    file = open('F:/Flexi_final/[IN-021T]/SG007EAA.txt','w') ###local
    file = open('/home/admin/Start/SG007EAA.txt','r+') ###server
    date_read = file.read(21)
    if filetype == 'prelim':
        date_read = date_read[:11] + str(curr_date)
    else:
        date_read = str(curr_date) + date_read[10:]

    file.seek(0)
    file.write(date_read)
    file.close()

def insert(fields,curr_date,filetype):
 
    counter = 0
    while True:            
        try:
            cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
            cursor = cnxn.cursor()
            update_start_file(fields[0],filetype) ##Check if you want to put it at the start of try
            if filetype == "final":
                start_date_update1 = curr_date
                with cursor.execute("INSERT INTO [Cleantech Meter Readings].dbo.[SG-007E-F] ([Date],[IEQTot..kWh.],[MEPAvg...c.kWh.],[GESCNTot....],[WEQTot..kWh.],[USEPAvg...c.kWh..],[LESDPTot....],[NETTot....],[USEPMEPRat],[ExportPerc],[Export.Volume..kWh.]) VALUES (?,?,?,?,?,?,?,?,?,?,?)",fields): 
                    print('Successfuly Inserted!' + fields[0],filetype)
            else:
                start_date_update2 = curr_date
                with cursor.execute("INSERT INTO [Cleantech Meter Readings].dbo.[SG-007E-P] ([Date],[IEQTot..kWh.],[MEPAvg...c.kWh.],[GESCNTot....],[WEQTot..kWh.],[USEPAvg...c.kWh..],[LESDPTot....],[NETTot....],[USEPMEPRat],[ExportPerc],[Export.Volume..kWh.]) VALUES (?,?,?,?,?,?,?,?,?,?,?)",fields): 
                    print('Successfuly Inserted!' + fields[0],filetype)          
                            
            
            cnxn.commit()
            cursor.close()
            cnxn.close()
            break
        
        except Exception as e:
            err_num = e[0]
            print ('ERROR:', err_num,e[1], fields[0])
            cursor.close()
            cnxn.close()
            if err_num == '08S01':
                counter = counter + 1
                print ('Sleeping for 5')
                time.sleep(300)
                if counter > 10:
                    exit('Persistent connection error')                    
                print ('%%%Re-trying connection%%%')             
            else:
                return




def convert_data_type(fields):
    for i in range(len(fields)):
        if i == 0:
            continue
        if fields[i] == 'NA' or fields[i] == 'NA\n' :
            fields[i] = None
        else:
            fields[i] = float(fields[i])

    return fields
        


def read_file(filetype):
    if filetype == "final":
        read_path = read_path1
        start_date_update = start_date_update1
    else:
        read_path = read_path2
        start_date_update = start_date_update2

    try:
        if not os.path.exists(read_path):
                raise Exception('Empty')        
    except Exception as e:
            print('No file found -',read_path)
            time.sleep(600)
            return

    start_date = start_date_update
    with open(read_path, 'r') as read_file:
        first = 1
        for line in read_file:
            if first == 1:
                first = 0
                continue
            fields =[]
            fields = line.split("\t")
            if fields[0] == "NA":
                continue
            fields[0] = fields[0].replace('"', '').strip()
    
            curr_date = datetime.datetime.strptime(fields[0], "%Y-%m-%d").date()
            if curr_date > start_date:
                fields = convert_data_type(fields)
                insert(fields,curr_date,filetype)
            else:
                print(filetype, "Waiting for new day data")



read_date()
start_date_update1 = start_date1
start_date_update2 = start_date2
while True:
    read_file('final')
    read_file('prelim')
    time.sleep(1800)

