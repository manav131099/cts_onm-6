import urllib.request, json
import csv,time
import datetime
import collections
import os
import pytz
import smtplib
import ssl

ssl._create_default_https_context = ssl._create_unverified_context

print('Running History script')
##with open('F:/Flexi_final/[IN-006C]/server_copy/IN-006C_raw_history.py') as source_file: ###local
with open('/home/admin/CODE/DatabaseCreation/IN006C/IN-006C_raw_history.py') as source_file: ###server
        exec(source_file.read())
print('RETURNED Running Live script')

##logging.basicConfig(filename = '/home/admin/CODE/DatabaseCreation/IN006C/logger.txt', level = logging.INFO)
##logging.basicConfig(filename = 'F:/Flexi_final/[IN-006C]/logger.txt', level = logging.INFO)

#global declarations
ihead = ['i1','i2','i3','i4','i5','i6','i7','i8','i9','i10','i11','i12','i13','i14','i15','i16','i17','i18','i19',
'i20','i21','i22','i23','i24','i25','i26','i27','i28','i29','i30','i31','i32','i33','i34','i35','i36','i37',
'i38','i39','i40','i41','i42','i43','i44','i45','i46','i47','i48','i49','i50','i51','i52','i53','i54']

mhead = ['m1','m2','m3','m4','m5','m6','m7','m8','m9','m10','m11','m12','m13','m14','m15','m16','m17','m18','m19','m20','m21','m22','m23',
'm24','m25','m26','m27','m28','m29','m30','m31','m32','m33','m34','m35','m36','m37','m38','m39','m40','m41','m42','m43','m44','m45',
'm46','m47','m48','m49','m50','m51','m52','m53','m54','m55','m56','m57','m58','m59','m60','m61','m62','m63','m64','m65','m66','m67']

whead = ['w1','w2','w3','w4','w5','w6','w7','w8','w9','w10','w11','w12','w13','w14','w15','w16','w17','w18','w19','w20','w21','w22','w23',
'w24','w25','w26','w27','w28','w29','w30','w31','w32','w33','w34','w35']


last_record1 = last_record2 = last_record3 = last_record4 = last_record5 = last_record6 = last_record7 = last_record8 = 0
last_record9 = last_record10 = last_record11 = last_record12 = last_record13 = last_record14 = last_record15 = last_record16 = last_record17 = last_record18 = 0
last_record19 = last_record20 = last_record21 = last_record22 = last_record23 = last_record24 = last_record25 = last_record26 = last_record27 = last_record28 = 0
last_record29 = last_record30 = last_record31 = last_record32 = last_record33 = 0
tz = pytz.timezone('Asia/Kolkata')
last_date = datetime.datetime.now(tz).date()


#Function to determine the current day
def determine_date():
    global last_date
    global last_record1, last_record2, last_record3, last_record4, last_record5, last_record6, last_record7, last_record8, last_record9, last_record10
    global last_record11, last_record12, last_record13, last_record14, last_record15, last_record16, last_record17, last_record18, last_record19, last_record20
    global last_record21, last_record22, last_record23, last_record24, last_record25, last_record26, last_record27, last_record28, last_record29, last_record30
    global last_record31, last_record32, last_record33
    
    tz = pytz.timezone('Asia/Kolkata')
    today_date = datetime.datetime.now(tz).date() 
    
    if today_date != last_date:
        last_date = today_date
        print('*********NEW DAY**********')
        last_record1 = last_record2 = last_record3 = last_record4 = last_record5 = last_record6 = last_record7 = last_record8 = 0
        last_record9 = last_record10 = last_record11 = last_record12 = last_record13 = last_record14 = last_record15 = last_record16 = last_record17 = last_record18 = 0
        last_record19 = last_record20 = last_record21 = last_record22 = last_record23 = last_record24 = last_record25 = last_record26 = last_record27 = last_record28 = 0
        last_record29 = last_record30 = last_record31 = last_record32 = last_record33 = 0

        print('New day; Last record counters reset')
        print('New day sleeping for 20')
        time.sleep(1200)
        
    return today_date


#Function to determine URL for the next day
def determine_url(date,url_head):
    replace_date = date.strftime("%Y%m%d")
    url = 'http://13.127.216.107/API/JSON/swl/BrakesIndia/Data/'+replace_date+'/'+url_head+replace_date+'.json'
    return url


#Function to determine path and create directory 
def determine_path(curr_date,device,fd):
    year = curr_date.strftime("%Y")
    month = curr_date.strftime("%Y-%m")
    day = curr_date.strftime("%Y-%m-%d")
##    master_path = 'F:/del/FlexiMC_Data/Raw_Data/[IN-006C]/' ###local
    master_path = '/home/admin/Data/Flexi_Raw_Data/[IN-006C]/' ###server
    year_path = master_path+year+'/'
    month_path = year_path+month+'/'
    device_path = month_path+device+'/'
    final_path = device_path+'[IN-006C]-'+fd+'-'+day+'.txt'
    
    if not os.path.exists(year_path):
        os.makedirs(year_path)
    if not os.path.exists(month_path):
        os.makedirs(month_path)
    if not os.path.exists(device_path):
        os.makedirs(device_path)
    if first_iteration == 0:
        if os.path.exists(final_path):
            os.remove(final_path)
            print('[IN-006C]-'+fd+'-'+day+'.txt'+' removed (first)')
    
    return final_path


#Function to update start file 
def update_start_file(curr_date):
##    file = open('F:/Flexi_final/[IN-006C]/Raw/IN006C.txt','w') ###local
    file = open('/home/admin/Start/IN006C.txt','w') ###server
    file.write(str(curr_date))
    file.close()


#Function to send mail
def sendmail():
    fromaddr = 'operations@cleantechsolar.com'
    toaddr = 'kn.rohan@gmail.com','andre.nobre@cleantechsolar.com','shravan1994@gmail.com','rupesh.baker@cleantechsolar.com'
    uname = 'shravan.karthik@cleantechsolar.com'

    subject = 'Bot IN-006C Raw Down - Portal Error'
    text = ' '
    message = 'Subject: {}\n\n{}'.format(subject,text)
    password = 'CTS&*(789'
    try:
        print('send try')
        server = smtplib.SMTP('smtp-mail.outlook.com',587)
        server.ehlo()
        server.starttls()
        server.login(uname,password)
        server.sendmail(fromaddr,toaddr,message)
        print('sending')
        server.quit()
    except Exception as e:
        print('mail send exception - ',str(e))



#Function to get header names
def get_header(json_head):
    if json_head == 'inverter':
        return ihead
    elif json_head == 'MFM':
        return mhead
    else:
        return whead

#Function to determine timestamp for logs
def determine_log_timestamp(json_head,data):
    if json_head == 'inverter':
        timestamp = data['i32']
    elif json_head == 'MFM':
        timestamp = data['m63']
    else:
        timestamp = data['w23']
    return timestamp


#Function to re-try connection
def connection_try(url):
    counter = 0
    while True:
        counter += 1
        try:
            print('second attempt try - ',counter)
            with urllib.request.urlopen(url) as url:
                try_data = json.loads(url.read().decode(), object_pairs_hook=collections.OrderedDict)
                return
        except Exception as e:
            if 'IncompleteRead' in str(e) or 'Errno 111' in str(e) or 'Remote end closed' in str(e) or 'urlopen error' in str(e):
                if counter == 4:
                    sendmail()
                    exit('Exiting due to persistent connection error')
                    
                print('sleeping for 15')
                time.sleep(900)
            else:
                return


    
#Function to write files for each device
def meter(url_head,json_head,folder_name,file_field,last_record,today_date):
    
    length = 0
    url = determine_url(today_date,url_head)
    path = determine_path(today_date,folder_name,file_field)
    try:
        with urllib.request.urlopen(url) as url:
            load_data = json.loads(url.read().decode(), object_pairs_hook=collections.OrderedDict)
            update_start_file(today_date)
            
    except Exception as e:
            if 'IncompleteRead' in str(e) or 'Errno 111' in str(e) or 'Remote end closed' in str(e) or 'urlopen error' in str(e):
                print('Incomplete read/connection refuse error - 1  Slepping for 60 min - ',str(e))
                time.sleep(3600)
                connection_try(url)
                try:
                    print('Last attempt try')
                    with urllib.request.urlopen(url) as url:
                        load_data = json.loads(url.read().decode(), object_pairs_hook=collections.OrderedDict)
                        update_start_file(today_date)
                except Exception as e:
                    if 'IncompleteRead' in str(e) or 'Errno 111' in str(e) or 'Remote end closed' in str(e) or 'urlopen error' in str(e):
                        print('Incomplete read/connection refuse error - 2  Killing bot and sending mail - ',str(e))
                        sendmail()
                        exit('Exiting due to persistent connection error')
                        
                    else:
                        print('No data found - '+str(today_date)+' '+ folder_name+' '+str(e))
            ##            logging.exception(str(e))
                        return last_record
            else:
                print('No data found - '+str(today_date)+' '+ folder_name+' '+str(e))
    ##            logging.exception(str(e))
                return last_record

    if load_data[json_head] ==None:
        print('NULL Returned by Flexi')
        return last_record
        
    length = len(load_data[json_head])
    
    if last_record < length:            
        while last_record < length:
            data = load_data[json_head][last_record]
            timestamp = determine_log_timestamp(json_head,data)
            with open(path, 'a',newline='') as csv_file:
                csvwriter = csv.writer(csv_file, delimiter='\t')
                line = []
                header = []
                
                header = get_header(json_head)
                line = [data.get(k,'NA') for k in header]
                line = ['NULL' if w is None else w for w in line]
                
                if last_record == 0:
                    csvwriter.writerow(header)
                    
                csvwriter.writerow(line)
                print(timestamp +' written '+path[-26:])
                csv_file.close()
            last_record += 1
            print('last_record = '+str(last_record) + ' ' + 'length = '+str(length))
            
        return last_record
    else:
        print('Waiting for new record '+path[-26:]+'last_record = '+str(last_record) + ' ' + 'length = '+str(length))
        return last_record


first_iteration = 0
while True:
    today_date = determine_date()

    last_record1 = meter('40_Inverter_25_','inverter','SS_Inverter_1','SS_I1',last_record1,today_date)
    last_record2 = meter('40_Inverter_24_','inverter','SS_Inverter_2','SS_I2',last_record2,today_date)
    last_record3 = meter('40_Inverter_23_','inverter','SS_Inverter_3','SS_I3',last_record3,today_date)
    last_record4 = meter('40_Inverter_22_','inverter','SS_Inverter_4','SS_I4',last_record4,today_date)
    last_record5 = meter('40_Inverter_21_','inverter','SS_Inverter_5','SS_I5',last_record5,today_date)
    last_record6 = meter('40_Inverter_6_','inverter','ABS_Inverter_1','ABS_I1',last_record6,today_date)
    last_record7 = meter('40_Inverter_7_','inverter','ABS_Inverter_2','ABS_I2',last_record7,today_date)
    last_record8 = meter('40_Inverter_8_','inverter','ABS_Inverter_3','ABS_I3',last_record8,today_date)
    last_record9 = meter('40_Inverter_9_','inverter','ABS_Inverter_4','ABS_I4',last_record9,today_date) 
    last_record10 = meter('40_Inverter_10_','inverter','ABS_Inverter_5','ABS_I5',last_record10,today_date)
    last_record11 = meter('40_Inverter_3_','inverter','EMR_Inverter_1','EMR_I1',last_record11,today_date)
    last_record12 = meter('40_Inverter_2_','inverter','EMR_Inverter_2','EMR_I2',last_record12,today_date)
    last_record13 = meter('40_Inverter_1_','inverter','EMR_Inverter_3','EMR_I3',last_record13,today_date)
    last_record14 = meter('40_Inverter_27_','inverter','ERD_Inverter_1','ERD_I1',last_record14,today_date)
    last_record15 = meter('40_Inverter_26_','inverter','ERD_Inverter_2','ERD_I2',last_record15,today_date)
    last_record16 = meter('40_Inverter_11_','inverter','U21_Inverter_1','U21_I1',last_record16,today_date)
    last_record17 = meter('40_Inverter_12_','inverter','U21_Inverter_2','U21_I2',last_record17,today_date)
    last_record18 = meter('40_Inverter_13_','inverter','U21_Inverter_3','U21_I3',last_record18,today_date)
    last_record19 = meter('40_Inverter_14_','inverter','U21_Inverter_4','U21_I4',last_record19,today_date) 
    last_record20 = meter('40_Inverter_15_','inverter','U21_Inverter_5','U21_I5',last_record20,today_date)
    last_record21 = meter('40_Inverter_16_','inverter','U21_Inverter_6','U21_I6',last_record21,today_date)
    last_record22 = meter('40_Inverter_17_','inverter','U21_Inverter_7','U21_I7',last_record22,today_date)
    last_record23 = meter('40_Inverter_18_','inverter','U21_Inverter_8','U21_I8',last_record23,today_date)
    last_record24 = meter('40_Inverter_19_','inverter','U21_Inverter_9','U21_I9',last_record24,today_date)
    last_record25 = meter('40_Inverter_20_','inverter','U21_Inverter_10','U21_I10',last_record25,today_date)
    last_record26 = meter('40_Inverter_5_','inverter','U24_Inverter_1','U24_I1',last_record26,today_date)
    last_record27 = meter('40_Inverter_4_','inverter','U24_Inverter_2','U24_I2',last_record27,today_date)

    last_record28 = meter('40_MFM_5_','MFM','SS_MFM','SS_MFM',last_record28,today_date)
    last_record29 = meter('40_MFM_3_','MFM','ABS_MFM','ABS_MFM',last_record29,today_date)
    last_record30 = meter('40_MFM_1_','MFM','EMR_MFM','EMR_MFM',last_record30,today_date)
    last_record31 = meter('40_MFM_6_','MFM','ERD_MFM','ERD_MFM',last_record31,today_date)
    last_record32 = meter('40_MFM_4_','MFM','U21_MFM','U21_MFM',last_record32,today_date)
    last_record33 = meter('40_MFM_2_','MFM','U24_MFM','U24_MFM',last_record33,today_date)    
      
    first_iteration = 1
    time.sleep(180)






        

    
