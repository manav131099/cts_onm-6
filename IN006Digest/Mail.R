rm(list=ls())
system('rm -R "/home/admin/Dropbox/Third Gen/[IN-006T]"')
source('/home/admin/CODE/IN006Digest/2G3GFunctions.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
require('mailR')
source('/home/admin/CODE/MasterMail/timestamp.R')
source('/home/admin/CODE/common/math.R')
errHandle = file('/home/admin/Logs/LogsIN006Mail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)
METERNICKNAMES = c("ABS","EMR","ER&D","SS","Unit 21","Unit 24")

checkdir = function(x)
{
	if(!file.exists(x))
	{
		dir.create(x)
	}
}
daysAlive = 0
DOB = "01-01-2016"
sender = c('operations@cleantechsolar.com')
uname = 'shravan.karthik@cleantechsolar.com'
recipients = getRecipients("IN-006T","m")
pwd = 'CTS&*(789'

sendMail= function(pathall)
{
  filenams = c(" "," "," "," "," "," ")
  body = ""
	body = paste(body,"Site Name: Brakes India, Padi",sep="")
	body = paste(body,"\n\nLocation: Chennai, India")
	body = paste(body,"\n\nO&M Code: IN-006")
	body = paste(body,"\n\nSystem Size: 486.475")
	body = paste(body,"\n\nNumber of Energy Meters: 6")
	body = paste(body,"\n\nModule Brand / Model / Nos: REC / 305W / 1595")
	body = paste(body,"\n\nInverter Brand / Model / Nos: SMA / STP60 / 27")
	body = paste(body,"\n\nSite COD: 2015-12-30")
	body = paste(body,"\n\nSystem age [days]:",as.character((32+as.numeric(daysAlive))))
	body = paste(body,"\n\nSystem age [years]:",as.character(round((32+as.numeric(daysAlive))/365,2)))
	bodyac = body
	body = ""
	TOTALGENCALC = 0
	MYLD = c()
	#Mail has to come in the order ER&D >> ABS >> EMR >> U24 >> U21 >> SS
	reOrder = c(3,1,2,6,5,4)
	acname = METERNICKNAMES[reOrder]
	pathall = pathall[reOrder]
	for(t in 1 : length(pathall))
  {
	path = pathall[t]
  dataread = read.table(path,header = T,sep="\t")
	currday = as.character(dataread[1,1])
	filenams[t] = paste(currday,".txt",sep="")
	body = paste(body,"\n\n________________________________________________\n\n")
	body = paste(body,currday,acname[t])
	body = paste(body,"\n\n________________________________________________\n\n")
	body = paste(body,"DA [%]:",as.character(dataread[1,2]),"\n\n")
	body = paste(body,"EAC method-1 (Pac) [kWh]:",as.character(dataread[1,3]),"\n\n")
	body = paste(body,"EAC method-2 (Eac) [kWh]:",as.character(dataread[1,4]),"\n\n")
	TOTALGENCALC = TOTALGENCALC + as.numeric(dataread[1,4])
	body = paste(body,"Yield-1 [kWh/kWp]:",as.character(dataread[1,5]),"\n\n")
	body = paste(body,"Yield-2 [kWh/kWp]:",as.character(dataread[1,6]),"\n\n")
	MYLD[t] = as.numeric(dataread[1,6])
	body = paste(body,"Last recorded value [kWh]:",as.character(dataread[1,7]),"\n\n")
	body = paste(body,"Last recorded timestamp:",as.character(dataread[1,8]))
	if(t==1)
	{
	body = paste(body,"\n\nStation DOB:",as.character(DOB),"\n\n")
	body = paste(body,"Days alive:",as.character(daysAlive))
	GSIGLOBAL = as.numeric(dataread[1,9])
	}
  }
	YIELDAll = round(TOTALGENCALC/486.475,2)
	bodyac = paste(bodyac,"\n\nSystem Full Generation (kWh):",TOTALGENCALC)
	bodyac = paste(bodyac,"\n\nSystem Full Yield (kWh/kWp):",YIELDAll)
	bodyac = paste(bodyac,"\n\nIrradiation [kWh/m2, from IN-713S, GHI silicon sensor]:",GSIGLOBAL)
	bodyac = paste(bodyac,"\n\nSystem Full PR:",round(YIELDAll*100/GSIGLOBAL,1))
	bodyac = paste(bodyac,"\n\n",acname[1],"Yield (kWh/kWp):",MYLD[1])
	bodyac = paste(bodyac,"\n\n",acname[2],"Yield (kWh/kWp):",MYLD[2])
	bodyac = paste(bodyac,"\n\n",acname[3],"Yield (kWh/kWp):",MYLD[3])
	bodyac = paste(bodyac,"\n\n",acname[4],"Yield (kWh/kWp):",MYLD[4])
	bodyac = paste(bodyac,"\n\n",acname[5],"Yield (kWh/kWp):",MYLD[5])
	bodyac = paste(bodyac,"\n\n",acname[6],"Yield (kWh/kWp):",MYLD[6])
	bodyac = paste(bodyac,"\n\nStdev/COV Yields: ",round(sdp(MYLD),2)," / ",round((sdp(MYLD)*100/mean(MYLD)),1),"%",sep="")
	bodyac = paste(bodyac,body)
	body = bodyac
	body = gsub("\n ","\n",body)
  send.mail(from = sender,
            to = recipients,
            subject = paste("Station [IN-006T] Digest",currday),
            body = body,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            attach.files = pathall,
            file.names = filenams, # optional paramete
            debug = F)
recordTimeMaster("IN-006T","Mail",currday)
}


path = "/home/admin/Dropbox/Gen 1 Data/[IN-006T]"
path2G = '/home/admin/Dropbox/Second Gen/[IN-006T]'
path3G = '/home/admin/Dropbox/Third Gen/[IN-006T]'

checkdir(path2G)
checkdir(path3G)

years = dir(path)
stnnickName2 = "IN-006T"
lastdatemail = lastMailDate(paste('/home/admin/Start/MasterMail/',stnnickName2,'_Mail.txt',sep=""))
stopDate = paste("[",stnnickName2,"-M6] ",lastdatemail,".txt",sep="")
ENDCALL=0
for(x in 1 : length(years))
{
	path2Gyr = paste(path2G,years[x],sep = "/")
	path3Gyr = paste(path3G,years[x],sep = "/")
	pathyr = paste(path,years[x],sep="/")
	checkdir(path2Gyr)
	checkdir(path3Gyr)
	path3Gyr1 = paste(path3Gyr,METERNICKNAMES[1],sep="/")
	path3Gyr2 = paste(path3Gyr,METERNICKNAMES[2],sep="/")
	path3Gyr3 = paste(path3Gyr,METERNICKNAMES[3],sep="/")
	path3Gyr4 = paste(path3Gyr,METERNICKNAMES[4],sep="/")
	path3Gyr5 = paste(path3Gyr,METERNICKNAMES[5],sep="/")
	path3Gyr6 = paste(path3Gyr,METERNICKNAMES[6],sep="/")
	checkdir(path3Gyr1)
	checkdir(path3Gyr2)
	checkdir(path3Gyr3)
	checkdir(path3Gyr4)
	checkdir(path3Gyr5)
	checkdir(path3Gyr6)
	months = dir(pathyr)
	for(y in 1 : length(months))
	{
		path2Gmon = paste(path2Gyr,months[y],sep = "/")
		path3Gfinal = c(paste(path3Gyr1,"/",months[y],".txt",sep = ""), paste(path3Gyr2,"/",months[y],".txt",sep = ""),paste(path3Gyr3,"/",months[y],".txt",sep = ""),
		paste(path3Gyr4,"/",months[y],".txt",sep = ""),paste(path3Gyr5,"/",months[y],".txt",sep = ""),paste(path3Gyr6,"/",months[y],".txt",sep = ""))
		pathmon = paste(pathyr,months[y],sep="/")
		checkdir(path2Gmon)
		stns = dir(pathmon)
		dunmun = 0
		for(t in 1 : length(stns))
		{
		  if(ENDCALL==1)
				break
			pathmon1 = paste(pathmon,stns[t],sep="/")
		  days = dir(pathmon1)
		  path2Gmon1 = paste(path2Gmon,stns[t],sep = "/")
			if(!file.exists(path2Gmon1))
			{	
			dir.create(path2Gmon1)
			}
		if(length(days)>0)
		{
		for(z in 1 : length(days))
		{
			if(ENDCALL == 1)
				break
			if((z==length(days)) && (y == length(months)) && (x ==length(years)))
				next
			print(days[z])
			pathfinal = paste(pathmon1,days[z],sep = "/")
			path2Gfinal = paste(path2Gmon1,days[z],sep="/")
			secondGenData(pathfinal,path2Gfinal,t)
			thirdGenData(path2Gfinal,path3Gfinal[t])
			if(!dunmun){
			daysAlive = daysAlive+1
			}
			if(days[z] == stopDate)
			{
				ENDCALL = 1
				next
			}
		}
		}
		dunmun = 1
	}
	if(ENDCALL == 1)
	break
}
if(ENDCALL == 1)
break
}

print('Backlog done')

prevx = x
prevy = y
prevz = z
repeats = 0
SENDMAILTRIGGER = 0
while(1)
{
	recipients = getRecipients("IN-006T","m")
	recordTimeMaster("IN-006T","Bot")
	years = dir(path)
	noyrs = length(years)
	for(x in prevx : noyrs)
	{
		pathyr = paste(path,years[x],sep="/")
		path2Gyr = paste(path2G,years[x],sep="/")
		path3Gyr = paste(path3G,years[x],sep="/")
		checkdir(path2Gyr)
		checkdir(path3Gyr)
		path3Gyr1 = paste(path3Gyr,METERNICKNAMES[1],sep="/")
		path3Gyr2 = paste(path3Gyr,METERNICKNAMES[2],sep="/")
		path3Gyr3 = paste(path3Gyr,METERNICKNAMES[3],sep="/")
		path3Gyr4 = paste(path3Gyr,METERNICKNAMES[4],sep="/")
		path3Gyr5 = paste(path3Gyr,METERNICKNAMES[5],sep="/")
		path3Gyr6 = paste(path3Gyr,METERNICKNAMES[6],sep="/")
		checkdir(path3Gyr1)
		checkdir(path3Gyr2)
		checkdir(path3Gyr3)
		checkdir(path3Gyr4)
		checkdir(path3Gyr5)
		checkdir(path3Gyr6)
		mons = dir(pathyr)
		nomons = length(mons)
		startmn = prevy
		endmn = nomons
		if(startmn>endmn)
		{
			startmn = 1
			prevx = x-1
			prevz = 1
		}
		for(y in startmn:endmn)
		{
			pathmon = paste(pathyr,mons[y],sep="/")
			path2Gmon = paste(path2Gyr,mons[y],sep="/")
			checkdir(path2Gmon)
			path3Gfinal = c(paste(path3Gyr1,"/",months[y],".txt",sep = ""), paste(path3Gyr2,"/",months[y],".txt",sep = ""),paste(path3Gyr3,"/",months[y],".txt",sep = ""),
			paste(path3Gyr4,"/",months[y],".txt",sep = ""),paste(path3Gyr5,"/",months[y],".txt",sep = ""),paste(path3Gyr6,"/",months[y],".txt",sep = ""))
			stns = dir(pathmon)
			path2Gfinalall = c(" "," "," ")
			for(t in 1 : length(stns))
			{
			  pathmon1 = paste(pathmon,stns[t],sep="/")
			  days = dir(pathmon1)
			  path2Gmon1 = paste(path2Gmon,stns[t],sep = "/")
			chkcopydays = days[grepl('Copy',days)]
			if(!file.exists(path2Gmon1))
			{
			dir.create(path2Gmon1)
			}
			if(length(chkcopydays) > 0)
			{
				print('Copy file found they are')
				print(chkcopydays)
				idxflse = match(chkcopydays,days)
				print(paste('idx matches are'),idxflse)
				for(innerin in 1 : length(idxflse))
				{
					command = paste("rm '",pathmon,"/",days[idxflse[innerin]],"'",sep="")
					print(paste('Calling command',command))
					system(command)
				}
				days = days[-idxflse]
			}
			nodays = length(days)
			if(y > startmn)
			{
				z = prevz = 1
			}
			if(nodays > 0)
			{
			for(z in prevz : nodays)
			{
				if((z == nodays) && (y == endmn) && (x == noyrs))
				{
					if(!repeats)
					{
						print('No new data')
						repeats = 1
					}
					next
				}
				if(is.na(days[z]))
				{
					print('Day was NA, do next in loop')
					next
				}
			  SENDMAILTRIGGER <<- 1
				repeats = 0
				print(paste('New data, calculating digests',days[z]))
				pathdays = paste(pathmon1,days[z],sep = "/")
				path2Gfinal = paste(path2Gmon1,days[z],sep="/")
				path2Gfinalall[t] = paste(path2Gmon1,days[z],sep="/")
				secondGenData(pathdays,path2Gfinal,t)
				thirdGenData(path2Gfinal,path3Gfinal[t])
			}
			}
			}
			  if(SENDMAILTRIGGER)
			  {
				print('Sending mail')
				daysAlive = daysAlive+1
				sendMail(path2Gfinalall)
			  SENDMAILTRIGGER <<- 0
				}
		}
	}
	prevx = x
	prevy = y
	prevz = z
	Sys.sleep(3600)
}
print(paste('Exited for some reason x y z values are'),x,y,z)
