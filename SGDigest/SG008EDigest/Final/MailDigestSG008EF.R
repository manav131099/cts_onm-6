errHandle = file('/home/admin/Logs/LogsSG008EFMail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)

source('/home/admin/CODE/SGDigest/SG008EDigest/Final/HistoricalAnalysis2G3GSG008EF.R')
source('/home/admin/CODE/MasterMail/timestamp.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
initDigest = function(df)
{
  body = "\n\n******************************************\n\n"
  body = paste(body,as.character(df[,1])," (",weekdays(as.Date(as.character(df[,1]))),")",sep="")
  body = paste(body,"\n\n******************************************\n\n")
	body = paste(body,"Solar generation on site (IEQ) [kWh]: ",(as.numeric(df[,2])),sep="")
  body = paste(body,"\n\nAverage nodal electricity price on site (MEP) [$/MWh]: ",as.character(df[,3]),sep="")
  body = paste(body,"\n\nSolar production revenue-equivalent on site (GESCN) [$]: ",as.character(df[,4]))
	body = paste(body,"\n\nConsumption of the solar generation on site (WEQ) [kWh]: ",(as.numeric(df[,5])),sep="")
	body = paste(body,"\n\nAverage uniform Singapore energy price (USEP) [$/MWh]: ",df[,6],sep="")
	body = paste(body,"\n\nSolar consumption revenue-equivalent on site (LESDP) [$]: ",df[,7],sep="")
	body = paste(body,"\n\nNet settlement [$]: ",df[,8],sep="")
	body = paste(body,"\n\nUSEP/MEP price difference: ",df[,9],sep="")
  body = paste(body,"\n\nExport electricity [kWh]: ",(abs((as.numeric(df[,5])-as.numeric(df[,2])))),sep="")
  body = paste(body,"\n\nPercentage export electricity [%]: ",format(round(as.numeric(df[,10])),nsmall=1))
  body = paste(body,"\n******************************************\n")
  return(body)
}

sendMail = function(pth1,pth3,pth2)
{
  filetosendpath = pth1
	df1 = read.table(pth1,header=T,sep="\t")
  if(file.exists(pth3))
	{
  filetosendpath = c(pth1,pth3,pth2)
  }
	filenams = c()
  for(l in 1 : length(filetosendpath))
  {
    temp = unlist(strsplit(filetosendpath[l],"/"))
    filenams[l] = temp[length(temp)]
  }

	print('Filenames Processed')
  body = initDigest(df1)
	print('2G data processed')
  body = paste(body,"\n\n\n******************************************\n\n")
  body = paste(body,"Data History")
  body = paste(body,"\n\n******************************************\n\n")
  body = paste(body,"\nStation DOB:",as.character(DOB))
  body = paste(body,"\n# Days alive:",as.character(DAYSALIVE))
  yrsalive = format(round((DAYSALIVE/365),2),nsmall=2)
  body = paste(body,"\n# Years alive:",yrsalive)
  body = paste(body,"\nMEP average for this month [SGD/MWh]:",MEPAVG)
  body = paste(body,"\nUSEP average for this month [SGD/MWh]:",USEPAVG)
  body = paste(body,"\nRatio USEP/MEP price difference this month:",RATIOAVG)
  body = paste(body,"\n\n******************************************")
	print('3G data processed')
	
  send.mail(from = sender,
            to = recipients,
            subject = paste("[SG-008E-F] Digest",as.character(df1[,1])),
            body = body,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            attach.files = filetosendpath,
            file.names = filenams, # optional parameter
            debug = F)
	recordTimeMaster("SG-008EFinal","Mail",as.character(df1[,1]))
  
}

sender = c('operations@cleantechsolar.com')
uname = 'shravan.karthik@cleantechsolar.com'
recipients = getRecipients("SG-008EFinal","m")


pwd = 'CTS&*(789'
todisp = 1
dispLastDate=0
while(1)
{
	recipients = getRecipients("SG-008EFinal","m")
  recordTimeMaster("SG-008EFinal","Bot")
	sendmail = 0
  prevx = x
  prevy = y
  prevt = t
  years = dir(path)
  for(x in prevx : length(years))
  {
    pathyear = paste(path,years[x],sep="/")
    writepath2Gyr = paste(writepath2G,years[x],sep="/")
    writepath3Gyr = paste(writepath3G,years[x],sep="/")
    checkdir(writepath2Gyr)
    checkdir(writepath3Gyr)
    months = dir(pathyear)
    startmnth = 1
    if(prevx == x)
    {
      startmnth = prevy
    }
    for(y in startmnth : length(months))
    {
      pathdays = paste(pathyear,months[y],sep="/")
      writepath2Gdays = paste(writepath2Gyr,months[y],sep="/")
      writepath3Gfinal = paste(writepath3Gyr,"/[SG-008E-F] ",months[y],".txt",sep="")
      checkdir(writepath2Gdays)
      days = dir(pathdays)
      startdays = 1
      if(y == prevy)
      {
        startdays = prevt
      }
      for(t in startdays : length(days))
      {
         condn1  = (t != length(days))
         condn2 = ((t == length(days)) && ((y != length(months)) || (x != length(years))))
         if(!(condn1 || condn2 || dispLastDate))
         {
				 	 
					 if(todisp)
					 {
					 	print('No new files')
						todisp = 0
					 }
           Sys.sleep(3600)
           next
         }
				 print(paste('Processing',days[t]))
				 if(grepl("Copy",days[t]))
				 {
				 	print('Copy file found so removing it')
					command = paste('rm "',pathdays,'/',days[t],'"',sep="")
					print(command)
					system(command)
					print('Command complete.. sleeping')
				 	Sys.sleep(3600)
					next
				 }
				 todisp = 1
         sendmail = 1
         DAYSALIVE = DAYSALIVE + 1
         writepath2Gfinal = paste(writepath2Gdays,"/",days[t],sep="")
         readpath = paste(pathdays,days[t],sep="/")
         Sys.sleep(60)
				 secondGenData(readpath,writepath2Gfinal)
         thirdGenData(writepath2Gfinal,writepath3Gfinal)
				 dispLastDate=0
				 if(condn1 || condn2)
				 	dispLastDate=1
  			if(sendmail ==0)
  			{
    			next
  			}
				print('Sending Mail')
  			sendMail(writepath2Gfinal,writepath3Gfinal,readpath)
				print('Mail Sent')
      }
    }
  }
	gc()
}
sink()
