rm(list = ls())
TIMESTAMPSALARM = NULL
ltcutoff = 0.001
timetomins = function(x)
{
  hr = unlist(strsplit(x,":"))
	seq1 = seq(from = 1,to = length(hr),by =2)
	seq2 = seq(from = 2,to = length(hr),by =2)
  min = as.numeric(hr[seq2]) 
  hr = as.numeric(hr[seq1]) * 60
  return((hr + min + 1))
}
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}
secondGenData = function(filepath,writefilepath)
{
	print(paste('file path is',filepath))
	print(paste('write-file path is',writefilepath))

	TIMESTAMPSALARM <<- NULL
  dataread = read.table(filepath,header = T,sep = "\t",stringsAsFactors=F)
	gsiidx = ncol(dataread)-1
	tmodidx = ncol(dataread)
	idx1en = 39
	idx2en = 15
	if(nrow(dataread) < 1)
	{
	  print('Err in file')
		return(NULL)
	}
	dataread2 = dataread
	dataread = dataread2[complete.cases(as.numeric(dataread2[,idx1en])),]
	lastt = NA
	lastr = NA
	{
	if(nrow(dataread) < 1)
	{
	 Eac2 = 0
	}
	else{
	print(paste('first reading',as.numeric(dataread[1,idx1en])))
	print(paste('last reading',as.numeric(dataread[nrow(dataread),idx1en])))
	Eac2 = format(round((as.numeric(dataread[nrow(dataread),idx1en]) - as.numeric(dataread[1,idx1en]))/1000,1),nsmall=1)
	lastr = round((as.numeric(dataread[nrow(dataread),idx1en])/1000),3)
	lastt = as.character(dataread[nrow(dataread),1])
	}
	}
	dataread = dataread2[complete.cases(as.numeric(dataread2[,idx2en])),]
  {
	if(nrow(dataread) < 1){
	 Eac1 = 0}
	else{
	print(paste('length of eac2',nrow(dataread)))
	Eac1 = format(round(sum(as.numeric(dataread[,idx2en]))/60000,1),nsmall=1)
	}
	}
	GSI = 0
	TMOD=0
	dataread = as.numeric(dataread2[complete.cases(as.numeric(dataread2[,gsiidx])),gsiidx])
	if(length(dataread))
		GSI = round(sum(dataread)/60000,2)
	dataread = as.numeric(dataread2[complete.cases(as.numeric(dataread2[,tmodidx])),tmodidx])
	if(length(dataread))
		TMOD = round(mean(dataread),1)
	dataread = dataread2
  datewrite = substr(as.character(dataread[1,1]),1,10)
	DA = format(round(nrow(dataread)/14.4,1),nsmall=1)
  tdx = timetomins(substr(dataread[,1],12,16))
  dataread2 = dataread[tdx > 540,]
  tdx = tdx[tdx > 540]
  dataread2 = dataread2[tdx < 1020,]
	dataread2 = dataread2[complete.cases(as.numeric(dataread2[,idx2en])),]
  missingfactor = 480 - nrow(dataread2)
	print(paste('missing factor is',missingfactor))
  dataread2 = dataread2[as.numeric(dataread2[,idx2en]) < (ltcutoff*1000),]
	DSPY = round(as.numeric(Eac1)/44.785,2)
	DSPY2 = round(as.numeric(Eac2)/44.785,2)
	PR1 = round(((DSPY*100)/as.numeric(GSI)),1)
	PR2 = round(((DSPY2*100)/as.numeric(GSI)),1)
	if(length(dataread2[,1]) > 0)
	{
			TIMESTAMPSALARM <<- as.character(dataread2[,1])
	}
  totrowsmissing = format(round((missingfactor + nrow(dataread2))/4.8,1),nsmall=1)
  df = data.frame(Date = datewrite, Eac1 = as.numeric(Eac1),Eac2 = as.numeric(Eac2),
                  DA = DA,Downtime = totrowsmissing,DailySpecYield = DSPY,LastTime = lastt,
									LastRead = lastr,Gsi=GSI,Tmod=TMOD,DSPY2=DSPY2,PR=PR1,PR2=PR2,stringsAsFactors=F)
  write.table(df,file = writefilepath,row.names = F,col.names = T,sep="\t",append = F)
  return(df)
}

thirdGenData = function(filepathm1,writefilepath)
{
  dataread1 =read.table(filepathm1,header = T,sep="\t",stringsAsFactors=F)
  Eac1T = as.numeric(dataread1[,2])
  Eac2T = as.numeric(dataread1[,3])
  df = data.frame(Date = substr(as.character(dataread1[1,1]),1,10),EacTotMethod1 = Eac1T,EacTotMethod2=Eac2T,
	DailySpecYield = as.character(dataread1[,6]),LastTime = as.character(dataread1[,7]),LastRead = as.character(dataread1[,8]),
	Gsi = as.character(dataread1[,9]),Tmod=as.character(dataread1[,10]),
	DailySpecYield2 = as.character(dataread1[,11]),PR1=as.character(dataread1[,12]),PR2=as.character(dataread1[,13]),
	stringsAsFactors=F)
  {
    if(file.exists(writefilepath))
    {
			data = read.table(writefilepath,header=T,sep="\t",stringsAsFactors=F)
			dates = as.character(data[,1])
			idxmtch = match(substr(as.character(dataread1[1,1]),1,10),dates)
			if(is.finite(idxmtch))
			{
				data = data[-idxmtch,]
				write.table(data,file = writefilepath,row.names = F,col.names = T,sep = "\t",append = F)
			}
      write.table(df,file = writefilepath,row.names = F,col.names = F,sep = "\t",append = T)
    }
    else
    {
      write.table(df,file = writefilepath,row.names = F,col.names = T,sep = "\t",append = F)
    }
  }
  return(c(as.numeric(Eac1T),as.numeric(Eac2T)))
}

