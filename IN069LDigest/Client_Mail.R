rm(list=ls())
require('compiler')
enableJIT(3)
errHandle = file('/home/admin/Logs/LogsIN069LClientMail.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)

source('/home/admin/CODE/IN069LDigest/summaryFunctions.R')
source('/home/admin/CODE/common/math.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
RESETHISTORICAL=0
daysAlive = 0
reorderStnPaths = c(30,23,24,25,1,12,16,17,18,19,20,21,22,2,3,4,5,6,7,8,9,10,11,13,14)


source('/home/admin/CODE/MasterMail/timestamp.R')
require('mailR')
source('/home/admin/CODE/IN068LDigest/aggregateInfo.R')


MYLDN = vector()
MYLDS = vector()
MYLD_arr = vector()
imp_energy1 = 0
imp_energy2 = 0
imp_energy3 = 0
exp_energy1 = 0
exp_energy2 = 0
exp_energy3 = 0
imp_reading1 = 0
imp_reading2 = 0
imp_reading3 = 0

METERNICKNAMES = c("WMS","MFM_1 (Unit 2)","MFM_2 (Colony Area)","MFM_3 (Unit 1)","INV-1","INV-2","INV-3","INV-4","INV-5","INV-6","INV-7","INV-8","INV-9","INV-10","INV-11","INV-12","INV-13","INV-14","INV-15","INV-16","INV-5 (UNIT-1)","INV-4 (UNIT-1)","INV-1 (UNIT-2)","INV-2 (UNIT-2)","INV-3 (UNIT-2)")

METERACNAMES = c("WMS_2","MFM_1","MFM_2","MFM_3","INVERTER_1","INVERTER_2","INVERTER_3","INVERTER_4","INVERTER_5","INVERTER_6","INVERTER_7","INVERTER_8","INVERTER_9","INVERTER_10","INVERTER_11","INVERTER_12","INVERTER_13","INVERTER_14","INVERTER_15","INVERTER_16","INVERTER_17","INVERTER_18","INVERTER_19","INVERTER_20","INVERTER_21")

checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

DOB = "06-04-2019"
DOB2="2019-04-06"
DOB2=as.Date(DOB2)
sender = c('operations@cleantechsolar.com')
uname = 'shravan.karthik@cleantechsolar.com'
recipients =getRecipients("IN-069L","m")
pwd = 'CTS&*(789'
referenceDays = NA
extractDaysOnly = function(days)
{
  seq = seq(from=1,to=length(days)*6,by=6)
  days = unlist(strsplit(days,"-"))
  days = paste(days[seq+3],days[seq+4],days[seq+5],sep="-")
  return(days)
}
analyseDays = function(days,ref)
{
  if(length(days) == length(ref))
    return(days)
  daysret = unlist(rep(NA,length(ref)))
  if(length(days) == 0)
    return(daysret)
  days2 = extractDaysOnly(days)
  idxmtch = match(days2,ref)
  idxmtch2 = idxmtch[complete.cases(idxmtch)]
  if(length(idxmtch) != length(idxmtch2))
  {
    print(".................Missmatch..................")
    print(days2)
    print("#########")
    print(ref)
    print("............................................")
    return(days)
  }
  if(is.finite(idxmtch))
  {
    daysret[idxmtch] = as.character(days)
    if(!(is.na(daysret[length(daysret)])))
      return(daysret)
  }	
  return(days)
}
performBackWalkCheck = function(day)
{
  path = "/home/admin/Dropbox/Gen 1 Data/[IN-069L]"
  retval = 0
  yr = substr(day,1,4)
  yrmon = substr(day,1,7)
  pathprobe = paste(path,yr,yrmon,sep="/")
  stns = dir(pathprobe)
  print(stns)
  idxday = c()
  for(t in 1 : length(stns))
  {
    pathdays = paste(pathprobe,stns[t],sep="/")
    print(pathdays)
    days = dir(pathdays)
    dayMtch = days[grepl(day,days)]
    if(length(dayMtch))
    {
      idxday[t] = match(dayMtch,days)
    }
    print(paste("Match for",day,"is",idxday[t]))
  }
  idxmtch = match(NA,idxday[t])
  if(length(unique(idxday))==1 || length(idxmtch) < 5)
  {
    print('All days present ')
    retval = 1
  }
  return(retval)
}
sendMail= function(pathall)
{
  path = pathall[t]
  dataread = read.table(path,header = T,sep="\t")
  currday = as.character(dataread[1,1])
  Cur=as.Date(currday)
  filenams = c()
  body = ""
  body = paste(body,"Site Name: Prism Cement Satna",sep="")
  body = paste(body,"\n\nLocation: Satna, India")
  body = paste(body,"\n\nO&M Code: IN-069")
  body = paste(body,"\n\nSystem Size [kWp]:",INSTCAP)

  bodyac = body
  body = ""
  TOTALGENCALC = 0
  yr = substr(Cur,1,4)
  yrmon = substr(Cur,1,7)
  path2 = "/home/admin/Dropbox/Gen 1 Data/[IN-069L]"
  filename2 = paste("[IN-069L]-MFM1-",Cur,".txt",sep="")
  yield1 = 0
  #pathRead2 = paste(path1,yr,yrmon,filename1,sep="/")
  pathRead3 = paste(path2,yr,yrmon,"MFM_1",filename2,sep="/")
  filename3 = paste("[IN-069L]-MFM2-",Cur,".txt",sep="")

  #pathRead2 = paste(path1,yr,yrmon,filename1,sep="/")
  pathRead4 = paste(path2,yr,yrmon,"MFM_2",filename3,sep="/")
  filename4 = paste("[IN-069L]-MFM3-",Cur,".txt",sep="")

  #pathRead2 = paste(path1,yr,yrmon,filename1,sep="/")
  pathRead5 = paste(path2,yr,yrmon,"MFM_3",filename4,sep="/")
  GTIGreater20=NA
  GTI = DNI = NA
  print("Printing new path")
  print(pathRead3)
  if(file.exists(pathRead3))
  {
    dataread3 = read.table(pathRead3, sep = "\t", header = T)
    dataread4 = read.table(pathRead4, sep = "\t", header = T)
    dataread5 = read.table(pathRead5, sep = "\t", header = T)
    if(nrow(dataread3) > 0)
    {
      last_read = as.numeric(round(dataread3[1,9]),2)
      imp_energy1 = as.numeric(round(dataread3[1,51]),2)
      exp_energy1 = as.numeric(round(dataread3[1,50]),2)
      imp_energy1 = imp_energy1 / 1000
      exp_energy1 = exp_energy1/1000
      imp_energy2 = as.numeric(round(dataread4[1,51]),2)
      exp_energy2 = as.numeric(round(dataread4[1,50]),2)
      imp_energy2 = imp_energy2 / 1000
      exp_energy2 = exp_energy2/1000
      imp_energy3 = as.numeric(round(dataread5[1,51]),2)
      exp_energy3 = as.numeric(round(dataread5[1,50]),2)
      imp_energy3 = imp_energy3 / 1000
      exp_energy3 = exp_energy3/1000
      imp_reading1 = exp_energy1 - imp_energy1
      imp_reading2 = exp_energy2 - imp_energy2
      imp_reading3 = exp_energy3 - imp_reading3
      
    }
    
  }
  MYLD = c()
  
  acname = METERNICKNAMES
  noInverters = NOINVS
  globMtYld = globMtVal = globMtname = c()
  idxglobMtYld=1
  InvReadings = unlist(rep(NA,noInverters))
  InvAvail = unlist(rep(NA,noInverters))
  for(t in 1 : length(pathall))
  {
    type = 0
    meteridx = t
    if(grepl("WMS",pathall[t])) 
      type = 1
    else if(grepl("MFM",pathall[t]))
      type=1
    else
      type = 0
    if( t > (1+NOMETERS))
    {
      splitpath = unlist(strsplit(pathall[t],"INVERTER_"))
      if(length(splitpath)>1)
      {
        meteridx = unlist(strsplit(splitpath[2],"/"))
        meteridx = as.numeric(meteridx[1])
      }
    }
    path = pathall[t]
    dataread = read.table(path,header = T,sep="\t")
    currday = as.character(dataread[1,1])
    if(type==0)
    {
      filenams[t] = paste(currday,"-",METERNICKNAMES[meteridx+2],".txt",sep="")
    }
    else if(type==1 && grepl("WMS",pathall[t]))
    {
      filenams[t] = paste(currday,"-",'WMS',".txt",sep="")
    }
    else
    {
      filenams[t] = paste(currday,"-",'MFM',".txt",sep="") 
    }
    print("---------------------")
    print(path)
    print(filenams[t])
    print("---------------------")
    if(type == 1)
    {
      
      {
      if(grepl("MFM",pathall[t]))
      {
        body = paste(body,"\n\n________________________________________________\n\n")	
        body = paste(body,currday,acname[t])	
        body = paste(body,"\n________________________________________________\n\n")	
        if(grepl("MFM_1",pathall[t])){
         # body = paste(body,"System Size [kWp]:",INSTCAPM[1],"\n\n")
        }else if(grepl("MFM_2",pathall[t])){
          #body = paste(body,"System Size [kWp]:",INSTCAPM[2],"\n\n")
        }else if(grepl("MFM_3",pathall[t])){
          #body = paste(body,"System Size [kWp]:",INSTCAPM[3],"\n\n")
        }
      
        body = paste(body,"Total Energy Generation [kWh]:",as.character(format(round(dataread[1,4],1), nsmall = 1)),"\n\n")
        TOTALGENCALC = TOTALGENCALC + as.numeric(dataread[1,4])
        
        body = paste(body,"Yield [kWh/kWp]:",as.character(format(round(dataread[1,6],2), nsmall = 2)),"\n\n")
        yld2 = dataread[1,6]
        globMtYld[idxglobMtYld] = as.numeric(dataread[1,6])
        globMtVal[idxglobMtYld] = as.numeric(dataread[1,4])
        #globMtname[idxglobMtYld] = acnames[idx]
        idxglobMtYld = idxglobMtYld + 1
        body = paste(body,"Performance Ratio [%]:",round(as.numeric(yld2*100/GTI),1),"\n\n")
        body = paste(body,"Energy Reading [kWh]:",as.character(format(round(dataread[1,9],1), nsmall = 1)),"\n\n")
        if(grepl("MFM_1",pathall[t])){
          body = paste(body,"Import Energy [kWh]:",imp_energy1,"\n\n")
          body = paste(body,"Import Reading [kWh]:",imp_reading1,"\n")
        }else if(grepl("MFM_2",pathall[t])){
          body = paste(body,"Import Energy [kWh]:",imp_energy2,"\n\n")
          body = paste(body,"Import Reading [kWh]:",imp_reading2,"\n")
        }else if(grepl("MFM_3",pathall[t])){
          body = paste(body,"Import Energy [kWh]:",imp_energy3,"\n\n")
          body = paste(body,"Import Reading [kWh]:",imp_reading3,"\n")
        }
        }
        else
        {
          GTI = dataread[1,3]
        }
      }
      next
    }
    InvReadings[meteridx] = as.numeric(dataread[1,7])
    InvAvail[meteridx]= as.numeric(dataread[1,10])
    MYLD[(meteridx)] = as.numeric(dataread[1,7])
  }
  MYLDCPY = MYLD
  MYLD_arr = c(MYLD)
  MYLDN = c(MYLDN, MYLD_arr[1], MYLD_arr[2], MYLD_arr[3], MYLD_arr[4], MYLD_arr[5],MYLD_arr[6], MYLD_arr[7], MYLD_arr[8],MYLD_arr[9], MYLD_arr[10], MYLD_arr[11], MYLD_arr[12], MYLD_arr[13],MYLD_arr[14], MYLD_arr[15], MYLD_arr[16])
  
  print(MYLDCPY)
  if(length(MYLD))
  {
    MYLD = MYLD[complete.cases(MYLD)]
    MYLDN = MYLDN[complete.cases(MYLDN)]
  }
  addwarn = 0
  sddev = covar = sddev_north = NA
  if(length(MYLD))
  {
    addwarn = 1
    sddev = round(sdp(MYLD),1)
    meanyld = mean(MYLD)
    covar = round((sdp(MYLD)*100/meanyld),1)
    
    sddev_north = round(sdp(MYLDN),2)
    meanyld_north = mean(MYLDN)
    covar_north = round((sdp(MYLDN)*100/meanyld_north),1)
  }
  MYLD = MYLDCPY
  for(t in 1 : noInverters)
  {
    if( addwarn && is.finite(covar) && covar > 5 &&
        (!is.finite(InvReadings[t]) || (abs(InvReadings[t] - meanyld) > 2*sddev)))
    {
    }
  }
  
  sdm =round(sdp(globMtYld),3)
  covarm = round(sdm*100/mean(globMtYld),1)

  bodyac = paste(bodyac,"\n\n________________________________________________\n\n")
  bodyac = paste(bodyac,currday,"SITE PERFORMANCE")
  bodyac = paste(bodyac,"\n________________________________________________\n")
  bodyac = paste(bodyac,"\n\nEnergy Generated FULL SITE [kWh]:",sum(globMtVal))
  bodyac = paste(bodyac,"\n\nYield FULL SITE [kWh/kWp]:",
                 round(sum(globMtVal)/sum(INSTCAPM),2))
  bodyac = paste(bodyac,"\n\nGTI [kWh/m2]:",GTI)
  bodyac = paste(bodyac,"\n\nPerformance Ratio FULL SITE [%]:",
                 round(sum(globMtVal)/(sum(INSTCAPM)*GTI)*100,1))

  body = gsub("\n ","\n",body)
  body = paste(bodyac,body,sep="")
  send.mail(from = sender,
            to = recipients,
            subject = paste("Generation Report PJL, Cement Division-I [IN-9069L]",currday),
            body = body,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
            authenticate = TRUE,
            send = TRUE,
            #attach.files = pathall,
            file.names = filenams, # optional paramete
            debug = F)
  recordTimeMaster("IN-069L","Mail",currday)
}


path = "/home/admin/Dropbox/Gen 1 Data/[IN-069L]"
path2G = '/home/admin/Dropbox/Second Gen/[IN-069L]'
path3G = '/home/admin/Dropbox/Third Gen/[IN-069L]'
path4G = '/home/admin/Dropbox/Fourth_Gen/[IN-069L]'


if(RESETHISTORICAL)
{
  command = paste("rm -rf",path2G)
  system(command)
  command = paste("rm -rf",path3G)
  system(command)
  command = paste("rm -rf",path4G)
  system(command)
}

checkdir(path2G)
checkdir(path3G)

years = dir(path)
stnnickName2 = "IN-069L"
lastdatemail = lastMailDate(paste('/home/admin/Start/MasterMail/',stnnickName2,'_Mail.txt',sep=""))
stopDate = paste("[",stnnickName2,"]-I21-",lastdatemail,".txt",sep="")
print(paste('StopDate is',stopDate))
ENDCALL=0
for(x in 1 : length(years))
{
  path2Gyr = paste(path2G,years[x],sep = "/")
  pathyr = paste(path,years[x],sep="/")
  checkdir(path2Gyr)
  months = dir(pathyr)
  for(y in 1 : length(months))
  {
    path2Gmon = paste(path2Gyr,months[y],sep = "/")
    pathmon = paste(pathyr,months[y],sep="/")
    checkdir(path2Gmon)
    stns = dir(pathmon)
    stns = stns[reorderStnPaths]
    dunmun = 0
    for(t in 1 : length(stns))
    {
      type = 1
      if(grepl("MFM", stns[t]))
        type = 0
      if(stns[t] == "WMS_2")
        type = 2
      pathmon1 = paste(pathmon,stns[t],sep="/")
      days = dir(pathmon1)
      path2Gmon1 = paste(path2Gmon,stns[t],sep = "/")
      if(!file.exists(path2Gmon1))
      {	
        dir.create(path2Gmon1)
      }
      
      
      if(length(days)>0)
      {
        for(z in 1 : length(days))
        {
          if(ENDCALL == 1)
            break
          if((z==length(days)) && (y == length(months)) && (x ==length(years)))
            next
          print(days[z])
          pathfinal = paste(pathmon1,days[z],sep = "/")
          path2Gfinal = paste(path2Gmon1,days[z],sep="/")
          if(RESETHISTORICAL)
          {
            secondGenData(pathfinal,path2Gfinal,type)
          }
          if(!dunmun){
            daysAlive = daysAlive+1
          }
          if(days[z] == stopDate)
          {
            ENDCALL = 1
            print('Hit end call')
            next
          }
        }
      }
      dunmun = 1
      if(ENDCALL == 1)
        break
    }
    if(ENDCALL == 1)
      break
  }
  if(ENDCALL == 1)
    break
}

print('Backlog done')

prevx = x
prevy = y
prevz = z
repeats = 0
SENDMAILTRIGGER = 0
newlen=1
while(1)
{
  recipients = getRecipients("IN-069L","m")
  recordTimeMaster("IN-069L","Bot")
  years = dir(path)
  noyrs = length(years)
  path2Gfinalall = vector('list')
  for(x in prevx : noyrs)
  {
    pathyr = paste(path,years[x],sep="/")
    path2Gyr = paste(path2G,years[x],sep="/")
    checkdir(path2Gyr)
    mons = dir(pathyr)
    nomons = length(mons)
    startmn = prevy
    endmn = nomons
    if(startmn>endmn)
    {
      startmn = 1
      prevx = x-1
      prevz = 1
    }
    for(y in startmn:endmn)
    {
      pathmon = paste(pathyr,mons[y],sep="/")
      path2Gmon = paste(path2Gyr,mons[y],sep="/")
      checkdir(path2Gmon)
      stns = dir(pathmon)
      if(length(stns) < 25)
      {
        print('Station sync issue.. sleeping for an hour')
        Sys.sleep(3600) # Sync issue, meter data comes after MFM and WMS
        stns = dir(pathmon)
      }
      stns = stns[reorderStnPaths]
      for(t in 1 : length(stns))
      {
        newlen = (y-startmn+1) + (x-prevx)
        print(paste('Reset newlen to',newlen))
        type = 1
        if(grepl("MFM",stns[t]))
          type = 0
        if(stns[t] == "WMS_2")
          type = 2
        pathmon1 = paste(pathmon,stns[t],sep="/")
        days = dir(pathmon1)
        path2Gmon1 = paste(path2Gmon,stns[t],sep = "/")
        chkcopydays = days[grepl('Copy',days)]
        if(!file.exists(path2Gmon1))
        {
          dir.create(path2Gmon1)
        }
        if(length(chkcopydays) > 0)
        {
          print('Copy file found they are')
          print(chkcopydays)
          idxflse = match(chkcopydays,days)
          print(paste('idx matches are'),idxflse)
          for(innerin in 1 : length(idxflse))
          {
            command = paste("rm '",pathmon,"/",days[idxflse[innerin]],"'",sep="")
            print(paste('Calling command',command))
            system(command)
          }
          days = days[-idxflse]
        }
        days = days[complete.cases(days)]
        if(t==1)
          referenceDays <<- extractDaysOnly(days)
        else
          days = analyseDays(days,referenceDays)
        nodays = length(days)
        if(y > startmn)
        {
          z = prevz = 1
        }
        if(nodays > 0)
        {
          startz = prevz
          if(startz > nodays)
            startz = nodays
          for(z in startz : nodays)
          {
            if(t == 1)
            {
              path2Gfinalall[[newlen]] = vector('list')
            }
            if(is.na(days[z]))
              next
            pathdays = paste(pathmon1,days[z],sep = "/")
            path2Gfinal = paste(path2Gmon1,days[z],sep="/")
            secondGenData(pathdays,path2Gfinal,type)
            if((z == nodays) && (y == endmn) && (x == noyrs))
            {
              if(!repeats)
              {
                print('No new data')
                repeats = 1
              }
              next
            }
            if(is.na(days[z]))
            {
              print('Day was NA, do next in loop')
              next
            }
            SENDMAILTRIGGER <<- 1
            if(t == 1)
            {
              splitstr =unlist(strsplit(days[z],"-"))
              daysSend = paste(splitstr[4],splitstr[5],splitstr[6],sep="-")
              print(paste("Sending",daysSend))
              if(performBackWalkCheck(daysSend) == 0)
              {
                print("Sync issue, sleeping for an 1 hour")
                Sys.sleep(3600) # Sync issue -- meter data comes a little later than
                # MFM and WMS
              }
            }
            repeats = 0
            print(paste('New data, calculating digests',days[z]))
            path2Gfinalall[[newlen]][t] = paste(path2Gmon1,days[z],sep="/")
            newlen = newlen + 1
            print(paste('Incremented newlen by 1 to',newlen))
          }
        }
      }
    }
  }
  if(SENDMAILTRIGGER)
  {
    print('Sending mail')
    print(paste('newlen is ',newlen))
    for(lenPaths in 1 : (newlen - 1))
    {
      if(lenPaths < 1)
        next
      print(unlist(path2Gfinalall[[lenPaths]]))
      pathsend = unlist(path2Gfinalall[[lenPaths]])
      daysAlive = daysAlive+1
      sendMail(pathsend)
    }
    SENDMAILTRIGGER <<- 0
    newlen = 1
    print(paste('Reset newlen to',newlen))
    
  }
  
  prevx = x
  prevy = y
  prevz = z
  Sys.sleep(3600)
}
print(paste('Exited for some reason x y z values are'),x,y,z)
sink()